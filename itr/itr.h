#ifndef ITR_H
#define ITR_H

/* ************************************************************************** */

#include "../bst/bst.h"
#include "../utility.h"

/* ************************************************************************** */

typedef void* (*ITRConstruct)(void*);
typedef void (*ITRDestruct)(void*);

typedef bool (*ITRTerminated)(void*);
typedef void* (*ITRElement)(void*);
typedef void (*ITRSuccessor)(void*);

/* ************************************************************************** */

typedef struct ITRType {
    ITRConstruct construct;
    ITRDestruct destruct;
    ITRTerminated isTerminated;
    ITRElement element;
    ITRSuccessor successor;
} ITRType;

typedef struct ITRObject {
    ITRType* type;
    void* iter;
} ITRObject;

/* ************************************************************************** */

ITRObject* itrConstruct(ITRType*, void*);
void itrDestruct(ITRObject*);

// Verifica se l'iteratore in ingresso è terminato
bool itrTerminated(ITRObject*);

// Restituisce l'elemento corrente
void* itrElement(ITRObject*);

// Sposta l'iteratore in avanti
void itrSuccessor(ITRObject*);

/* ************************************************************************** */

#endif
