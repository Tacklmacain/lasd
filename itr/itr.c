#include "itr.h"

/* ************************************************************************** */

ITRObject* itrConstruct(ITRType* type, void* root) {
    ITRObject* iterator = (ITRObject*) malloc(sizeof(ITRObject));

    iterator->type = type;
    iterator->iter = type->construct(root);

    return iterator;
}

void itrDestruct(ITRObject* iterator) {
    assert(iterator);
    iterator->type->destruct(iterator->iter);
    free(iterator);
}

// Verifica se l'iteratore in ingresso è terminato
bool itrTerminated(ITRObject* iterator) {
    assert(iterator);
    return iterator->type->isTerminated(iterator->iter);
}

// Restituisce l'elemento corrente
void* itrElement(ITRObject* iterator) {
    assert(iterator);
    return iterator->type->element(iterator->iter);
}

// Sposta l'iteratore in avanti
void itrSuccessor(ITRObject* iterator) {
    assert(iterator);
    return iterator->type->successor(iterator->iter);
}