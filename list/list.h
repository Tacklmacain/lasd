#ifndef EXERCISE3_LIST_H
#define EXERCISE3_LIST_H

/* ************************************************************************** */

#include "../utility.h"
#include "../adt/adt.h"

/* ************************************************************************** */

typedef struct ListNode ListNode;

/** Struttura per il singolo nodo della lista
  * @attribute DataObject
  * Contiene il nome e la corrispondenza in Array di quel vertice
  *
  * @attribute next
  * Il successore di quel nodo
  */
struct ListNode {
    DataObject* data;
    ListNode* next;
};

/** Struttura dati per la gestione di liste, necessaria per implementare
  * i grafi con liste di adiacenza
  * @attribute head
  * Il nodo in testa alla lista
  *
  * @attribute size
  * La grandezza della lista
  */
typedef struct ListObject {
    ListNode* head;
    uint size;
} ListObject;


/* ************************************************************************** */

ListObject* lstConstruct();
void lstDestruct(ListObject*);

bool lstInsert(ListObject*, DataObject*);
bool lstRemove(ListObject*, DataObject*);
void lstClear(ListObject*);
uint lstSize(ListObject*);

ListObject* lstClone(ListObject*);
bool lstExists(ListObject*, DataObject*);
bool lstEmpty(ListObject*);
bool lstEqual(ListObject*, ListObject*);

void lstMap(ListObject*, MapFun, void*);
void lstFold(ListObject*, FoldFun, void*, void*);

/* ************************************************************************** */


#endif