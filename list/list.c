#include "list.h"

ListObject* lstConstruct() {
    ListObject* list = (ListObject*) malloc(sizeof(ListObject));

    list->size = 0;
    list->head = NULL;

    return list;
}

void lstDestruct(ListObject* list) {
    assert(list);

    if(!lstEmpty(list)) {
        lstClear(list);
    }
    free(list);
}

/** Inserisce un elemento in lista in modo ordinato
  * @param list la lista a cui si vuole inserire l'elemento
  * @param newElem l'elemento che si intende aggiungere
  * @return true se l'elemento è stato inserito con successo, false altrimenti
  */
bool lstInsert(ListObject* list, DataObject* newElem) {
    assert(list);

    // Creo il nodo che intendo inserire
    ListNode *newNode = (ListNode*) malloc(sizeof(ListNode));
    newNode->data = adtClone(newElem);

    // Caso base, head è NULL oppure elemento più piccolo della testa
    if (!list->head || adtCompare(list->head->data, newElem) > 0) {
        newNode->next = list->head;
        list->head = newNode;
        list->size++;
        return true;
    }
    // Caso in cui la testa è uguale distruggo il nodo e ritorno false
    else if(adtCompare(list->head->data, newElem) == 0) {
        adtDestruct(newNode->data);
        free(newNode);
        return false;
    }

    // Se arrivo qui devo scorrere la lista per cercare se già esiste
    ListNode *pred = list->head, *travel = list->head->next;
    while (travel) {
        // Se trovo un nodo uguale, allora lo distruggo e torno false
        if (adtCompare(travel->data, newElem) == 0) {
            adtDestruct(newNode->data);
            free(newNode);
            return false;
        }
        // Se l'elemento è più grande, allora posso inserirlo
        else if (adtCompare(travel->data, newElem) > 0) {
            newNode->next = travel;
            pred->next = newNode;
            list->size++;
            return true;
        }

        // Avanzo le variabili
        pred = pred->next;
        travel = travel->next;
    }

    // Controllo se l'ultimo nodo è diverso, se è il caso inserisco
    if (adtCompare(pred->data, newElem) != 0) {
        newNode->next = NULL;
        pred->next = newNode;
        list->size++;
        return true;
    }

    return false;
}

/** Rimuove un elemento in lista
  * @param list la lista a cui si vuole rimuovere l'elemento
  * @param newElem l'elemento che si intende rimuovere
  * @return true se l'elemento è stato rimosso con successo, false altrimenti
  */
bool lstRemove(ListObject* list, DataObject* elemToRemove) {
    assert(list && elemToRemove);

    // Se la lista è vuota, non posso rimuovere nulla
    if(lstEmpty(list)) {
        return false;
    }

    /* Se l'elemento da rimuovere è la testa la salvo, la sposto
     * e procedo con l'eliminazione */
    ListNode* tmp;
    if(adtCompare(list->head->data, elemToRemove) == 0) {
        tmp = list->head;
        list->head = list->head->next;
        adtDestruct(tmp->data);
        free(tmp);
        list->size--;
        return true;
    }

    // Altrimenti ciclo finché non lo trovo e poi lo cancello
    tmp = list->head;
    ListNode* travel = list->head->next;
    while(travel) {
        if(adtCompare(travel->data, elemToRemove) == 0) {
            tmp->next = travel->next;
            adtDestruct(travel->data);
            free(travel);
            list->size--;
            return true;
        }

        tmp = travel;
        travel = travel->next;
    }

    // Se arrivo qui non ho mai trovato l'elemento e ritorno false
    return false;
}

void lstClear(ListObject* list) {
    assert(list);

    ListNode *travel = list->head, *tmp;
    while(travel) {
        tmp = travel->next;
        adtDestruct(travel->data);
        free(travel);
        travel = tmp;
    }
    list->size = 0;
}

uint lstSize(ListObject* list) {
    assert(list);
    return list->size;
}

ListObject* lstClone(ListObject* list) {
    assert(list);
    ListObject* clone = (ListObject*) malloc(sizeof(ListObject));
    clone->size = list->size;
    clone->head = NULL;

    // Se ho almeno un elemento allora devo duplicare
    if(list->head) {
        // Copio il primo elemento
        ListNode* newNode = (ListNode*) malloc(sizeof(ListNode));
        newNode->data = adtClone(list->head->data);
        newNode->next = NULL;

        ListNode *node = list->head->next, *prev = newNode;
        clone->head = newNode;
        // Ciclo copiando tutti gli elementi successivi man mano
        while(node) {
            newNode = (ListNode*) malloc(sizeof(ListNode));
            newNode->data = adtClone(node->data);
            newNode->next = NULL;

            prev->next = newNode;
            prev = prev->next;
            node = node->next;
        }
    }

    return clone;
}

bool lstExists(ListObject* list, DataObject* key) {
    assert(list);

    ListNode* travel = list->head;

    while(travel) {
        if(adtCompare(travel->data, key) == 0) {
            return true;
        }
        travel = travel->next;
    }
    return false;
}

bool lstEmpty(ListObject* list) {
    assert(list);
    return list->size == 0;
}

bool lstEqual(ListObject* list1, ListObject* list2) {
    assert(list1 && list2);

    // Se le dimensioni sono diverse, le liste sono diverse
    if(list1->size != list2->size) {
        return false;
    }

    ListNode* travel1 = list1->head, *travel2 = list2->head;

    /* Siccome hanno la stessa dimensione è sufficiente ciclare su
     * uno dei due, se incontro un DataObject diverso, sono diversi */
    while(travel1) {
        if(adtCompare(travel1->data, travel2->data) != 0) {
            return false;
        }

        travel1 = travel1->next;
        travel2 = travel2->next;
    }

    return true;
}

void lstMap(ListObject* list, MapFun mapFunction, void* parameter) {
    assert(list);

    ListNode* travel = list->head;
    while(travel) {
        mapFunction(travel->data, parameter);
        travel = travel->next;
    }
}

void lstFold(ListObject* list, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(list);

    ListNode* travel=list->head;
    while(travel) {
        foldFunction(travel->data, accumulator, parameter);
        travel=travel->next;
    }
}