#include "utility.h"
#include "itr/itr.h"

/* ************************************************************************** */

void mapPrint(DataObject* element, void* _) {
    printf("Elemento: ");
    adtWriteToMonitor(element);
    printf("\n");
}

// Utility che aggiorna il type di tutti i DataObject di una struttura
void changeType(DataObject* element, void* newType) {
    element->type = newType;
}

void mapUppercaseStr(DataObject* element, void* _) {
    assert(element);

    char* str = (char*) element->value;
    uint i = 0;

    // Scorro le lettere della stringa
    while (str[i]) {
        // Se e' un carattere alfabetico lowercase, allora ne faccio l'Uppercase
        if (((int) str[i] >= 97) && ((int) str[i] <= 122)) {
            str[i] -= 32;
        }
        i++;
    }
}

void mapLowercaseStr(DataObject* element, void* _) {
    assert(element);

    char* str = (char*) element->value;
    uint i = 0;

    // Scorro le lettere della stringa
    while (str[i]) {
        // Se e' un carattere alfabetico uppercase, allora ne faccio il Lowercase
        if (((int) str[i] >= 65) && ((int) str[i] <= 90)) {
            str[i] += 32;
        }
        i++;
    }
}

void mapHeadStr(DataObject* element, void* head) {
    assert(element && head);

    int totalLength = strlen((char*) element->value) + strlen((char*) head) + 1;
    char* newStr = (char*) malloc(sizeof(char) * totalLength);

    strcpy(newStr, (char*) head);
    strcat(newStr, (char*) element->value);

    free(element->value);
    element->value = newStr;
}

void mapPowerFloat(DataObject* element, void* _) {
    assert(element);

    *((float*) element->value) *= *((float*) element->value);
}

void mapMultiplicateInt(DataObject* element, void* _) {
    assert(element);

    *((int*) element->value) *= 2;
}

/* ************************************************************************** */

void foldProductInt(DataObject* element, void* result, void* maxSize) {
    assert(element);

    // Assegno il valore attuale ad una variabile per semplificare
    int value = *((int*) element->value);

    //Se l'intero e' minore di maxSize lo moltiplico al totale
    if(value < *((int*)maxSize)) {
        *((int*) result) *= value;
    }
}

void foldSumFloat(DataObject* element, void* result, void* minSize) {
    assert(element);

    // Assegno il valore attuale ad una variabile per semplificare
    float value = *((float*) element->value);

    //Se l'intero e' minore di maxSize lo sommo al totale
    if(value > *((float*) minSize)) {
        *((float*) result) += value;
    }
}

// Utility per contare la lunghezza di tutte le stringhe che rispettano MaxSize
void foldSizeOfStr(DataObject* element, void* sum, void* maxSize) {
    assert(element && sum && maxSize);

    // Prendo la grandezza della stringa attuale non considerando l'escape
    uint strLength = (uint) strlen((char*) element->value);

    // Se la grandezza è minore di quella richiesta, la sommo al totale
    if(strLength <= *((uint*) maxSize)) {
        *((uint*) sum) += strLength;
    }
}

void foldConcatStr(DataObject* element, void* concatStr, void* maxSize) {
    assert(element && concatStr);

    // Se la grandezza della stringa e' minore di quella richiesta, la concateno
    if(strlen((char*) element->value) <= *((uint*) maxSize)) {
        strcat((char*) concatStr, (char*) element->value);
    }
}

void foldValueExists(DataObject* element, void* exists, void* key) {
    assert(element && exists);

    // Se non l'ho ancora trovato verifico l'elemento corrente
    if(!*((bool*) exists)) {
        if(adtCompare(element, (DataObject*) key) == 0) {
            *((bool*) exists) = true;
        }
    }
}

/* ************************************************************************** */

int rndNum(int a, int b) {
    assert(a <= b);
    return (a + (rand() % (b - a + 1)));
}

char* rndStr(uint length) {
    char* randomString = (char*) malloc((length + 1) * sizeof(char));
    uint i = 0;

    // Genero vari numeri man mano e li converto in caratteri
    while(i < length) {
        uint randomNumber = (uint) rndNum(65, 122);

        if((randomNumber <= 90) || (randomNumber >= 97)) {
            randomString[i++] = (char) randomNumber;
        }
    }

    // Inserisco l'escape
    randomString[i] = '\0';
    return randomString;
}

/* ************************************************************************** */

void getStr(char* str, uint len) {
    assert(str != NULL);
    uint i;
    char c;
    for(i = 0; (i < len) && ((c = getchar()) != '\n') && (c != EOF); i++) {
        str[i] = c;
    }
    str[i] = '\0';
}

/* ************************************************************************** */

void destructIteratorAndType(void* iterator) {
    ITRObject* iter = (ITRObject*) iterator;
    void* oldType = iter->type;
    itrDestruct(iter);
    free(oldType);
}
