#ifndef UTILITY_H
#define UTILITY_H

/* ************************************************************************** */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "adt/adt.h"

/* ************************************************************************** */

typedef unsigned int uint;
typedef struct DataType DataType;
typedef struct DataObject DataObject;

/* ************************************************************************** */

void mapPrint(DataObject*, void*);
void changeType(DataObject* element, void* newType);
void mapUppercaseStr(DataObject*, void*);
void mapLowercaseStr(DataObject*, void*);
void mapHeadStr(DataObject*, void*);
void mapPowerFloat(DataObject*, void*);
void mapMultiplicateInt(DataObject*, void*);

/* ************************************************************************** */

void foldProductInt(DataObject*, void*, void*);
void foldSumFloat(DataObject*, void*, void*);
void foldSizeOfStr(DataObject*, void*, void*);
void foldConcatStr(DataObject*, void*, void*);
void foldValueExists(DataObject*, void*, void*);

/* ************************************************************************** */

int rndNum(int, int);
char* rndStr(uint);

/* ************************************************************************** */

void getStr(char*, uint);

/* ************************************************************************** */

// Funzione di utility per distruggere l'iteratore ed il suo type
void destructIteratorAndType(void*);

/* ************************************************************************** */

#endif
