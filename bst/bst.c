#include "bst.h"

/* ************************************************************************** */
// La costruct si fa solo astratta in quanto sia ricorsivo che iterativo è uguale
BSTObject* bstConstruct(BSTType* type) {
    BSTObject* tree = (BSTObject*) malloc(sizeof(BSTObject));

    tree->type = type;
    tree->root = NULL;
    tree->size = 0;

    return tree;
}

void bstDestruct(BSTObject* tree) {
    assert(tree);

    if(!bstEmpty(tree)) {
        tree->type->destruct(tree->root);
    }
    free(tree);
}

bool bstEmpty(BSTObject* tree) {
    assert(tree);

    return tree->size == 0;
}

uint bstSize(BSTObject* tree) {
    return tree->size;
}

void bstClear(BSTObject* tree) {
    assert(tree);

    if(tree->root) {
        tree->type->destruct(tree->root);
        tree->size = 0;
        tree->root = NULL;
    }
}

BSTObject* bstClone(BSTObject* tree) {
    assert(tree);
    BSTObject* clone = (BSTObject*) malloc(sizeof(BSTObject));

    clone->type = tree->type;
    clone->root = tree->type->clone(tree->root);
    clone->size = tree->size;

    return clone;
}

bool bstEqual(BSTObject* tree1, BSTObject* tree2) {
    assert(tree1 && tree2);

    // Se hanno numero di elementi oppure tipo diverso, allora sono diversi
    if(tree1->size != tree2->size || tree1->type != tree2->type) {
        return false;
    }

    // Se le due radici sono entrambe NULL, sono uguali
    if(!tree1->root && !tree2->root) {
        return true;
    }

    //Siccome il tipo è uguale posso usare il compare di tree1
    return tree1->type->equal(tree1->root, tree2->root);
}

bool bstExists(BSTObject* tree, DataObject* key) {
    assert(tree && key);

    return tree->type->exists(tree->root, key);
}


bool bstInsert(BSTObject* tree, DataObject* newElem) {
    assert(tree);

    // Se newElem è null allora l'inserimento non ha successo
    if(!newElem) {
        return false;
    }

    /* Se l'inserimento specifico ha successo, incremento il
     * numero di elementi e ritorno che ha avuto successo */
    bool isInserted = false;
    tree->root = tree->type->insert(tree->root, newElem, &isInserted);
    if(isInserted) {
        tree->size++;
    }
    return isInserted;
}

bool bstRemove(BSTObject* tree, DataObject* elemToDelete) {
    assert(tree);

    // Se l'elemento è null oppure l'albero è vuoto la rimozione non ha successo
    if(!elemToDelete || bstEmpty(tree)) {
        return false;
    }

    /* Se la rimozione specifica ha successo, decremento il
     * numero di elementi e ritorno che ha avuto successo */
    bool isRemoved = false;
    tree->root = tree->type->remove(tree->root, elemToDelete, &isRemoved);
    if(isRemoved) {
        tree->size--;
    }
    return isRemoved;
}

DataObject* bstGetMin(BSTObject* tree) {
    assert(tree);

    if(bstEmpty(tree)) {
        return NULL;
    }

    return tree->type->getMin(tree->root);
}

DataObject* bstGetNRemoveMin(BSTObject* tree) {
    assert(tree);
    DataObject* elemToReturn = NULL;

    // Se il bst non è vuoto allora troverò sempre un elemento da rimuovere
    if(!bstEmpty(tree)) {
        tree->size--;

        /* Dichiaro il dataObject che punterà all'elemento che dovrò tornare
         * modifico l'albero e lo ritorno */
        tree->root = tree->type->getNRemoveMin(tree->root, &elemToReturn);
    }

    return elemToReturn;
}

void bstRemoveMin(BSTObject* tree) {
    assert(tree);

    // Se ho almeno un elemento allora esiste il min
    if(!bstEmpty(tree)) {
        tree->root = tree->type->removeMin(tree->root);
        tree->size--;
    }
}

DataObject* bstGetMax(BSTObject* tree) {
    assert(tree);

    if(bstEmpty(tree)) {
        return NULL;
    }

    return tree->type->getMax(tree->root);
}

DataObject* bstGetNRemoveMax(BSTObject* tree) {
    assert(tree);

    if(bstEmpty(tree)) {
        return NULL;
    }

    // Se il bst non è vuoto allora troverò sempre un elemento da rimuovere
    tree->size--;

    /* Dichiaro il dataObject che punterà all'elemento che dovrò tornare
     * modifico l'albero e lo ritorno */
    DataObject* elemToReturn = NULL;
    tree->root = tree->type->getNRemoveMax(tree->root, &elemToReturn);
    return elemToReturn;
}

void bstRemoveMax(BSTObject* tree) {
    assert(tree);

    // Se ho almeno un elemento allora esiste il max
    if(!bstEmpty(tree)) {
        tree->root = tree->type->removeMax(tree->root);
        tree->size--;
    }
}

DataObject* bstGetPredecessor(BSTObject* tree, DataObject* key) {
    assert(tree);

    // Se l'albero è vuoto, allora non può esistere il predecessore
    if(bstEmpty(tree)) {
        return NULL;
    }

    return tree->type->getPredecessor(tree->root, key);
}

DataObject* bstGetNRemovePredecessor(BSTObject* tree, DataObject* key) {
    assert(tree);

    // Se l'albero è vuoto, allora non può esistere il predecessore
    if(bstEmpty(tree)) {
        return NULL;
    }

    bool isRemoved = false;
    DataObject* elemToReturn = NULL;
    tree->root = tree->type->getNRemovePredecessor(tree->root, key, &elemToReturn, &isRemoved);

    /* Controllo se mi ha tornato un elemento concreto o meno, nel caso
     * sia il primo caso decremento anche la size dell'albero*/
    if(isRemoved) {
        tree->size--;
    }
    return elemToReturn;
}

bool bstRemovePredecessor(BSTObject* tree, DataObject* key) {
    assert(tree);

    // Se l'albero è vuoto la rimozione fallisce
    if(bstEmpty(tree)) {
        return false;
    }

    bool isRemoved = false;
    tree->root = tree->type->removePredecessor(tree->root, key, &isRemoved);

    if(isRemoved) {
        tree->size--;
    }
    return isRemoved;
}

DataObject* bstGetSuccessor(BSTObject* tree, DataObject* key) {
    assert(tree);

    // Se l'albero è vuoto, allora non può esistere il predecessore
    if(bstEmpty(tree)) {
        return NULL;
    }

    return tree->type->getSuccessor(tree->root, key);
}

DataObject* bstGetNRemoveSuccessor(BSTObject* tree, DataObject* key) {
    assert(tree);

    // Se l'albero è vuoto, allora non può esistere il predecessore
    if(bstEmpty(tree)) {
        return NULL;
    }

    bool isRemoved = false;
    DataObject* elemToReturn = NULL;
    tree->root = tree->type->getNRemoveSuccessor(tree->root, key, &elemToReturn, &isRemoved);

    /* Controllo se mi ha tornato un elemento concreto o meno, nel caso
     * sia il primo caso decremento anche la size dell'albero*/
    if(isRemoved) {
        tree->size--;
    }
    return elemToReturn;
}

bool bstRemoveSuccessor(BSTObject* tree, DataObject* key) {
    assert(tree);

    // Se l'albero è vuoto la rimozione fallisce
    if(bstEmpty(tree)) {
        return false;
    }

    bool isRemoved = false;
    tree->root = tree->type->removeSuccessor(tree->root, key, &isRemoved);

    if(isRemoved) {
        tree->size--;
    }
    return isRemoved;
}

void bstPreOrderMap(BSTObject* tree, MapFun mapFunction, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->preOrderMap(tree->root, mapFunction, parameter);
    }
}

void bstInOrderMap(BSTObject* tree, MapFun mapFunction, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->inOrderMap(tree->root, mapFunction, parameter);
    }
}

void bstPostOrderMap(BSTObject* tree, MapFun mapFunction, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->postOrderMap(tree->root, mapFunction, parameter);
    }
}

void bstBreadthMap(BSTObject* tree, MapFun mapFunction, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->breadthMap(tree->root, mapFunction, parameter);
    }
}

void bstPreOrderFold(BSTObject* tree, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->preOrderFold(tree->root, foldFunction, accumulator, parameter);
    }
}

void bstInOrderFold(BSTObject* tree, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->inOrderFold(tree->root, foldFunction, accumulator, parameter);
    }
}

void bstPostOrderFold(BSTObject* tree, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->postOrderFold(tree->root, foldFunction, accumulator, parameter);
    }
}

void bstBreadthFold(BSTObject* tree, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(tree);

    if(tree->root) {
        tree->type->breadthFold(tree->root, foldFunction, accumulator, parameter);
    }
}

// Funzioni custom
DataObject* bstGetValue(BSTObject* tree, DataObject* data) {
    assert(tree && data);

    // Se il bst non è vuoto allora potrei trovare l'elemento
    if(!bstEmpty(tree)) {
        return tree->type->getValue(tree->root, data);
    }

    return NULL;
}