
#include "bstitrinord.h"
#include "../stack/vec/stackvec.h"

/* ************************************************************************** */

ITRType* ConstructBSTInOrderIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &itrConstructInOrd;
    type->destruct = &itrDestructInOrd;
    type->element = &itrElementInOrd;
    type->successor = &itrSuccessorInOrd;
    type->isTerminated = &itrTerminatedInOrd;

    return type;
}

void DestructBSTInOrderIterator(ITRType* type) {
    free(type);
}

void* itrConstructInOrd(void* root) {
    BSTInOrderIterator* iter = (BSTInOrderIterator*) malloc(sizeof(BSTInOrderIterator));
    StackType* type = ConstructStackVecType();
    BSTNode *node = (BSTNode*) root;
    StackObject* stack = stkConstruct(type);

    // Mi sposto a sinistra e salvo ogni elemento nel percorso
    while(node->left) {
        DataType* ptrType = ConstructPtrDataType();
        DataObject* ptr = adtConstruct(ptrType);
        adtSetValue(ptr, node);
        stkPush(stack, ptr);
        node = node->left;
        adtDestruct(ptr);
    }

    /* Assegno current come node in quanto come caso base node = root mentre
     * se sono sceso nel percorso node = min */
    iter->current = node;
    iter->stack = stack;

    return iter;
}

void itrDestructInOrd(void* iterator) {
    BSTInOrderIterator* iter = (BSTInOrderIterator*) iterator;
    StackType* type = iter->stack->type;

    stkClear(iter->stack);
    stkDestruct(iter->stack);
    DestructStackVecType(type);
    free(iter);
}

bool itrTerminatedInOrd(void* iterator) {
    BSTInOrderIterator* iter = (BSTInOrderIterator*) iterator;

    // Ritorno true solo se non ho un corrente e lo stack è vuoto
    return !iter->current && stkEmpty(iter->stack);
}

void* itrElementInOrd(void* iterator) {
    return ((BSTInOrderIterator*) iterator)->current;
}

void itrSuccessorInOrd(void* iterator) {
    BSTInOrderIterator* iter = (BSTInOrderIterator*) iterator;

    // Se ho un nodo destro devo scendere e cercare il minimo del suo sottoalbero
    iter->current = iter->current->right;

    if(iter->current) {
        while(iter->current->left) {
            DataType* ptrType = ConstructPtrDataType();
            DataObject* ptr = adtConstruct(ptrType);
            adtSetValue(ptr, iter->current);
            stkPush(iter->stack, ptr);
            iter->current = iter->current->left;
            adtDestruct(ptr);
        }
    }
    // Se non ho figlio destro, allora devo salire
    else {
        if(!stkEmpty(iter->stack)) {
            DataObject* tmp = stkTopNPop(iter->stack);
            iter->current = adtGetValue(tmp);
            DataType* typeToDestroy = tmp->type;
            adtDestruct(tmp);
            DestructPtrDataType(typeToDestroy);
        }
        else {
            iter->current = NULL;
        }
    }
}
