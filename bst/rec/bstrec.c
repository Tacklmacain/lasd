#include "bstrec.h"

/* ************************************************************************** */

BSTType* ConstructBSTRecursive() {
    BSTType* type = (BSTType*) malloc(sizeof(BSTType));

    type->destruct = &bstDestructRec;
    type->clone = &bstCloneRec;
    type->equal = &bstEqualRec;
    type->exists = &bstExistsRec;
    type->insert = &bstInsertRec;
    type->remove = &bstRemoveRec;
    type->getMin = &bstGetMinRec;
    type->getNRemoveMin = &bstGetNRemoveMinRec;
    type->removeMin = &bstRemoveMinRec;
    type->getMax = &bstGetMaxRec;
    type->getNRemoveMax = &bstGetNRemoveMaxRec;
    type->removeMax = &bstRemoveMaxRec;
    type->getPredecessor = &bstGetPredecessorRec;
    type->getNRemovePredecessor = &bstGetNRemovePredecessorRec;
    type->removePredecessor = &bstRemovePredecessorRec;
    type->getSuccessor = &bstGetSuccessorRec;
    type->getNRemoveSuccessor = &bstGetNRemoveSuccessorRec;
    type->removeSuccessor = &bstRemoveSuccessorRec;
    type->preOrderMap = &bstPreOrderMapRec;
    type->inOrderMap = &bstInOrderMapRec;
    type->postOrderMap = &bstPostOrderMapRec;
    type->breadthMap = &bstBreadthMapRec;
    type->preOrderFold = &bstPreOrderFoldRec;
    type->inOrderFold = &bstInOrderFoldRec;
    type->postOrderFold = &bstPostOrderFoldRec;
    type->breadthFold = &bstBreadthFoldRec;
    type->getValue = &bstGetValueRec;

    return type;
}

void DestructBSTRecursive(BSTType* type) {
    free(type);
}

void bstDestructRec(void* root) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        bstDestructRec(node->left);
        bstDestructRec(node->right);

        adtDestruct(node->value);
        free(node);
    }
}

void* bstCloneRec(void* root) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        BSTNode* clone = (BSTNode*) malloc(sizeof(BSTNode));

        // Clono ricorsivamente il sottoalbero sinistro e destro ed il valore
        clone->left = bstCloneRec(node->left);
        clone->right = bstCloneRec(node->right);
        clone->value = adtClone(node->value);
        return clone;
    }
    return NULL;
}

bool bstEqualRec(void* root1, void* root2) {
    BSTNode* node1 = (BSTNode*) root1;
    BSTNode* node2 = (BSTNode*) root2;

    // Se uno dei due è NULL e l'altro no, ritorna che sono diversi
    if((node1 && !node2) || (!node1 && node2)) {
        return false;
    }

    /* Siccome se sono qui o sono entrambi diversi da NULL
     * o entrambi NULL, è sufficiente controllare solo node1 */
    if(node1) {
        if(adtCompare(node1->value, node2->value) == 0) {
            //Se è uguale a sinistra ed a destra, allora sono uguali
            if(bstEqualRec(node1->left, node2->left) && bstEqualRec(node1->right, node2->right)) {
                return true;
            }
        }
        // Se l'elemento corrente è diverso, sono diversi
        return false;
    }

    // Se arrivo qui sono entrambi NULL quindi uguali
    return true;
}

bool bstExistsRec(void* root, DataObject* key) {
    // Caso base, radice nulla
    if (root) {
        BSTNode* node = (BSTNode*) root;
        if(adtCompare(node->value, key) == 0) {
            return true;
        }
        else if(adtCompare(node->value, key) > 0) {
            return bstExistsRec(node->left, key);
        }
        else {
            return bstExistsRec(node->right, key);
        }
    }
    return false;
}

void* bstInsertRec(void* root, DataObject* newElem, bool* isInserted) {
    // Caso base senza radice
    if(!root) {
        BSTNode* newRoot = (BSTNode*) malloc(sizeof(BSTNode));

        *isInserted = true;
        newRoot->value = adtClone(newElem);
        newRoot->left = NULL;
        newRoot->right = NULL;
        root = newRoot;
    }
    // Caso ricorsivo
    else {
        BSTNode* node = (BSTNode*) root;

        if(adtCompare(node->value, newElem) > 0) {
            node->left = bstInsertRec(node->left, newElem, isInserted);
        }
        else if(adtCompare(node->value, newElem) < 0) {
            node->right = bstInsertRec(node->right, newElem, isInserted);
        }
        // Se compare == 0 non lo inserisco
    }
    return root;
}

void* bstRemoveRec(void* root, DataObject* elemToRemove, bool* isRemoved) {
    BSTNode* node = (BSTNode*) root;
    if(node) {
        if(adtCompare(node->value, elemToRemove) < 0) {
            node->right = bstRemoveRec(node->right, elemToRemove, isRemoved);
        }
        else if(adtCompare(node->value, elemToRemove) > 0) {
            node->left = bstRemoveRec(node->left, elemToRemove, isRemoved);
        }
        else {
            BSTNode* tmp = NULL;
            *isRemoved = true;

            if(node->left && node->right) {
                DataObject* dataPtr;
                node->right = bstGetNRemoveMinRec(node->right, &dataPtr);
                adtDestruct(node->value);
                node->value = adtClone(dataPtr);
                adtDestruct(dataPtr);
            }
            else if(node->right) {
                tmp = node;
                node = node->right;
            }
            else {
                tmp = node;
                node = node->left;
            }

            if(tmp) {
                adtDestruct(tmp->value);
                free(tmp);
            }
        }
    }
    return node;
}

DataObject* bstGetMinRec(void* root) {
    BSTNode* node = (BSTNode*) root;

    // Scendo finché ho un nodo a sinistra
    if(node->left) {
        return bstGetMinRec(node->left);
    }
    else {
        return adtClone(node->value);
    }
}

void* bstGetNRemoveMinRec(void* root, DataObject** value) {
    BSTNode *node = (BSTNode *) root;

    // Se ho un nodo a sinistra, continuo a scendere
    if (node->left) {
        node->left = bstGetNRemoveMinRec(node->left, value);
    }
    else if (node->right) {
        // Se non ce l'ho sono arrivato all'ultimo, controllo se ha figlio destro
        BSTNode* tmp;

        tmp = node;
        *value = tmp->value;
        node = node->right;
        free(tmp);
    }
    else {
        // Non ho figlio destro, posso eliminarlo e ritornarlo
        *value = node->value;
        free(node);
        node = NULL;
    }

    return node;
}

void* bstRemoveMinRec(void* root) {
    BSTNode *node = (BSTNode*) root, *tmp;

    // Scendo finché ho un nodo a sinistra
    if(node->left) {
        node->left = bstRemoveMinRec(node->left);
    }
    else if(node->right) {
        // Se ho figlio destro devo sovrascriverlo al nodo attuale
        tmp = node;
        node = node->right;
        adtDestruct(tmp->value);
        free(tmp);
    }
    else {
        adtDestruct(node->value);
        free(node);
        node = NULL;
    }
    return node;
}

DataObject* bstGetMaxRec(void* root) {
    BSTNode* node = (BSTNode*) root;

    // Scendo finché ho un nodo a destra
    if(node->right) {
        return bstGetMinRec(node->right);
    }
    else {
        return adtClone(node->value);
    }
}

void* bstGetNRemoveMaxRec(void* root, DataObject** value) {
    BSTNode* node = (BSTNode*) root;

    // Se ho un nodo a destra, continuo a scendere
    if (node->right) {
        node->right = bstGetNRemoveMaxRec(node->right, value);
    }
        // Se non ce l'ho sono arrivato all'ultimo, controllo se ha figlio destro
    else if (node->left) {
        BSTNode* tmp;

        tmp = node;
        *value = tmp->value;
        node = node->left;
        free(tmp);
    }
        // Non ho figlio sinistro, posso eliminarlo e ritornarlo
    else {
        *value = node->value;
        free(node);
        node = NULL;
    }

    return node;
}

void* bstRemoveMaxRec(void* root) {
    BSTNode *node = (BSTNode*) root, *tmp;

    // Scendo finché ho un nodo a destra
    if(node->right) {
        node->right = bstRemoveMinRec(node->right);
    }
    else if(node->left) {
        // Se ho figlio sinistro devo sovrascriverlo al nodo attuale
        tmp = node;
        node = node->left;
        adtDestruct(tmp->value);
        free(tmp);
    }
    else {
        adtDestruct(node->value);
        free(node);
        node = NULL;
    }
    return node;
}

DataObject* bstGetPredecessorRec(void* root, DataObject* key) {
    DataObject* pred = NULL;

    if(root) {
        BSTNode* node = (BSTNode*) root;

        if(adtCompare(node->value, key) == 0) {
            if(node->left) {
                pred = bstGetMaxRec(node->left);
            }
        }
        else if(adtCompare(node->value, key) < 0) {
            pred = bstGetPredecessorRec(node->right, key);
            if(!pred) {
                pred = adtClone(node->value);
            }
        }
        else {
            pred = bstGetPredecessorRec(node->left, key);
        }
    }
    return pred;
}

void* bstGetNRemovePredecessorRec(void* root, DataObject* key, DataObject** value, bool* isRemoved) {
    BSTNode* node = (BSTNode*) root;

    if(node) {
        if(adtCompare(node->value, key) == 0) {
            if(node->left) {
                node->left = bstGetNRemoveMaxRec(node->left, value);
                *isRemoved = true;
            }
        }
        else if(adtCompare(node->value, key) < 0) {
            node->right = bstGetNRemovePredecessorRec(node->right, key, value, isRemoved);

            if(!(*isRemoved)) {
                *value = adtClone(node->value);
                BSTNode* tmp;
                *isRemoved = true;

                if(node->left && node->right) {
                    tmp = removeMin(node->right, node);
                    node->value = adtClone(tmp->value);
                }
                else if(node->right) {
                    tmp = node;
                    node = node->right;
                }
                else {
                    tmp = node;
                    node = node->left;
                }

                adtDestruct(tmp->value);
                free(tmp);
            }
        }
        else {
            node->left = bstGetNRemovePredecessorRec(node->left, key, value, isRemoved);
        }
    }
    return node;
}

void* bstRemovePredecessorRec(void* root, DataObject* key, bool* isRemoved) {
    BSTNode* node = (BSTNode*) root;

    if(node) {
        if(adtCompare(node->value, key) == 0) {
            if(node->left) {
                node->left = bstRemoveMaxRec(node->left);
                *isRemoved = true;
            }
        }
        else if(adtCompare(node->value, key) < 0) {
            node->right = bstRemovePredecessorRec(node->right, key, isRemoved);

            if(!(*isRemoved)) {
                BSTNode* tmp;
                *isRemoved = true;

                if(node->left && node->right) {
                    tmp = removeMin(node->right, node);
                    node->value = adtClone(tmp->value);
                }
                else if(node->right) {
                    tmp = node;
                    node = node->right;
                }
                else {
                    tmp = node;
                    node = node->left;
                }

                adtDestruct(tmp->value);
                free(tmp);
            }
        }
        else {
            node->left = bstRemovePredecessorRec(node->left, key, isRemoved);
        }
    }
    return node;
}

DataObject* bstGetSuccessorRec(void* root, DataObject* key) {
    DataObject* succ = NULL;

    if(root) {
        BSTNode* node = (BSTNode*) root;

        if(adtCompare(node->value, key) == 0) {
            if(node->right) {
                succ = bstGetMinRec(node->right);
            }
        }
        else if(adtCompare(node->value, key) < 0) {
            succ = bstGetSuccessorRec(node->right, key);
        }
        else {
            succ = bstGetSuccessorRec(node->left, key);
            if(!succ) {
                succ = adtClone(node->value);
            }
        }
    }
    return succ;
}

void* bstGetNRemoveSuccessorRec(void* root, DataObject* key, DataObject** value, bool* isRemoved) {
    BSTNode* node = (BSTNode*) root;

    if(node) {
        if(adtCompare(node->value, key) == 0) {
            if(node->right) {
                node->right = bstGetNRemoveMinRec(node->right, value);
                *isRemoved = true;
            }
        }
        else if(adtCompare(node->value, key) > 0) {
            node->left = bstGetNRemoveSuccessorRec(node->left, key, value, isRemoved);

            if(!(*isRemoved)) {
                *value = adtClone(node->value);
                BSTNode* tmp;
                *isRemoved = true;

                if(node->left && node->right) {
                    tmp = removeMin(node->right, node);
                    node->value = adtClone(tmp->value);
                }
                else if(node->right) {
                    tmp = node;
                    node = node->right;
                }
                else {
                    tmp = node;
                    node = node->left;
                }

                adtDestruct(tmp->value);
                free(tmp);
            }
        }
        else {
            node->right = bstGetNRemoveSuccessorRec(node->right, key, value, isRemoved);
        }
    }
    return node;
}

void* bstRemoveSuccessorRec(void* root, DataObject* key, bool* isRemoved) {
    BSTNode* node = (BSTNode*) root;

    if(node) {
        if(adtCompare(node->value, key) == 0) {
            if(node->right) {
                node->right = bstRemoveMinRec(node->right);
                *isRemoved = true;
            }
        }
        else if(adtCompare(node->value, key) > 0) {
            node->left = bstRemoveSuccessorRec(node->left, key, isRemoved);

            if(!(*isRemoved)) {
                BSTNode* tmp;
                *isRemoved = true;

                if(node->left && node->right) {
                    tmp = removeMin(node->right, node);
                    node->value = adtClone(tmp->value);
                }
                else if(node->right) {
                    tmp = node;
                    node = node->right;
                }
                else {
                    tmp = node;
                    node = node->left;
                }

                adtDestruct(tmp->value);
                free(tmp);
            }
        }
        else {
            node->right = bstRemoveSuccessorRec(node->right, key, isRemoved);
        }
    }
    return node;
}

void bstPreOrderMapRec(void* root, MapFun mapFunction, void* parameter) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        mapFunction(node->value, parameter);
        bstPreOrderMapRec(node->left, mapFunction, parameter);
        bstPreOrderMapRec(node->right, mapFunction, parameter);
    }
}

void bstInOrderMapRec(void* root, MapFun mapFunction, void* parameter) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        bstInOrderMapRec(node->left, mapFunction, parameter);
        mapFunction(node->value, parameter);
        bstInOrderMapRec(node->right, mapFunction, parameter);
    }
}

void bstPostOrderMapRec(void* root, MapFun mapFunction, void* parameter) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        bstPostOrderMapRec(node->left, mapFunction, parameter);
        bstPostOrderMapRec(node->right, mapFunction, parameter);
        mapFunction(node->value, parameter);
    }
}

void bstBreadthMapRec(void* root, MapFun mapFunction, void* parameter) {
    if(root) {
        QueueVec* queue = queVecConstruct();
        DataType* ptrType = ConstructPtrDataType();
        DataObject* tmp = adtConstruct(ptrType);

        // Inizio ad incodare la root
        adtSetValue(tmp, root);
        queVecEnqueue(queue, tmp);

        while(!queVecEmpty(queue)) {
            // Prendo e rimuovo la testa, assegnandola in una variabile temporanea
            DataObject* tmpDO = queVecHeadNDequeue(queue);
            BSTNode* node = (BSTNode*) adtGetValue(tmpDO);

            // Eseguo l'operazione sul nodo attuale
            mapFunction(node->value, parameter);

            // Se ho elementi a sinistra o destra, li incodo
            if(node->left) {
                adtSetValue(tmp, node->left);
                queVecEnqueue(queue, tmp);
            }
            if(node->right) {
                adtSetValue(tmp, node->right);
                queVecEnqueue(queue, tmp);
            }
            adtDestruct(tmpDO);
        }

        // Distruggo le strutture di appoggio utilizzate
        adtDestruct(tmp);
        DestructPtrDataType(ptrType);
        queVecDestruct(queue);
    }
}

void bstPreOrderFoldRec(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        foldFunction(node->value, accumulator, parameter);
        bstPreOrderFoldRec(node->left, foldFunction, accumulator, parameter);
        bstPreOrderFoldRec(node->right, foldFunction, accumulator, parameter);
    }
}

void bstInOrderFoldRec(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        bstInOrderFoldRec(node->left, foldFunction, accumulator, parameter);
        foldFunction(node->value, accumulator, parameter);
        bstInOrderFoldRec(node->right, foldFunction, accumulator, parameter);
    }
}

void bstPostOrderFoldRec(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    if(root) {
        BSTNode* node = (BSTNode*) root;
        bstPostOrderFoldRec(node->left, foldFunction, accumulator, parameter);
        bstPostOrderFoldRec(node->right, foldFunction, accumulator, parameter);
        foldFunction(node->value, accumulator, parameter);
    }
}

void bstBreadthFoldRec(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    if(root) {
        QueueVec* queue = queVecConstruct();
        DataType* ptrType = ConstructPtrDataType();
        DataObject* tmp = adtConstruct(ptrType);

        // Inizio ad incodare la root
        adtSetValue(tmp, root);
        queVecEnqueue(queue, tmp);

        while(!queVecEmpty(queue)) {
            // Prendo e rimuovo la testa, assegnandola in una variabile temporanea
            DataObject* tmpDO = queVecHeadNDequeue(queue);
            BSTNode* node = (BSTNode*) adtGetValue(tmpDO);

            // Eseguo l'operazione sul nodo attuale
            foldFunction(node->value, accumulator, parameter);

            // Se ho elementi a sinistra o destra, li incodo
            if(node->left) {
                adtSetValue(tmp, node->left);
                queVecEnqueue(queue, tmp);
            }
            if(node->right) {
                adtSetValue(tmp, node->right);
                queVecEnqueue(queue, tmp);
            }
            adtDestruct(tmpDO);
        }

        // Distruggo le strutture di appoggio utilizzate
        adtDestruct(tmp);
        DestructPtrDataType(ptrType);
        queVecDestruct(queue);
    }
}

// Funzioni custom
DataObject* bstGetValueRec(void* root, DataObject* key) {
    // Caso base, radice nulla
    if (root) {
        BSTNode* node = (BSTNode*) root;
        if(adtCompare(node->value, key) == 0) {
            return adtClone(node->value);
        }
        else if(adtCompare(node->value, key) > 0) {
            return bstGetValueRec(node->left, key);
        }
        else {
            return bstGetValueRec(node->right, key);
        }
    }
    return NULL;
}


/* ************************************************************************** */
// Funzioni accessorie

void* removeMin(BSTNode* node, BSTNode* prev) {
    if (node) {
        if (node->left) {
            return removeMin(node->left, node);
        }
        else {
            if (node == prev->left) {
                prev->left = node->right;
            }
            else {
                prev->right = node->right;
            }
        }
    }
    return node;
}
