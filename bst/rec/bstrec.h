#ifndef BSTREC_H
#define BSTREC_H

/* ************************************************************************** */

#include "../bst.h"

/* ************************************************************************** */

BSTType* ConstructBSTRecursive();
void DestructBSTRecursive(BSTType*);

void bstDestructRec(void*);

void* bstCloneRec(void*);
bool bstEqualRec(void*, void*);
bool bstExistsRec(void*, DataObject*);

void* bstInsertRec(void*, DataObject*, bool*);
void* bstRemoveRec(void*, DataObject*, bool*);

DataObject* bstGetMinRec(void*);
void* bstGetNRemoveMinRec(void*, DataObject**);
void* bstRemoveMinRec(void*);
DataObject* bstGetMaxRec(void*);
void* bstGetNRemoveMaxRec(void*, DataObject**);
void* bstRemoveMaxRec(void*);

DataObject* bstGetPredecessorRec(void*, DataObject*);
void* bstGetNRemovePredecessorRec(void*, DataObject*, DataObject**, bool*);
void* bstRemovePredecessorRec(void*, DataObject*, bool*);

DataObject* bstGetSuccessorRec(void*, DataObject*);
void* bstGetNRemoveSuccessorRec(void*, DataObject*, DataObject**, bool*);
void* bstRemoveSuccessorRec(void*, DataObject*, bool*);

void bstPreOrderMapRec(void*, MapFun, void*);
void bstInOrderMapRec(void*, MapFun, void*);
void bstPostOrderMapRec(void*, MapFun, void*);
void bstBreadthMapRec(void*, MapFun, void*);
void bstPreOrderFoldRec(void*, FoldFun, void*, void*);
void bstInOrderFoldRec(void*, FoldFun, void*, void*);
void bstPostOrderFoldRec(void*, FoldFun, void*, void*);
void bstBreadthFoldRec(void*, FoldFun, void*, void*);

// Customs
DataObject* bstGetValueRec(void*, DataObject*);

/* ************************************************************************** */
void* removeMin(BSTNode*, BSTNode*);

#endif
