
#ifndef BSTITRPOSTORD_H
#define BSTITRPOSTORD_H

/* ************************************************************************** */

#include "../itr/itr.h"
#include "../stack/stack.h"
#include "bst.h"

/* ************************************************************************** */

typedef struct BSTPostOrderIterator {
    BSTNode* current;
    BSTNode* last;
    StackObject* stack;
} BSTPostOrderIterator;

/* ************************************************************************** */

ITRType* ConstructBSTPostOrderIterator();
void DestructBSTPostOrderIterator(ITRType*);

void* itrConstructPostOrd(void*);
void itrDestructPostOrd(void*);
bool itrTerminatedPostOrd(void*);
void* itrElementPostOrd(void*);
void itrSuccessorPostOrd(void*);

/* ************************************************************************** */

#endif
