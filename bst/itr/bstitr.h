
#ifndef BSTITR_H
#define BSTITR_H

/* ************************************************************************** */

#include "../bst.h"
#include "../../adt/ptr/adtptr.h"

/* ************************************************************************** */

BSTType* ConstructBSTIterative();
void DestructBSTIterative(BSTType*);

void bstDestructIter(void*);

void* bstCloneIter(void*);
bool bstEqualIter(void*, void*);
bool bstExistsIter(void*, DataObject*);

void* bstInsertIter(void*, DataObject*, bool*);
void* bstRemoveIter(void*, DataObject*, bool*);

DataObject* bstGetMinIter(void*);
void* bstGetNRemoveMinIter(void*, DataObject**);
void* bstRemoveMinIter(void*);
DataObject* bstGetMaxIter(void*);
void* bstGetNRemoveMaxIter(void*, DataObject**);
void* bstRemoveMaxIter(void*);

DataObject* bstGetPredecessorIter(void*, DataObject*);
void* bstGetNRemovePredecessorIter(void*, DataObject*, DataObject**, bool*);
void* bstRemovePredecessorIter(void*, DataObject*, bool*);

DataObject* bstGetSuccessorIter(void*, DataObject*);
void* bstGetNRemoveSuccessorIter(void*, DataObject*, DataObject**, bool*);
void* bstRemoveSuccessorIter(void*, DataObject*, bool*);

void bstPreOrderMapIter(void*, MapFun, void*);
void bstInOrderMapIter(void*, MapFun, void*);
void bstPostOrderMapIter(void*, MapFun, void*);
void bstBreadthMapIter(void*, MapFun, void*);
void bstPreOrderFoldIter(void*, FoldFun, void*, void*);
void bstInOrderFoldIter(void*, FoldFun, void*, void*);
void bstPostOrderFoldIter(void*, FoldFun, void*, void*);
void bstBreadthFoldIter(void*, FoldFun, void*, void*);

/* ************************************************************************** */

void* RemoveRoot(void*);

#endif
