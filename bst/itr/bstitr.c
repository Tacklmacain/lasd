#include "bstitr.h"
#include "../bstitrpreord.h"
#include "../bstitrinord.h"
#include "../bstitrpostord.h"
#include "../bstitrbreadth.h"
#include "../../stack/vec/stackvec.h"
#include "../rec/bstrec.h"

/* ************************************************************************** */

BSTType* ConstructBSTIterative() {
    BSTType* type = (BSTType*) malloc(sizeof(BSTType));

    type->destruct = &bstDestructIter;
    type->clone = &bstCloneIter;
    type->equal = &bstEqualIter;
    type->exists = &bstExistsIter;
    type->insert = &bstInsertIter;
    type->remove = &bstRemoveIter;
    type->getMin = &bstGetMinIter;
    type->getNRemoveMin = &bstGetNRemoveMinIter;
    type->removeMin = &bstRemoveMinIter;
    type->getMax = &bstGetMaxIter;
    type->getNRemoveMax = &bstGetNRemoveMaxIter;
    type->removeMax = &bstRemoveMaxIter;
    type->getPredecessor = &bstGetPredecessorIter;
    type->getNRemovePredecessor = &bstGetNRemovePredecessorIter;
    type->removePredecessor = &bstRemovePredecessorIter;
    type->getSuccessor = &bstGetSuccessorIter;
    type->getNRemoveSuccessor = &bstGetNRemoveSuccessorIter;
    type->removeSuccessor = &bstRemoveSuccessorIter;
    type->preOrderMap = &bstPreOrderMapIter;
    type->inOrderMap = &bstInOrderMapIter;
    type->postOrderMap = &bstPostOrderMapIter;
    type->breadthMap = &bstBreadthMapIter;
    type->preOrderFold = &bstPreOrderFoldIter;
    type->inOrderFold = &bstInOrderFoldIter;
    type->postOrderFold = &bstPostOrderFoldIter;
    type->breadthFold = &bstBreadthFoldIter;
    type->getValue = &bstGetValueRec; // Siccome è una funzione custom non è richiesta esplicitamente iterativa

    return type;
}

void DestructBSTIterative(BSTType* type) {
    free(type);
}

void bstDestructIter(void* root) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTPostOrderIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        adtDestruct(node->value);
        free(node);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTPostOrderIterator(itrType);
}

void* bstCloneIter(void* root) {
    BSTNode* node = (BSTNode*) root;

    // Se ho il nodo inizio a creare il clone
    if(node) {
        StackType* stkType = ConstructStackVecType();
        StackObject* stack = stkConstruct(stkType);
        DataType* ptrType = ConstructPtrDataType();
        DataObject *ptr = adtConstruct(ptrType), *data;
        BSTNode *clone, *newClone;

        clone = (BSTNode *) malloc(sizeof(BSTNode));
        clone->value = adtClone(node->value);
        clone->left = NULL;
        clone->right = NULL;

        BSTNode *newRoot = clone;

        // Finché node ha un valore continuo a duplicare
        while (node) {
            // Se non ho clonato il nodo sinistro, lo clono
            if (node->left && !clone->left) {
                newClone = (BSTNode *) malloc(sizeof(BSTNode));
                newClone->value = adtClone(node->left->value);
                newClone->left = NULL;
                newClone->right = NULL;
                clone->left = newClone;

                adtSetValue(ptr, clone);
                stkPush(stack, ptr);

                adtSetValue(ptr, node);
                stkPush(stack, ptr);

                node = node->left;
                clone = clone->left;
            } // Stessa cosa a dx
            else if (node->right && !clone->right) {
                newClone = (BSTNode *) malloc(sizeof(BSTNode));
                newClone->value = adtClone(node->right->value);
                newClone->left = NULL;
                newClone->right = NULL;
                clone->right = newClone;

                adtSetValue(ptr, clone);
                stkPush(stack, ptr);

                adtSetValue(ptr, node);
                stkPush(stack, ptr);

                node = node->right;
                clone = clone->right;
            }
            // Se arrivo qui allora o sono entrambi null o nessuno dei due
            else {
                // Se ci sono elementi nello stack posso salire
                if (!stkEmpty(stack)) {
                    data = stkTopNPop(stack);
                    node = (BSTNode *) adtGetValue(data);
                    adtDestruct(data);

                    data = stkTopNPop(stack);
                    clone = (BSTNode *) adtGetValue(data);
                    adtDestruct(data);
                }
                else {
                    node = NULL;
                    clone = NULL;
                }
            }
        }

        adtDestruct(ptr);
        stkDestruct(stack);
        DestructStackVecType(stkType);
        DestructPtrDataType(ptrType);
        return newRoot;
    }
    // Se la root è null allora torno null
    return NULL;
}

bool bstEqualIter(void* root1, void* root2) {
    BSTNode *node1 = (BSTNode*) root1, *node2 = (BSTNode*) root2;

    ITRType* itrType = ConstructBSTBreadthIterator();
    ITRObject* iter1 = itrConstruct(itrType, node1);
    ITRObject* iter2 = itrConstruct(itrType, node2);

    BSTNode *tmp1, *tmp2;

    // Finché uno dei due non termina scorro gli elementi in sincrono
    while(!itrTerminated(iter1) && !itrTerminated(iter2)) {
        tmp1 = itrElement(iter1);
        tmp2 = itrElement(iter2);

        // Se incontro un nodo differente distruggo gli iteratori e ritorno che sono diversi
        if(adtCompare(tmp1->value, tmp2->value) != 0) {
            itrDestruct(iter1);
            itrDestruct(iter2);
            DestructBSTBreadthIterator(itrType);
            return false;
        }

        itrSuccessor(iter1);
        itrSuccessor(iter2);
    }

    bool terminated1 = !itrTerminated(iter1), terminated2 = !itrTerminated(iter2);

    itrDestruct(iter1);
    itrDestruct(iter2);
    DestructBSTBreadthIterator(itrType);

    // Se è terminato solo uno dei due allora è falso
    if(terminated1 || terminated2) {
        return false;
    }

    return true;
}

bool bstExistsIter(void* root, DataObject* key) {
    BSTNode* curr = (BSTNode*) root;

    // Ciclo finché non trovo l'elemento o arrivo alle foglie
    while(curr) {
        // Mi sposto nell'albero in base al compare
        if(adtCompare(curr->value, key) > 0) {
            curr = curr->left;
        }
        else if (adtCompare(curr->value, key) < 0){
            curr = curr->right;
        }
        else {
            return true;
        }
    }
    // Se ciclando non lo trovo, ritorno false
    return false;
}

void* bstInsertIter(void* root, DataObject* newElem, bool* isInserted) {
    BSTNode *curr = (BSTNode*) root, *pred = NULL;

    while(curr) {
        pred = curr;
        if(adtCompare(curr->value, newElem) > 0) {
            curr = curr->left;
        }
        else if(adtCompare(curr->value, newElem) < 0) {
            curr = curr->right;
        }
        else {
            // Se arrivo qui non lo inserisco ma ritorno la root invariata
            return root;
        }
    }

    // Se arrivo qui allora posso aggiungere il nuovo nodo
    *isInserted = true;
    BSTNode* newNode = (BSTNode*) malloc(sizeof(BSTNode));
    newNode->value = adtClone(newElem);
    newNode->left = NULL;
    newNode->right = NULL;

    /* Se prevRoot allora lo inserisco a sx o dx, altrimenti è il caso base
     * ovvero è la radice e lo aggiungo come tale */
    if(pred) {
        if(adtCompare(pred->value, newElem) > 0) {
            pred->left = newNode;
        }
        else if(adtCompare(pred->value, newElem) < 0) {
            pred->right = newNode;
        }
        return root;
    }
    else {
        return newNode;
    }
}

void* bstRemoveIter(void* root, DataObject* elemToRemove, bool* isRemoved) {
    BSTNode *pred = NULL, *curr = root;

    while (curr) {
        if (adtCompare(curr->value, elemToRemove) > 0) {
            pred = curr;
            curr = curr->left;
        } else if (adtCompare(curr->value, elemToRemove) < 0) {
            pred = curr;
            curr = curr->right;
        } else {
            *isRemoved = true;
            if (pred) {
                if (pred->left == curr) {
                    pred->left = RemoveRoot(pred->left);
                } else {
                    pred->right = RemoveRoot(pred->right);
                }
                curr = NULL;
            } else {
                return RemoveRoot(curr);
            }
        }
    }
    return root;
}

DataObject* bstGetMinIter(void* root) {
    BSTNode* curr = (BSTNode*) root;

    while(curr && curr->left) {
        curr = curr->left;
    }

    if(curr) {
        return adtClone(curr->value);
    }
    return NULL;
}

void* bstGetNRemoveMinIter(void* root, DataObject** value) {
    BSTNode *curr = (BSTNode*) root, *pred = NULL, *tmp;

    while(curr && curr->left) {
        pred = curr;
        curr = curr->left;
    }

    // Verifico se mi trovo nell'albero e devo prendere il sottoalbero dx
    if(curr && pred) {
        *value = curr->value;
        pred->left = curr->right;
        free(curr);
        return root;
    }
    // Oppure se il mio minimo è proprio la radice stessa
    else if(curr) {
        *value = curr->value;
        tmp = curr->right;
        free(curr);
        return tmp;
    }

    return NULL;
}

void* bstRemoveMinIter(void* root) {
    BSTNode *curr = (BSTNode*) root, *pred = NULL, *tmp;

    while(curr && curr->left) {
        pred = curr;
        curr = curr->left;
    }

    // Verifico se mi trovo nell'albero e devo prendere il sottoalbero dx
    if(curr && pred) {
        adtDestruct(curr->value);
        pred->left = curr->right;
        free(curr);
        return root;
    }
    // Oppure se il mio minimo è proprio la radice stessa
    else if(curr) {
        adtDestruct(curr->value);
        tmp = curr->right;
        free(curr);
        return tmp;
    }

    return NULL;
}

DataObject* bstGetMaxIter(void* root) {
    BSTNode* curr = (BSTNode*) root;

    while(curr && curr->right) {
        curr = curr->right;
    }

    if(curr) {
        return adtClone(curr->value);
    }
    return NULL;
}

void* bstGetNRemoveMaxIter(void* root, DataObject** value) {
    BSTNode *curr = (BSTNode*) root, *pred = NULL, *tmp;

    while(curr && curr->right) {
        pred = curr;
        curr = curr->right;
    }

    // Verifico se mi trovo nell'albero e devo prendere il sottoalbero sx
    if(curr && pred) {
        *value = curr->value;
        pred->right = curr->left;
        free(curr);
        return root;
    }
    // Oppure se il mio massimo è proprio la radice stessa
    else if(curr) {
        *value = curr->value;
        tmp = curr->left;
        free(curr);
        return tmp;
    }

    return NULL;
}

void* bstRemoveMaxIter(void* root) {
    BSTNode *curr = (BSTNode*) root, *pred = NULL, *tmp;

    while(curr && curr->right) {
        pred = curr;
        curr = curr->right;
    }

    // Verifico se mi trovo nell'albero e devo prendere il sottoalbero sx
    if(curr && pred) {
        adtDestruct(curr->value);
        pred->right = curr->left;
        free(curr);
        return root;
    }
        // Oppure se il mio massimo è proprio la radice stessa
    else if(curr) {
        adtDestruct(curr->value);
        tmp = curr->left;
        free(curr);
        return tmp;
    }

    return NULL;
}

DataObject* bstGetPredecessorIter(void* root, DataObject* key) {
    BSTNode* curr = (BSTNode*) root, *pred = NULL;

    while(curr && adtCompare(curr->value, key) != 0) {
        if(adtCompare(curr->value, key) > 0) {
            curr = curr->left;
        }
        else {
            pred = curr;
            curr = curr->right;
        }
    }

    /* Se arrivo qui e curr o curr->left sono nulli allora potrei averlo trovato*/
    if(!curr || !curr->left) {
        if(pred) {
            return adtClone(pred->value);
        }
        return NULL;
    }
    /* Altrimenti ho trovato l'elemento stesso nell'albero e mi prendo il max di sinistra */
    return bstGetMaxIter(curr->left);
}

void* bstGetNRemovePredecessorIter(void* root, DataObject* key, DataObject** value, bool* isRemoved) {
    BSTNode *node = (BSTNode*) root, *succ = NULL, *father = NULL, *fatherSucc = NULL;

    // Ricerca dell'elemento di cui si vuole il predecessore
    while(node && adtCompare(node->value, key) != 0) {
        if(adtCompare(node->value, key) < 0) {
            fatherSucc = father;
            father = node;
            succ = node;
            node = node->right;
        }
        else {
            father = node;
            node = node->left;
        }
    }

    // Se arrivo qui devo stabilire se root è null o perché l'ho trovato
    if(!node || !node->left) {
        if(succ) {
            *isRemoved = true;
            BSTNode* tmp = NULL;

            /* Se ha un figlio sinistro o destro, provvedo a sostituirlo con il
             * massimo del sottoalbero sinistro */
            if(succ->left && succ->right) {
                DataObject* dataTmp;
                succ->left = bstGetNRemoveMaxIter(succ->left, &dataTmp);
                *value = succ->value;
                succ->value = dataTmp;
            }
            // Altrimenti procedo con l'eliminazione standard
            else if(succ->left) {
                tmp = succ;
                succ = succ->left;
            }
            else {
                tmp = succ;
                succ = succ->right;
            }

            // Gestisco il caso in cui l'elemento da rimuovere è la radice stessa
            if(!fatherSucc) {
                root = succ;
                if(tmp) {
                    *value = tmp->value;
                    free(tmp);
                }
            }
            else if(tmp) {
                if(fatherSucc->left == tmp) {
                    fatherSucc->left = succ;
                }
                else {
                    fatherSucc->right = succ;
                }
                *value = tmp->value;
                free(tmp);
            }
        }
        else {
            return root;
        }
    }
    else {
        /* Se la chiave è un elemento dell'albero ed ha un sottoalbero sinistro,
         * il predecessore è il max di quest'ultimo */
        *isRemoved = true;
        node->left = bstGetNRemoveMaxIter(node->left, value);
    }

    return root;
}

void* bstRemovePredecessorIter(void* root, DataObject* key, bool* isRemoved) {
    BSTNode *node = (BSTNode*) root, *succ = NULL, *father = NULL, *fatherSucc = NULL;

    while(node && adtCompare(node->value, key) != 0) {
        if(adtCompare(node->value, key) < 0) {
            fatherSucc = father;
            father = node;
            succ = node;
            node = node->right;
        }
        else {
            father = node;
            node = node->left;
        }
    }

    if(!node || !node->left) {
        if(succ) {
            *isRemoved = true;
            BSTNode* tmp = NULL;

            if(succ->left && succ->right) {
                DataObject* dataTmp;
                succ->left = bstGetNRemoveMaxIter(succ->left, &dataTmp);
                adtDestruct(succ->value);
                succ->value = dataTmp;
            }
            else if(succ->left) {
                tmp = succ;
                succ = succ->left;
            }
            else {
                tmp = succ;
                succ = succ->right;
            }

            if(!fatherSucc) {
                root = succ;
                if(tmp) {
                    adtDestruct(tmp->value);
                    tmp->left = NULL;
                    tmp->right = NULL;
                    free(tmp);
                }
            }
            else if(tmp) {
                if(fatherSucc->left == tmp) {
                    fatherSucc->left = succ;
                }
                else {
                    fatherSucc->right = succ;
                }
                adtDestruct(tmp->value);
                tmp->left = NULL;
                tmp->right = NULL;
                free(tmp);
            }
        }
        else {
            return root;
        }
    }
    else {
        *isRemoved = true;
        node->left = bstRemoveMaxIter(node->left);
    }

    return root;
}

DataObject* bstGetSuccessorIter(void* root, DataObject* key) {
    BSTNode* curr = (BSTNode*) root, *succ = NULL;

    while(curr && adtCompare(curr->value, key) != 0) {
        if(adtCompare(curr->value, key) < 0) {
            curr = curr->right;
        }
        else {
            succ = curr;
            curr = curr->left;
        }
    }

    if(!curr || !curr->right) {
        if(succ) {
            return adtClone(succ->value);
        }
        return NULL;
    }
    return bstGetMinIter(curr->right);
}

void* bstGetNRemoveSuccessorIter(void* root, DataObject* key, DataObject** value, bool* isRemoved) {
    BSTNode *node = (BSTNode*) root, *succ = NULL, *father = NULL, *fatherSucc = NULL;

    while(node && adtCompare(node->value, key) != 0) {
        if(adtCompare(node->value, key) > 0) {
            fatherSucc = father;
            father = node;
            succ = node;
            node = node->left;
        }
        else {
            father = node;
            node = node->right;
        }
    }

    if(!node || !node->right) {
        if(succ) {
            *isRemoved = true;
            BSTNode* tmp = NULL;

            if(succ->left && succ->right) {
                DataObject* dataTmp;
                succ->right = bstGetNRemoveMinIter(succ->right, &dataTmp);
                *value = succ->value;
                succ->value = dataTmp;
            }
            else if(succ->right) {
                tmp = succ;
                succ = succ->right;
            }
            else {
                tmp = succ;
                succ = succ->left;
            }

            if(!fatherSucc) {
                root = succ;
                if(tmp) {
                    *value = tmp->value;
                    free(tmp);
                }
            }
            else if(tmp) {
                if(fatherSucc->left == tmp) {
                    fatherSucc->left = succ;
                }
                else {
                    fatherSucc->right = succ;
                }
                *value = tmp->value;
                free(tmp);
            }
        }
        else {
            return root;
        }
    }
    else {
        *isRemoved = true;
        node->right = bstGetNRemoveMinIter(node->right, value);
    }

    return root;
}

void* bstRemoveSuccessorIter(void* root, DataObject* key, bool* isRemoved) {
    BSTNode *node = (BSTNode*) root, *succ = NULL, *father = NULL, *fatherSucc = NULL;

    while(node && adtCompare(node->value, key) != 0) {
        if(adtCompare(node->value, key) > 0) {
            fatherSucc = father;
            father = node;
            succ = node;
            node = node->left;
        }
        else {
            father = node;
            node = node->right;
        }
    }

    if(!node || !node->right) {
        if(succ) {
            *isRemoved = true;
            BSTNode* tmp = NULL;

            if(succ->left && succ->right) {
                DataObject* dataTmp;
                succ->right = bstGetNRemoveMinIter(succ->right, &dataTmp);
                adtDestruct(succ->value);
                succ->value = dataTmp;
            }
            else if(succ->right) {
                tmp = succ;
                succ = succ->right;
            }
            else {
                tmp = succ;
                succ = succ->left;
            }

            if(!fatherSucc) {
                root = succ;
                if(tmp) {
                    adtDestruct(tmp->value);
                    free(tmp);
                }
            }
            else if(tmp) {
                if(fatherSucc->left == tmp) {
                    fatherSucc->left = succ;
                }
                else {
                    fatherSucc->right = succ;
                }
                adtDestruct(tmp->value);
                free(tmp);
            }
        }
        else {
            return root;
        }
    }
    else {
        *isRemoved = true;
        node->right = bstRemoveMinIter(node->right);
    }

    return root;
}

void bstPreOrderMapIter(void* root, MapFun mapFunction, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTPreOrderIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        mapFunction(node->value, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTPreOrderIterator(itrType);
}

void bstInOrderMapIter(void* root, MapFun mapFunction, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTInOrderIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        mapFunction(node->value, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTInOrderIterator(itrType);
}

void bstPostOrderMapIter(void* root, MapFun mapFunction, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTPostOrderIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        mapFunction(node->value, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTPostOrderIterator(itrType);
}

void bstBreadthMapIter(void* root, MapFun mapFunction, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTBreadthIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        mapFunction(node->value, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTBreadthIterator(itrType);
}

void bstPreOrderFoldIter(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTPreOrderIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        foldFunction(node->value, accumulator, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTPreOrderIterator(itrType);
}

void bstInOrderFoldIter(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTInOrderIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        foldFunction(node->value, accumulator, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTInOrderIterator(itrType);
}

void bstPostOrderFoldIter(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTPostOrderIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        foldFunction(node->value, accumulator, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTPostOrderIterator(itrType);
}

void bstBreadthFoldIter(void* root, FoldFun foldFunction, void* accumulator, void* parameter) {
    BSTNode* node = (BSTNode*) root;

    ITRType* itrType = ConstructBSTBreadthIterator();
    ITRObject* iter = itrConstruct(itrType, node);

    while(!itrTerminated(iter)) {
        node = (BSTNode*) itrElement(iter);
        foldFunction(node->value, accumulator, parameter);
        itrSuccessor(iter);
    }

    itrDestruct(iter);
    DestructBSTBreadthIterator(itrType);
}

/* ************************************************************************** */

// Funzione accessoria per removeIter
void* RemoveRoot(void* root) {
    BSTNode* node = (BSTNode*) root;

    if (node) {
        if (node->left && node->right) {
            DataObject* tmpData;
            node->right = bstGetNRemoveMinIter(node->right, &tmpData);
            adtDestruct(node->value);
            node->value = tmpData;
        }
        else {
            BSTNode* tmp = node;
            adtDestruct(node->value);
            node = !(node->left) ? node->right : node->left;
            free(tmp);
        }
    }
    return node;
}