
#ifndef BSTITRINORD_H
#define BSTITRINORD_H

/* ************************************************************************** */

#include "../itr/itr.h"
#include "../stack/stack.h"
#include "bst.h"

/* ************************************************************************** */

typedef struct BSTInOrderIterator {
    BSTNode* current;
    StackObject* stack;
} BSTInOrderIterator;

/* ************************************************************************** */

ITRType* ConstructBSTInOrderIterator();
void DestructBSTInOrderIterator(ITRType*);

void* itrConstructInOrd(void*);
void itrDestructInOrd(void*);
bool itrTerminatedInOrd(void*);
void* itrElementInOrd(void*);
void itrSuccessorInOrd(void*);

/* ************************************************************************** */

#endif
