#include "bstitrpostord.h"
#include "../stack/vec/stackvec.h"

/* ************************************************************************** */

ITRType* ConstructBSTPostOrderIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &itrConstructPostOrd;
    type->destruct = &itrDestructPostOrd;
    type->element = &itrElementPostOrd;
    type->successor = &itrSuccessorPostOrd;
    type->isTerminated = &itrTerminatedPostOrd;

    return type;
}

void DestructBSTPostOrderIterator(ITRType* type) {
    free(type);
}

void* itrConstructPostOrd(void* root) {
    BSTPostOrderIterator* iter = (BSTPostOrderIterator*) malloc(sizeof(BSTPostOrderIterator));
    BSTNode *node = (BSTNode*) root;
    StackType* type = ConstructStackVecType();
    StackObject* stack = stkConstruct(type);

    // Scendo finché ho nodi destri o sinistri
    while(node->left || node->right) {
        DataType* ptrType = ConstructPtrDataType();
        DataObject* ptr = adtConstruct(ptrType);

        adtSetValue(ptr, node);
        stkPush(stack, ptr);
        adtDestruct(ptr);

        node = (node->left) ? node->left : node->right;
    }

    /* Assegno current come node in quanto come caso base node = root mentre
     * se sono sceso nel percorso node = min */
    iter->current = node;
    iter->last = node;
    iter->stack = stack;

    return iter;
}

void itrDestructPostOrd(void* iterator) {
    BSTPostOrderIterator* iter = (BSTPostOrderIterator*) iterator;
    StackType* type = iter->stack->type;

    stkClear(iter->stack);
    stkDestruct(iter->stack);
    DestructStackVecType(type);
    free(iter);
}

bool itrTerminatedPostOrd(void* iterator) {
    BSTPostOrderIterator* iter = (BSTPostOrderIterator*) iterator;

    // Ritorno true solo se non ho un corrente e lo stack è vuoto
    return !iter->current && stkEmpty(iter->stack);
}

void* itrElementPostOrd(void* iterator) {
    return ((BSTPostOrderIterator*) iterator)->current;
}

void itrSuccessorPostOrd(void* iterator) {
    BSTPostOrderIterator* iter = (BSTPostOrderIterator*) iterator;
    DataObject* dataptr;
    bool found = false;

    if(iter->current == iter->last) {
        iter->current = NULL;
    }

    while(((!stkEmpty(iter->stack)) || (iter->current)) && !found) {
        if (iter->current != NULL) {
            DataType* ptrType = ConstructPtrDataType();
            DataObject *ptr = adtConstruct(ptrType);
            adtSetValue(ptr, iter->current);
            stkPush(iter->stack, ptr);
            iter->current = iter->current->left;

            adtDestruct(ptr);
        }
        else {
            dataptr = stkTop(iter->stack);
            iter->current = (BSTNode*) adtGetValue(dataptr);
            adtDestruct(dataptr);

            if((iter->current->right) && (iter->last != iter->current->right)) {
                iter->current = iter->current->right;
            }
            else {
                iter->last = iter->current;
                dataptr = stkTopNPop(iter->stack);
                DataType* typeToDestroy = dataptr->type;
                adtDestruct(dataptr);
                DestructPtrDataType(typeToDestroy);

                found = true; //Esco dalla funzione per effettuare la visita
            }
        }
    }
}