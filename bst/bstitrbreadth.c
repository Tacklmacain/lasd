#include "bstitrbreadth.h"

/* ************************************************************************** */

ITRType* ConstructBSTBreadthIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &itrConstructBreadth;
    type->destruct = &itrDestructBreadth;
    type->element = &itrElementBreadth;
    type->successor = &itrSuccessorBreadth;
    type->isTerminated = &itrTerminatedBreadth;

    return type;
}

void DestructBSTBreadthIterator(ITRType* type) {
    free(type);
}

void* itrConstructBreadth(void* root) {
    BSTBreadthIterator* iter = (BSTBreadthIterator*) malloc(sizeof(BSTBreadthIterator));
    BSTNode* node = (BSTNode*) root;
    QueueType* type = ConstructQueueVecType();

    // L'elemento corrente è quello dato in ingresso
    iter->current = root;
    iter->queue = queConstruct(type);

    // Inizio ad incodare gli eventuali nodi successivi a root
    if(node->left) {
        DataType* ptrType = ConstructPtrDataType();
        DataObject* data = adtConstruct(ptrType);
        adtSetValue(data, node->left);
        queEnqueue(iter->queue, data);
        adtDestruct(data);
    }

    if(node->right) {
        DataType* ptrType = ConstructPtrDataType();
        DataObject* data = adtConstruct(ptrType);
        adtSetValue(data, node->right);
        queEnqueue(iter->queue, data);
        adtDestruct(data);
    }

    return iter;
}

void itrDestructBreadth(void* iterator) {
    BSTBreadthIterator* iter = (BSTBreadthIterator*) iterator;
    QueueType* type = iter->queue->type;

    queClear(iter->queue);
    queDestruct(iter->queue);
    DestructQueueVecType(type);
    free(iter);
}

bool itrTerminatedBreadth(void* iterator) {
    BSTBreadthIterator* iter = (BSTBreadthIterator*) iterator;

    // Ritorno true solo se non ho un corrente e lo stack è vuoto
    return !iter->current && queEmpty(iter->queue);
}

void* itrElementBreadth(void* iterator) {
    return ((BSTBreadthIterator*) iterator)->current;
}

void itrSuccessorBreadth(void* iterator) {
    BSTBreadthIterator* iter = (BSTBreadthIterator*) iterator;

    if(!queEmpty(iter->queue)) {
        DataObject* tmp = queHeadNDequeue(iter->queue);

        iter->current = adtGetValue(tmp);
        DataType* typeToDestroy = tmp->type;
        adtDestruct(tmp);
        DestructPtrDataType(typeToDestroy);

        if(iter->current->left) {
            DataType* ptrType = ConstructPtrDataType();
            DataObject* ptr = adtConstruct(ptrType);
            adtSetValue(ptr, iter->current->left);
            queEnqueue(iter->queue, ptr);
            adtDestruct(ptr);
        }

        if(iter->current->right) {
            DataType* ptrType = ConstructPtrDataType();
            DataObject* ptr = adtConstruct(ptrType);
            adtSetValue(ptr, iter->current->right);
            queEnqueue(iter->queue, ptr);
            adtDestruct(ptr);
        }

    }
    else {
        iter->current = NULL;
    }
}
