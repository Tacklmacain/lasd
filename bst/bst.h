#ifndef BST_H
#define BST_H

/* ************************************************************************** */

#include "../utility.h"
#include "../adt/adt.h"
#include "../queue/queue.h"
#include "../queue/vec/queuevec.h"
#include "../adt/ptr/adtptr.h"
#include "../adt/int/adtint.h"

/* ************************************************************************** */

// Nodo singolo che ha un adt e 2 figli
typedef struct BSTNode BSTNode;
struct BSTNode {
    DataObject* value;
    BSTNode* left;
    BSTNode* right;
};

/* ************************************************************************** */

typedef void (*BSTDestruct)(void*);

// Va fatta in post-order, servono 2 stack
typedef void* (*BSTClone)(void*);

// Va implementata con due iteratori
typedef bool (*BSTEqual)(void*, void*);

// Va implementata senza utilizzare iteratori
typedef bool (*BSTExists)(void*, DataObject*);

// L'inserimento in un BST può fallire se l'elemento già esiste
typedef void* (*BSTInsert)(void*, DataObject*, bool*);

// L'eliminazione in un BST può fallire se l'elemento non esiste
typedef void* (*BSTRemove)(void*, DataObject*, bool*);

/* Ritorno il DataObject minimo o massimo, opzionalmente lo
 * rimuovo o lo tolgo e basta, non servono iteratori */
typedef DataObject* (*BSTGetMin)(void*);
typedef void* (*BSTGetNRemoveMin)(void*, DataObject**);
// Devo controllare che abbia almeno 1 elemento
typedef void* (*BSTRemoveMin)(void*);
typedef DataObject* (*BSTGetMax)(void*);
typedef void* (*BSTGetNRemoveMax)(void*, DataObject**);
// Devo controllare che abbia almeno 1 elemento
typedef void* (*BSTRemoveMax)(void*);

/* Il predecessore è l'elemento più grande del sottoalbero sinistro,
 * se il sottoalbero non esiste ritorna null e non decrementa size*/
typedef DataObject* (*BSTGetPredecessor)(void*, DataObject*);
typedef void* (*BSTGetNRemovePredecessor)(void*, DataObject*, DataObject**, bool*);
typedef void* (*BSTRemovePredecessor)(void*, DataObject*, bool*);

/* Il successore è l'elemento più piccolo del sottoalbero destro,
 * se il sottoalbero non esiste ritorna null e non decrementa size*/
typedef DataObject* (*BSTGetSuccessor)(void*, DataObject*);
typedef void* (*BSTGetNRemoveSuccessor)(void*, DataObject*, DataObject**, bool*);
typedef void* (*BSTRemoveSuccessor)(void*, DataObject*, bool*);

typedef void (*BSTPreOrderMap)(void*, MapFun, void*);
typedef void (*BSTInOrderMap)(void*, MapFun, void*);
typedef void (*BSTPostOrderMap)(void*, MapFun, void*);
typedef void (*BSTBreadthMap)(void*, MapFun, void*);
typedef void (*BSTPreOrderFold)(void*, FoldFun , void*, void*);
typedef void (*BSTInOrderFold)(void*, FoldFun , void*, void*);
typedef void (*BSTPostOrderFold)(void*, FoldFun , void*, void*);
typedef void (*BSTBreadthFold)(void*, FoldFun , void*, void*);

// Funzioni custom
typedef DataObject* (*BSTGetValue)(void*, DataObject*);

/* ************************************************************************** */

typedef struct BSTType {
  BSTDestruct destruct;
  BSTClone clone;
  BSTEqual equal;
  BSTExists exists;
  BSTInsert insert;
  BSTRemove remove;
  BSTGetMin getMin;
  BSTGetNRemoveMin getNRemoveMin;
  BSTRemoveMin removeMin;
  BSTGetMax getMax;
  BSTGetNRemoveMax getNRemoveMax;
  BSTRemoveMax removeMax;
  BSTGetPredecessor getPredecessor;
  BSTGetNRemovePredecessor getNRemovePredecessor;
  BSTRemovePredecessor removePredecessor;
  BSTGetSuccessor getSuccessor;
  BSTGetNRemoveSuccessor getNRemoveSuccessor;
  BSTRemoveSuccessor removeSuccessor;
  BSTPreOrderMap preOrderMap;
  BSTInOrderMap inOrderMap;
  BSTPostOrderMap postOrderMap;
  BSTBreadthMap breadthMap;
  BSTPreOrderFold preOrderFold;
  BSTInOrderFold inOrderFold;
  BSTPostOrderFold postOrderFold;
  BSTBreadthFold breadthFold;
  BSTGetValue getValue;
} BSTType;

// Ha un BSTType, una radice BSTNode ed una size
typedef struct BSTObject {
    BSTType* type;
    BSTNode* root;
    uint size;
} BSTObject;

/* ************************************************************************** */

// La costruct si fa solo astratta in quanto sia ricorsivo che iterativo è uguale
BSTObject* bstConstruct(BSTType*);
void bstDestruct(BSTObject*);

bool bstEmpty(BSTObject*);

uint bstSize(BSTObject*);

// Vanno fatte in post-order
void bstClear(BSTObject*);
BSTObject* bstClone(BSTObject*);

bool bstEqual(BSTObject*, BSTObject*);

bool bstExists(BSTObject*, DataObject*);

bool bstInsert(BSTObject*, DataObject*);
bool bstRemove(BSTObject*, DataObject*);

DataObject* bstGetMin(BSTObject*);
DataObject* bstGetNRemoveMin(BSTObject*);
void bstRemoveMin(BSTObject*);
DataObject* bstGetMax(BSTObject*);
DataObject* bstGetNRemoveMax(BSTObject*);
void bstRemoveMax(BSTObject*);

DataObject* bstGetPredecessor(BSTObject*, DataObject*);
DataObject* bstGetNRemovePredecessor(BSTObject*, DataObject*);
bool bstRemovePredecessor(BSTObject*, DataObject*);

DataObject* bstGetSuccessor(BSTObject*, DataObject*);
DataObject* bstGetNRemoveSuccessor(BSTObject*, DataObject*);
bool bstRemoveSuccessor(BSTObject*, DataObject*);

void bstPreOrderMap(BSTObject*, MapFun, void*);
void bstInOrderMap(BSTObject*, MapFun, void*);
void bstPostOrderMap(BSTObject*, MapFun, void*);
void bstBreadthMap(BSTObject*, MapFun, void*);
void bstPreOrderFold(BSTObject*, FoldFun, void*, void*);
void bstInOrderFold(BSTObject*, FoldFun, void*, void*);
void bstPostOrderFold(BSTObject*, FoldFun, void*, void*);
void bstBreadthFold(BSTObject*, FoldFun, void*, void*);

// Funzioni custom
DataObject* bstGetValue(BSTObject*, DataObject*);

/* ************************************************************************** */

#endif