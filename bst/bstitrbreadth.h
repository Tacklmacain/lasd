
#ifndef BSTITRBREADTH_H
#define BSTITRBREADTH_H

/* ************************************************************************** */

#include "../itr/itr.h"
#include "../adt/ptr/adtptr.h"
#include "../queue/queue.h"
#include "bst.h"

/* ************************************************************************** */

typedef struct BSTBreadthIterator {
    BSTNode* current;
    QueueObject* queue;
} BSTBreadthIterator;

/* ************************************************************************** */

ITRType* ConstructBSTBreadthIterator();
void DestructBSTBreadthIterator(ITRType*);

void* itrConstructBreadth(void*);
void itrDestructBreadth(void*);
bool itrTerminatedBreadth(void*);
void* itrElementBreadth(void*);
void itrSuccessorBreadth(void*);

/* ************************************************************************** */

#endif
