#include "bstitrpreord.h"
#include "../stack/vec/stackvec.h"
#include "bstitrpreord.h"
#include "../adt/ptr/adtptr.h"

/* ************************************************************************** */

ITRType* ConstructBSTPreOrderIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &itrConstructPreOrd;
    type->destruct = &itrDestructPreOrd;
    type->element = &itrElementPreOrd;
    type->successor = &itrSuccessorPreOrd;
    type->isTerminated = &itrTerminatedPreOrd;

    return type;
}

void DestructBSTPreOrderIterator(ITRType* type) {
    free(type);
}

void* itrConstructPreOrd(void* root) {
    BSTPreOrderIterator* iter = (BSTPreOrderIterator*) malloc(sizeof(BSTPreOrderIterator));
    StackType* type = ConstructStackVecType();

    // L'elemento corrente è quello dato in ingresso
    iter->current = root;
    iter->stack = stkConstruct(type);

    return iter;
}

void itrDestructPreOrd(void* iterator) {
    BSTPreOrderIterator* iter = (BSTPreOrderIterator*) iterator;
    StackType* type = iter->stack->type;

    stkClear(iter->stack);
    stkDestruct(iter->stack);
    DestructStackVecType(type);
    free(iter);
}

bool itrTerminatedPreOrd(void* iterator) {
    BSTPreOrderIterator* iter = (BSTPreOrderIterator*) iterator;

    // Ritorno true solo se non ho un corrente e lo stack è vuoto
    return !iter->current && stkEmpty(iter->stack);
}

void* itrElementPreOrd(void* iterator) {
    return ((BSTPreOrderIterator*) iterator)->current;
}

void itrSuccessorPreOrd(void* iterator) {
    BSTPreOrderIterator* iter = (BSTPreOrderIterator*) iterator;

    DataType* ptrType = ConstructPtrDataType();
    DataObject* ptr = adtConstruct(ptrType);
    adtSetValue(ptr, iter->current);

    // Salvo nello stack il nodo attuale e mi sposto a sinistra
    stkPush(iter->stack, ptr);
    iter->current = iter->current->left;
    adtDestruct(ptr);

    // Se l'elemento corrente è NULL e lo stack non è vuoto, devo risalire
    while(!stkEmpty(iter->stack) && !iter->current) {
        DataObject* tmp = stkTopNPop(iter->stack);
        iter->current = adtGetValue(tmp);

        DataType* typeToDestroy = tmp->type;
        adtDestruct(tmp);
        DestructPtrDataType(typeToDestroy);

        iter->current = iter->current->right;
    }
}
