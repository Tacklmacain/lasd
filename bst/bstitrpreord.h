
#ifndef BSTITRPREORD_H
#define BSTITRPREORD_H

/* ************************************************************************** */

#include "../itr/itr.h"
#include "../stack/stack.h"
#include "bst.h"

/* ************************************************************************** */

typedef struct BSTPreOrderIterator {
    BSTNode* current;
    StackObject* stack;
} BSTPreOrderIterator;

/* ************************************************************************** */

ITRType* ConstructBSTPreOrderIterator();
void DestructBSTPreOrderIterator(ITRType*);

void* itrConstructPreOrd(void*);
void itrDestructPreOrd(void*);
bool itrTerminatedPreOrd(void*);
void* itrElementPreOrd(void*);
void itrSuccessorPreOrd(void*);

/* ************************************************************************** */

#endif
