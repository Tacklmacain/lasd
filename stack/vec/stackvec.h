#ifndef STACKVEC_H
#define STACKVEC_H

/* ************************************************************************** */

#include "../stack.h"

/* ************************************************************************** */

typedef struct StackVec
{
    uint headIndex;
    uint dimension;
    DataObject** elements;
} StackVec;

/* ************************************************************************** */

// Costruzione del tipo di implementazione con array
StackType* ConstructStackVecType();
void DestructStackVecType(StackType*);

// Operazioni di costruzione e distruzione della struttura
void* stkVecConstruct();
void stkVecDestruct(void*);

// Operazioni elementari
bool stkVecEmpty(void*);
DataObject* stkVecTop(void*);
void stkVecPop(void*);
DataObject* stkVecTopNPop(void*);
void stkVecPush(void*, DataObject*);
void stkVecClear(void*);

// Operazioni non-elementari
void* stkVecClone(void*);
bool stkVecEqual(void*, void*);
void stkVecMap(void*, MapFun, void*);
void stkVecFold(void*, FoldFun, void*, void*);

/* ************************************************************************** */

void reduceStackVecDimension(StackVec* stack);

#endif
