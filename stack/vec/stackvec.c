#include "stackvec.h"

/* ************************************************************************** */

// Costruisco lo StackType ed assegno tutti i puntatori a funzioni
StackType* ConstructStackVecType() {
    StackType* type = (StackType*) malloc(sizeof(StackType));

    type->construct = &stkVecConstruct;
    type->destruct = &stkVecDestruct;
    type->isEmpty = &stkVecEmpty;
    type->top = &stkVecTop;
    type->pop = &stkVecPop;
    type->topAndPop = &stkVecTopNPop;
    type->push = &stkVecPush;
    type->clear = &stkVecClear;
    type->clone = &stkVecClone;
    type->isEqual = &stkVecEqual;
    type->map = &stkVecMap;
    type->fold = &stkVecFold;
    return type;
}

void DestructStackVecType(StackType* type) {
    free(type);
}

// Operazioni di costruzione ed eliminazione
void* stkVecConstruct() {
    StackVec* stack = (StackVec*) malloc(sizeof(StackVec));

    assert(stack);

    stack->headIndex = 0;
    stack->dimension = 4;
    stack->elements = (DataObject**) malloc(sizeof(DataObject*) * stack->dimension);

    return (void*) stack;
}

void stkVecDestruct(void* stack) {
    StackVec* stk = (StackVec*) stack;
    free(stk->elements);
    free(stk);
}

// Operazioni elementari
bool stkVecEmpty(void* stack) {
    StackVec* stk = (StackVec*) stack;
    // Se l'indice è diverso da 0 significa che ho un elemento e ritorno che non è vuoto
    return stk->headIndex == 0;
}

DataObject* stkVecTop(void* stack) {
    // Se lo stack non è vuoto ritorno una copia del primo elemento, altrimenti NULL
    if(!(stkVecEmpty(stack))) {
        //Utilizzo la funzione di clonazione del tipo di dato per ritornarmi una copia
        StackVec* stk = (StackVec*) stack;
        DataObject* copyObject = adtClone(stk->elements[stk->headIndex-1]);
        return copyObject;
    }
    else {
        return NULL;
    }
}

void stkVecPop(void* stack) {
    StackVec* stk = (StackVec*) stack;

    // Decremento il numero di oggetti e libero in quella posizione con la funzione di tipo
    adtDestruct(stk->elements[--stk->headIndex]);

    // Verifico se devo dimezzare lo stack
    reduceStackVecDimension(stk);
}

DataObject* stkVecTopNPop(void* stack) {
    StackVec* stk = (StackVec*) stack;
    DataObject* value = stk->elements[--stk->headIndex];

    // Verifico se devo dimezzare lo stack
    reduceStackVecDimension(stk);

    return value;
}

void stkVecPush(void* stack, DataObject* obj) {
    // Utilizzo la funzione di clonazione per duplicare il DataObject in ingresso
    DataObject* newObj = adtClone(obj);

    StackVec* stk = (StackVec*) stack;

    // Se ho finito lo spazio, ne realloco il doppio
    if(stk->headIndex == stk->dimension) {
        stk->dimension *= 2;
        stk->elements = (DataObject**) realloc(stk->elements, stk->dimension * sizeof(DataObject*));
    }

    // Inserisco il nuovo elemento nello stack
    stk->elements[stk->headIndex++] = newObj;
}

void stkVecClear(void* stack) {
    StackVec* stk = (StackVec*) stack;

    while(!stkVecEmpty(stack)) {
        adtDestruct(stk->elements[--stk->headIndex]);
    }

    // Imposto la grandezza di default 4 e realloco
    stk->dimension = 4;
    stk->elements = (DataObject**) realloc(stk->elements, stk->dimension * sizeof(DataObject*));
}

// Operazioni non elementari
void* stkVecClone(void* stack) {
    StackVec* newStk = (StackVec*) malloc(sizeof(StackVec));
    StackVec* oldStk = (StackVec*) stack;

    newStk->dimension = oldStk->dimension;
    newStk->headIndex = 0;
    newStk->elements = (DataObject**) malloc(sizeof(DataObject*) * oldStk->dimension);

    // Faccio la push degli elementi uno ad uno in newStack
    for(uint i = 0; i < oldStk->headIndex; i++) {
        stkVecPush(newStk, oldStk->elements[i]);
    }

    return (void*) newStk;
}

bool stkVecEqual(void* stack1, void* stack2) {
    StackVec* firstStk = (StackVec*) stack1;
    StackVec* secondStk = (StackVec*) stack2;

    // Se il numero di elementi o lo spazio allocato non coincide, sono diversi
    if((firstStk->headIndex != secondStk->headIndex) ||
       (firstStk->dimension != secondStk->dimension)) {
        return false;
    }

    // Se non sono vuoti verifico elemento per elemento
    if(!stkVecEmpty(firstStk) && !stkVecEmpty(secondStk)) {

        /* Siccome hanno lo stesso numero di elementi per il controllo precedente
         * posso usare firstStk per ciclare senza conseguenze */
        for(uint i = 0; i < firstStk->headIndex; i++) {
            if(adtCompare(firstStk->elements[i], secondStk->elements[i]) != 0) {
                return false;
            }
        }
    }
    return true;
}

void stkVecMap(void* stack, MapFun function, void* parameter) {
    StackVec* stk = (StackVec*) stack;

    for(uint i = stk->headIndex; i > 0; i--) {
        function(stk->elements[i - 1], parameter);
    }
}

void stkVecFold(void* stack, FoldFun function, void* value, void* parameter) {
    StackVec* stk = (StackVec*) stack;

    for(uint i = stk->headIndex; i > 0; i--) {
        function(stk->elements[i - 1], value, parameter);
    }
}

// Utility per verificare se è necessario dimezzare lo stack
void reduceStackVecDimension(StackVec* stack) {
    if((stack->headIndex * 4) == stack->dimension) {
        stack->elements = (DataObject**) realloc(stack->elements, (stack->dimension / 2) * sizeof(DataObject*));
        stack->dimension /= 2;
    }
}
