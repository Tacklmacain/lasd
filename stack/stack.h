#ifndef STACK_H
#define STACK_H

/* ************************************************************************** */

#include "../utility.h"
#include "../adt/adt.h"

/* ************************************************************************** */

// Sono quelle utilizzate dal type
typedef void* (*StkConstruct)();
typedef void (*StkDestruct)(void*);

typedef bool (*StkEmpty)(void*);
typedef DataObject* (*StkTop)(void*);
typedef void (*StkPop)(void*);
typedef DataObject* (*StkTopNPop)(void*);
typedef void (*StkPush)(void*, DataObject*);
typedef void (*StkClear)(void*);

typedef void* (*StkClone)(void*);
typedef bool (*StkEqual)(void*, void*);
typedef void (*StkMap)(void*, MapFun, void*);
typedef void (*StkFold)(void*, FoldFun, void*, void*);

/* ************************************************************************** */

typedef struct StackType {
    StkConstruct construct;
    StkDestruct destruct;
    StkEmpty isEmpty;
    StkTop top;
    StkPop pop;
    StkTopNPop topAndPop;
    StkPush push;
    StkClear clear;
    StkClone clone;
    StkEqual isEqual;
    StkMap map;
    StkFold fold;
} StackType;

/* Utilizzo una variabile size nel modello astratto per poter
 * avere una grandezza costante con le liste senza calcolarla linearmente */
typedef struct StackObject {
    StackType* type;
    uint size;
    void* stk;
} StackObject;

/* ************************************************************************** */


// Sono quelle richiamate nel main
StackObject* stkConstruct(StackType*);
void stkDestruct(StackObject*);

bool stkEmpty(StackObject*);
DataObject* stkTop(StackObject*);
void stkPop(StackObject*);
DataObject* stkTopNPop(StackObject*);
void stkPush(StackObject*, DataObject*);
void stkClear(StackObject*);

StackObject* stkClone(StackObject*);
bool stkEqual(StackObject*, StackObject*);
void stkMap(StackObject*, MapFun, void*);
void stkFold(StackObject*, FoldFun, void*, void*);

int stkSize(StackObject*);

// Dovrò fare la fold e passare l'utility di exits
bool stkExists(StackObject*, DataObject*);

/* ************************************************************************** */

#endif
