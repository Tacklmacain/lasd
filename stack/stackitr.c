#include "stackitr.h"
#include "../stack/vec/stackvec.h"

/* ************************************************************************** */

ITRType* ConstructStackIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &itrConstructStackItr;
    type->destruct = &itrDestructStackItr;
    type->element = &itrElementStackItr;
    type->successor = &itrSuccessorStackItr;
    type->isTerminated = &itrTerminatedStackItr;

    return type;
}

void DestructStackIterator(ITRType* type) {
    free(type);
}

void* itrConstructStackItr(void* stack) {
    StackIterator* iter = (StackIterator*) malloc(sizeof(StackIterator));
    StackObject* stackObject = (StackObject*) stack;

    iter->stack = stackObject;
    if(!stkEmpty(iter->stack)) {
        iter->current = stkTop(stackObject);
        iter->dataType = iter->current->type;
    }
    else {
        iter->current = NULL;
        iter->dataType = NULL;
    }

    return iter;
}

void itrDestructStackItr(void* iterator) {
    StackIterator* iter = (StackIterator*) iterator;
    StackType* type = iter->stack->type;

    if(iter->dataType) {
        free(iter->dataType);
    }

    stkClear(iter->stack);
    stkDestruct(iter->stack);
    DestructStackVecType(type);
    free(iter);
}

bool itrTerminatedStackItr(void* iterator) {
    StackIterator* iter = (StackIterator*) iterator;
    return stkEmpty(iter->stack);
}

void* itrElementStackItr(void* iterator) {
    StackIterator* iter = (StackIterator*) iterator;

    return iter->current->value;
}

void itrSuccessorStackItr(void* iterator) {
    StackIterator* iter = (StackIterator*) iterator;

    adtDestruct(iter->current);

    stkPop(iter->stack);
    iter->current = stkTop(iter->stack);
}

/* ************************************************************************** */