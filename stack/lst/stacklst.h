#ifndef STACKLST_H
#define STACKLST_H

/* ************************************************************************** */

#include "../stack.h"

/* ************************************************************************** */

typedef struct StackLst StackLst;

struct StackLst {
    DataObject* value;
    StackLst* next;
};

/* ************************************************************************** */

// Costruzione del tipo di implementazione con array
StackType* ConstructStackLstType();
void DestructStackLstType(StackType*);

// Operazioni di costruzione e distruzione della struttura
void* stkLstConstruct();
void stkLstDestruct(void*);

// Operazioni elementari
bool stkLstEmpty(void*);
DataObject* stkLstTop(void*);
void stkLstPop(void*);
DataObject* stkLstTopNPop(void*);
void stkLstPush(void*, DataObject*);
void stkLstClear(void*);

// Operazioni non-elementari
void* stkLstClone(void*);
bool stkLstEqual(void*, void*);
void stkLstMap(void*, MapFun, void*);
void stkLstFold(void*, FoldFun, void*, void*);

/* ************************************************************************** */

#endif
