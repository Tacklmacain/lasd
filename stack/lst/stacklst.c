#include "stacklst.h"

/* ************************************************************************** */

// Costruisco lo StackType ed assegno tutti i puntatori a funzioni
StackType* ConstructStackLstType() {
    StackType* type = (StackType*) malloc(sizeof(StackType));

    type->construct = &stkLstConstruct;
    type->destruct = &stkLstDestruct;
    type->isEmpty = &stkLstEmpty;
    type->top = &stkLstTop;
    type->pop = &stkLstPop;
    type->topAndPop = &stkLstTopNPop;
    type->push = &stkLstPush;
    type->clear = &stkLstClear;
    type->clone = &stkLstClone;
    type->isEqual = &stkLstEqual;
    type->map = &stkLstMap;
    type->fold = &stkLstFold;
    return type;
}

void DestructStackLstType(StackType* type) {
    free(type);
}

// Operazioni di costruzione e distruzione della struttura
void* stkLstConstruct() {
    StackLst* stack = (StackLst*) malloc(sizeof(StackLst));

    assert(stack);

    // Creo il primo nodo sentinella, non è allocato il suo value
    stack->value = NULL;
    stack->next = NULL;

    return stack;
}

void stkLstDestruct(void* stack) {
    free(stack);
}

bool stkLstEmpty(void* stack) {
    StackLst* sentinel = (StackLst*) stack;
    // Se next è diverso da null significa che ho un elemento e ritorno che non è vuoto
    return !sentinel->next;
}

DataObject* stkLstTop(void* stack) {
    StackLst* stk = ((StackLst*) stack)->next;

    // Se lo stack non è vuoto ritorno una copia del primo elemento, altrimenti NULL
    return (!stkLstEmpty(stack)) ? adtClone(stk->value) : NULL;
}

void stkLstPop(void* stack) {
    StackLst* sentinel = (StackLst*) stack;
    StackLst* stk = sentinel->next;

    // Indico che ora la sentinella deve puntare all'elemento successivo
    sentinel->next = stk->next;

    // Libero in quella posizione con la funzione di tipo e libero il nodo
    adtDestruct(stk->value);
    free(stk);
}

DataObject* stkLstTopNPop(void* stack) {
    StackLst* sentinel = (StackLst*) stack;
    StackLst* stk = sentinel->next;

    // Indico che ora la sentinella deve puntare all'elemento successivo
    sentinel->next = stk->next;

    // Prendo il valore da tornare e libero il nodo
    DataObject* returnObj = stk->value;
    free(stk);

    return returnObj;
}

void stkLstPush(void* stack, DataObject* data) {
    StackLst* newNode = (StackLst*) malloc(sizeof(StackLst));

    // Utilizzo la funzione di clonazione per duplicare il DataObject in ingresso
    newNode->value = adtClone(data);

    // Casto la sentinella e assegno il suo next al nuovo nodo
    StackLst* sentinel = (StackLst*) stack;
    newNode->next = sentinel->next;
    sentinel->next = newNode;
}

void stkLstClear(void* stack) {
    while(!stkLstEmpty(stack)) {
        stkLstPop(stack);
    }
}

// Operazioni non-elementari
void* stkLstClone(void* stack) {
    // Creo uno stack di appoggio
    StackLst* tmp = stkLstConstruct();

    // Creo lo stack clone effettivo
    StackLst* clone = stkLstConstruct();

    // Variabili di appoggio
    StackLst* travel = ((StackLst*) stack)->next;
    DataObject* value;

    /* Inizio a riempire lo stack temporaneo con le push per ottenere uno stack capovolto
     * utilizzo la variabile travel per viaggiare nello stack originale */
    while(travel) {
        stkLstPush(tmp, travel->value);
        travel = travel->next;
    }

    // Capovolgo lo stack di supporto per ottenere lo stack originale
    while(!stkLstEmpty(tmp)) {
        value = stkLstTopNPop(tmp);
        stkLstPush(clone, value);
        adtDestruct(value);
    }

    stkLstDestruct(tmp);
    return clone;
}

bool stkLstEqual(void* stk1, void* stk2) {
    StackLst* travel1 = ((StackLst*) stk1)->next;
    StackLst* travel2 = ((StackLst*) stk2)->next;

    /* Utilizzo lo XOR per assicurarmi che entrambi puntino a qualcosa
     * oppure entrambi non puntino a nulla, se uno punta a qualcosa
     * e l'altro no ritorno false */
    if(!(travel1) != !(travel2)) {
        return false;
    }

    /* Viaggio tra i due finché entrambi hanno elementi e
     * verifico elemento per elemento */
    while(travel1 && travel2) {
        if(adtCompare(travel1->value, travel2->value) != 0) {
            return false;
        }
        travel1 = travel1->next;
        travel2 = travel2->next;
    }

    /* Ritorno il confronto che entrambi puntino a NULL
     * in quanto è l'unico caso in cui sono uguali */
    return (!travel1 && !travel2);
}

void stkLstMap(void* stack, MapFun mapFunction, void* parameter) {
    StackLst* travel = ((StackLst*) stack)->next;

    while(travel) {
        mapFunction(travel->value, parameter);
        travel = travel->next;
    }
}

void stkLstFold(void* stack, FoldFun foldFunction, void* value, void* parameter) {
    StackLst* travel = ((StackLst*) stack)->next;

    while(travel) {
        foldFunction(travel->value, value, parameter);
        travel = travel->next;
    }
}
