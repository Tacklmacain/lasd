#ifndef EXERCISE3_STACKITR_H
#define EXERCISE3_STACKITR_H

/* ************************************************************************** */

#include "../itr/itr.h"
#include "../stack/stack.h"

/* ************************************************************************** */

typedef struct StackIterator {
    DataObject* current;
    StackObject* stack;
    DataType* dataType;
} StackIterator;

/* ************************************************************************** */

ITRType* ConstructStackIterator();
void DestructStackIterator(ITRType*);

void* itrConstructStackItr(void*);
void itrDestructStackItr(void*);
bool itrTerminatedStackItr(void*);
void* itrElementStackItr(void*);
void itrSuccessorStackItr(void*);

/* ************************************************************************** */


#endif
