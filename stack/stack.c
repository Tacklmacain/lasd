#include "stack.h"

/* ************************************************************************** */

StackObject* stkConstruct(StackType* type) {
    assert(type);

    StackObject* stack = (StackObject*) malloc(sizeof(StackObject));
    stack->type = type;
    stack->size = 0;
    stack->stk = type->construct();

    return stack;
}

void stkDestruct(StackObject* stkObj) {
    assert(stkObj);

    if(stkObj->stk) {
        stkObj->type->destruct(stkObj->stk);
    }
    free(stkObj);
}

bool stkEmpty(StackObject* stkObj) {
    assert(stkObj && stkObj->stk);
    return stkObj->type->isEmpty(stkObj->stk);
}

DataObject* stkTop(StackObject* stkObj) {
    assert(stkObj && stkObj->stk);
    return stkObj->type->top(stkObj->stk);
}

void stkPop(StackObject* stkObj) {
    assert(stkObj && stkObj->stk);

    if(!stkEmpty(stkObj)) {
        stkObj->size--;
        stkObj->type->pop(stkObj->stk);
    }
}

DataObject* stkTopNPop(StackObject* stkObj) {
    assert(stkObj && stkObj->stk);

    // Se lo stack e' vuoto ritorno NULL
    if(stkEmpty(stkObj)) {
        return NULL;
    }

    stkObj->size--;
    return stkObj->type->topAndPop(stkObj->stk);
}

void stkPush(StackObject* stkObj, DataObject* data) {
    assert(stkObj && stkObj->stk);

    // Inserisco solo se l'elemento non è nullo
    if(data) {
        stkObj->size++;
        stkObj->type->push(stkObj->stk, data);
    }
}

void stkClear(StackObject* stkObj) {
    assert(stkObj && stkObj->stk);

    // Elimino gli elementi solo se lo stack non e' vuoto
    if(!stkEmpty(stkObj)) {
        stkObj->size = 0;
        stkObj->type->clear(stkObj->stk);
    }
}

int stkSize(StackObject* stkObj) {
    assert(stkObj);
    return stkObj->size;
}

StackObject* stkClone(StackObject* stkObj) {
    assert(stkObj && stkObj->stk);

    // Creo lo StackObject clone
    StackObject* clone = (StackObject*) malloc(sizeof(StackObject));
    clone->type = stkObj->type;
    clone->size = stkObj->size;
    clone->stk = stkObj->type->clone(stkObj->stk);

    return clone;
}

bool stkEqual(StackObject* stkObj1, StackObject* stkObj2) {
    assert(stkObj1 && stkObj1->stk && stkObj2 && stkObj2->stk);
    return stkObj1->type->isEqual(stkObj1->stk, stkObj2->stk);
}

void stkMap(StackObject* stkObj, MapFun mapFunction, void* parameter) {
    assert(stkObj && stkObj->stk);
    stkObj->type->map(stkObj->stk, mapFunction, parameter);
}

void stkFold(StackObject* stkObj, FoldFun foldFunction, void* value, void* parameter) {
    assert(stkObj && stkObj->stk);
    stkObj->type->fold(stkObj->stk, foldFunction, value, parameter);
}

bool stkExists(StackObject* stkObj, DataObject* data) {
    assert(stkObj && stkObj->stk);
    bool exists = false;
    // Passo lo StackObject in quanto richiamo il metodo astratto
    stkFold(stkObj, &foldValueExists, &exists, data);
    return exists;
}
