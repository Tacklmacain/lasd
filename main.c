#include "utility.h"

/* ************************************************************************** */

#include "adt/adt.h"
#include "adt/int/adtint.h"
#include "adt/flt/adtflt.h"
#include "adt/str/adtstr.h"
#include "adt/rec/adtrec.h"

#include "stack/stack.h"
#include "stack/vec/stackvec.h"
#include "stack/lst/stacklst.h"

#include "queue/queue.h"
#include "queue/vec/queuevec.h"
#include "queue/lst/queuelst.h"

#include "bst/bst.h"
#include "bst/rec/bstrec.h"
#include "bst/itr/bstitr.h"

#include "graph/graph.h"
#include "graph/rec/graphrec.h"
#include "graph/itr/graphitr.h"
#include "graph/rep/mat/graphmat.h"
#include "graph/rep/lst/graphlst.h"
#include "graph/rep/bst/graphbst.h"

/* ************************************************************************** */

/* Puntatori a funzione per permettere all'utente che tipo di Map  e fold
 * vuole tra quella in, post, pre e breadth order */
typedef void (*MapType)(GraphObject*, MapFun, void*);
typedef void (*FoldType)(GraphObject*, FoldFun, void*, void*);

// Prototipi delle funzioni
void getUserChoice(int*, int, int);
void printAllAdjacents(GraphObject*);
GraphObject* createRandomGraph(uint, DataType*, GraphType*);
void useGraph(GraphType*, DataType*, int*, FoldFun, MapFun, bool);

/* ************************************************************************** */

int main() {
    srand(time(NULL));

    // Disattiva il buffer dello scanf
    setbuf(stdout, NULL);

    /* ************************************************************************** */

    // Variabili relative alle scelte dell'utente
    int userRepresentation, userImplementation, userDataType, userMenuChoice = 0;

    // Creazione delle variabili relative alle scelte dell'utente
    DataType* dataType = NULL;
    GraphRepresentation* graphRepresentation = NULL;
    GraphType* graphType = NULL;
    FoldFun specialFold = NULL;
    MapFun specialMap = NULL;
    bool typeIsString = false;

    // Ciclo almeno una volta, potrebbe ciclare più volte se l'utente cambia rappresentazione
    do {
        // Prendo in input i valori dell'utente
        printf("*** Benvenuto nella libreria per i Grafi ***\n"
               "Selezionare il tipo di rappresentazione che si intende utilizzare, sara' possibile cambiarla successivamente:\n"
               "1. Matrici di adiacenza \n2. Liste di adiacenza \n3. Alberi di adiacenza \n");
        getUserChoice(&userRepresentation, 1, 3);

        printf("Selezionare il tipo di implementazione che si intende utilizzare, sara' possibile cambiarla successivamente:\n"
               "1. Grafi ricorsivi \n2. Grafi iterativi \n");
        getUserChoice(&userImplementation, 1, 2);

        printf("Selezionare ora il tipo di dato da utilizzare, sara' possibile cambiarlo successivamente:\n"
               "1. Interi \n2. Float \n3. Stringhe \n4. Record (Prodotto, ha un prezzo ed un peso) \n");
        getUserChoice(&userDataType, 1, 4);


        // Procedo ad assegnare tutto come richiesto
        printf("\nCreazione della rappresentazione, implementazione, tipo di dato e struttura in corso...\n\n");

        switch (userDataType) {
            case 1:
                dataType = ConstructIntDataType();
                specialFold = &foldProductInt;
                specialMap = &mapMultiplicateInt;
                break;
            case 2:
                dataType = ConstructFloatDataType();
                specialFold = &foldSumFloat;
                specialMap = &mapPowerFloat;
                break;
            case 3:
                typeIsString = true;
                dataType = ConstructStringDataType();
                specialMap = &mapHeadStr;
                specialFold = &foldConcatStr;
                break;
            case 4:
                dataType = ConstructRecordDataType();
                break;
            default:
                // Non dovrei mai arrivare qui, ma per motivi di sicurezza se ci arrivo esco con errore
                exit(EXIT_FAILURE);
        }

        // Creo la rappresentazione in base alla scelta dell'utente
        switch (userRepresentation) {
            case 1:
                graphRepresentation = ConstructGraphMat();
                break;
            case 2:
                graphRepresentation = ConstructGraphLst();
                break;
            case 3:
                graphRepresentation = ConstructGraphBst();
                break;
            default:
                // Non dovrei mai arrivare qui, ma per motivi di sicurezza se ci arrivo esco con errore
                exit(EXIT_FAILURE);
        }

        // Creo l'implementazione in base alla scelta dell'utente
        graphType = (userImplementation == 1) ? ConstructGraphRecursive(graphRepresentation) : ConstructGraphIterative(graphRepresentation);

        // Richiamo la funzione che cicla la libreria effettiva
        useGraph(graphType, dataType, &userMenuChoice, specialFold, specialMap, typeIsString);

        // Distruzione del tipo di dato
        switch (userDataType) {
            case 1:
                DestructIntDataType(dataType);
                break;
            case 2:
                DestructFloatDataType(dataType);
                break;
            case 3:
                DestructStringDataType(dataType);
                break;
            case 4:
                DestructRecordDataType(dataType);
                break;
            default:
                // Non dovrei mai arrivare qui, ma per motivi di sicurezza se ci arrivo esco con errore
                exit(EXIT_FAILURE);
        }

        // Distruzione del tipo di implementazione
        (userImplementation == 1) ? DestructGraphRecursive(graphType) : DestructGraphIterative(graphType);

        // Distruzione del tipo di rappresentazione
        switch (userRepresentation) {
            case 1:
                DestructGraphMat(graphRepresentation);
                break;
            case 2:
                DestructGraphLst(graphRepresentation);
                break;
            case 3:
                DestructGraphBst(graphRepresentation);
                break;
            default:
                // Non dovrei mai arrivare qui, ma per motivi di sicurezza se ci arrivo esco con errore
                exit(EXIT_FAILURE);
        }

        typeIsString = false;

    } while(userMenuChoice != -1);

    printf("That's all folks!\n\n");
    printf("*****************************************************************\n\n");

    return 0;

    /* ************************************************************************ */
}

void getUserChoice(int* value, int minRange, int maxRange) {
    do {
        scanf("%d", value);
        if(*value < minRange || *value > maxRange) {
            /* Stampo un messaggio di errore, questa e' anche una citazione al film del 1968
             * "2001: Odissea nello spazio" di Stanley Kubrick */
            printf("Mi dispiace, David, purtroppo non posso farlo, inserisci altro.\n");
        }
    } while(*value < minRange || *value > maxRange);
}

GraphObject* createRandomGraph(uint numItems, DataType* dataType, GraphType* graphType) {
    GraphObject* graphObject = graphConstruct(graphType);
    DataObject *dataObject = adtConstruct(dataType);

    // DataObject di supporto per dare nomi ai vertici casuali
    DataType* intType = ConstructIntDataType();
    DataObject* vertexName = adtConstruct(intType);
    int* tmp;
    bool repeat;

    // Creo un array di interi per contenere i vari nomi
    int* vertexNames = (int*) calloc(numItems, sizeof(int));

    // Ciclo finche' non creo esattamente numItems
    for(uint i = 0; i < numItems; i++) {
        do {
            // Genero un nome casuale per il vertice
            adtRandomValue(vertexName);
            tmp = adtGetValue(vertexName);

            // Se il valore generato e' 0 oppure gia' esiste lo libero e ciclo di nuovo
            if(*tmp == 0 || graphExistsVertex(graphObject, *tmp)) {
                repeat = true;
                free(tmp);
            }
            else {
                repeat = false;
            }
        } while(repeat);

        // Genero un dataObject casuale per il vertice
        adtRandomValue(dataObject);
        graphInsertVertex(graphObject, dataObject, *tmp);

        // Salvo questo nome nell'array temporaneo
        vertexNames[i] = *tmp;

        /* Questo vertice potra' avere un numero di archi pari al numero di vertici del grafo
         * quindi genero un valore casuale e poi inserisco gli archi */
        uint numArchi = rndNum(0, graphObject->numberOfVertex);
        for (int j = 0; j < numArchi; ++j) {

            // Prendo casualmente un vertice dall'array ed inserisco l'arco
            uint rndIndex = rndNum(0, graphObject->numberOfVertex - 1);
            int otherVertex = vertexNames[rndIndex];

            graphInsertEdge(graphObject, *tmp, otherVertex);
        }

        free(tmp);
    }

    adtDestruct(vertexName);
    DestructIntDataType(intType);
    adtDestruct(dataObject);
    free(vertexNames);

    return graphObject;
}

void printAllAdjacents(GraphObject *graph) {
    ITRObject* iterator = graphVertices(graph);
    printf("Ci sono %d vertici e %d archi contenuti nel grafo:\n", graph->numberOfVertex, graph->numberOfEdges);

    int i = 1;
    while(!itrTerminated(iterator)) {
        uint* index = itrElement(iterator);
        GraphVertex* vertex = graphVertexFromPointer(graph, index);
        printf("Vertice %d, nome %d, etichetta: ", i, vertex->name);
        adtWriteToMonitor(vertex->dataObject);
        printf("\n");

        // Stampo tutti gli adiacenti di quel vertice
        int j = 1;
        ITRObject* adjIterator = graphVertexEdges(graph, vertex->name);
        if(itrTerminated(adjIterator)) {
            printf("Il vertice non ha adiacenti\n");
        }
        while(!itrTerminated(adjIterator)) {
            uint* innerIndex = itrElement(adjIterator);
            GraphVertex* innerVertex = graphVertexFromPointer(graph, innerIndex);

            printf("\tAdiacente %d, nome %d, etichetta: ", j, innerVertex->name);
            adtWriteToMonitor(innerVertex->dataObject);
            printf("\n");
            itrSuccessor(adjIterator);
            j++;
        }
        destructIteratorAndType(adjIterator);
        printf("\n");
        itrSuccessor(iterator);

        i++;
    }
    destructIteratorAndType(iterator);
}

void useGraph(GraphType* graphType, DataType* dataType, int* userMenuChoice, FoldFun foldFunc, MapFun mapFunc, bool isString) {
    uint numItems = 1;
    int userConfirm = 0;

    // Avviso l'utente che se prova a mettere un valore negativo sara' trattato differentemente
    while(!userConfirm) {
        printf("La struttura sara' popolata casualmente, inserire quanti elementi dovra' contenere "
               "(Deve essere un numero positivo, si consiglia un numero compreso tra 5-10):\n");
        scanf("%u", &numItems);
        if(((int) numItems) < 0) {
            printf("Credo tu abbia inserito il valore negativo %d, tuttavia questo valore sara' trattato come %u, sei "
                   "davvero sicuro? \n1. Cambia valore \n2. Conferma (sconsigliato, ci mettera' un po')\n", (int) numItems, numItems);
            getUserChoice(&userConfirm, 1, 2);
            userConfirm--;
        }
        else {
            userConfirm = 1;
        }
    }

    // Procedo a creare il numero di elementi scelto dall'utente
    printf("\nInserimento di %u elementi nella struttura in corso...\n\n", numItems);

    // Costruisco il grafo, un eventuale grafi temporanei
    GraphObject* graphObject = createRandomGraph(numItems, dataType, graphType);
    GraphObject *transpose, *clone, *maximal, *sccGraph;

    printf("\nLa struttura con %d vertici e %d archi e' stata creata con successo, ora e' "
           "possibile scegliere che operazioni svolgere.\n", graphVertexNumber(graphObject), graphEdgeNumber(graphObject));


    // Dichiaro varie variabili di appoggio per il menù
    int printMenu = true, minChoice = -1, maxChoice = 26, subChoice, vertexName, edge1, edge2;
    DataObject *dataObject = adtConstruct(dataType), *printer;
    char *mapName = (char*) malloc(sizeof(char*) * 12), *foldName = (char*) malloc(sizeof(char*) * 12);
    strcpy(mapName, "Breadth\0");
    strcpy(foldName, "Breadth\0");
    ITRObject* topologicalOrder;

    MapType mapType = &graphBreadthMap;
    FoldType foldType = &graphBreadthFold;

    // Itero il menu' finche' l'utente non decide di uscire con -1 oppure 0
    do {
        // Stampo il menu' solo la prima volta e poi se richiesto esplicitamente in quanto contiene troppe opzioni
        if(printMenu) {
            printf("\n*****************************************************************\n");
            printf("Digita il numero vicino all'operazione per eseguirla:\n"
                   "-1. Esci dalla programma. \n"
                   "0. Cambia struttura, implementazione e tipo di dato (Nota: La struttura attuale sara' distrutta)\n"
                   "1. Cambia il tipo di fold e map (Di default sono entrambe Breadth) \n"
                   "2. Visualizza tutti gli elementi della struttura (Effettuata tramite funzione Map scelta nel punto 1)\n"
                   "3. Esegui la funzione fold speciale per il tipo di dato scelto \n"
                   "4. Esegui la funzione map speciale per il tipo di dato scelto \n"
                   "5. Verifica se il grafo e' vuoto\n"
                   "6. Stampa numero di archi e numero di vertici del grafo\n"
                   "7. Rimuovi tutti gli elementi dal BST\n"
                   "8. Crea il trasposto del grafo, stampa entrambi e fai il confronto di uguaglianza\n"
                   "9. Crea il clone del grafo, stampa entrambi e fai il confronto di uguaglianza\n"
                   "10. Crea il clone del grafo, togli un elemento e fai il confronto di uguaglianza\n"
                   "11. Inserisci un vertice nel grafo\n"
                   "12. Rimuovi un vertice dal grafo\n"
                   "13. Inserisci un arco nel grafo\n"
                   "14. Rimuovi un arco dal grafo\n"
                   "15. Verifica se esiste un vertice nel grafo\n"
                   "16. Verifica se esiste un arco nel grafo\n"
                   "17. Stampa il valore di un vertice partendo dal suo nome\n"
                   "18. Cambia il valore di un vertice\n"
                   "19. Verifica se esiste un determinato valore nel grafo (VertexWithData)\n"
                   "20. Verifica se il grafo e' aciclico\n"
                   "21. Effettua l'ordinamento topologico del grafo\n"
                   "22. Crea e stampa il massimo sottografo raggiungibile partendo da un vertice\n"
                   "23. Cerca e stampa il percorso minimo tra 2 vertici\n"
                   "24. Stampa il grafo delle componenti fortemente connesse\n"
                   "25. Elimina il 50%% dei vertici del grafo\n"
                   "26. Stampa nuovamente il menu'");
            printMenu = false;
        }
        printf("\n\nInserisci la prossima operazione da eseguire: ");
        getUserChoice(userMenuChoice, minChoice, maxChoice);


        switch (*userMenuChoice) {
            case 1:
                printf("\nInserire il tipo di MAP che si intende utilizzare:\n1. Pre-order \n2. Post-order \n3. Breadth\n");
                getUserChoice(&subChoice, 1, 3);

                switch (subChoice) {
                    case 1:
                        strcpy(mapName, "Pre-order\0");
                        mapType = &graphPreOrderMap;
                        break;
                    case 2:
                        strcpy(mapName, "Post-order\0");
                        mapType = &graphPostOrderMap;
                        break;
                    case 3:
                        strcpy(mapName, "Breadth\0");
                        mapType = &graphBreadthMap;
                        break;
                    default:
                        break;
                }

                printf("\nInserire il tipo di FOLD che si intende utilizzare:\n1. Pre-order \n2. Post-order \n3. Breadth\n");
                getUserChoice(&subChoice, 1, 3);

                switch (subChoice) {
                    case 1:
                        strcpy(foldName, "Pre-order\0");
                        foldType = &graphPreOrderFold;
                        break;
                    case 2:
                        strcpy(foldName, "Post-order\0");
                        foldType = &graphPostOrderFold;
                        break;
                    case 3:
                        strcpy(foldName, "Breadth\0");
                        foldType = &graphBreadthFold;
                        break;
                    default:
                        break;
                }
                printf("Preferenze modificate con successo, la funzione map e' %s mentre la fold e' %s!", mapName, foldName);
                break;

            case 2:
                printf("\nScegli il tipo di stampa:\n"
                       "1. Quella impostata (attualmente e' %s) \n2. Tutte\n", mapName);
                getUserChoice(&subChoice, 1, 2);

                if (subChoice == 1) {
                    mapType(graphObject, &mapPrint, NULL);
                }
                else {
                    printf("Stampa in Pre-order:\n");
                    graphPreOrderMap(graphObject, &mapPrint, NULL);

                    printf("\n\nStampa in Post-order:\n");
                    graphPostOrderMap(graphObject, &mapPrint, NULL);

                    printf("\n\nStampa in Breadth:\n");
                    graphBreadthMap(graphObject, &mapPrint, NULL);
                }
                break;


            case 3:
                // Verifico se per il tipo di dato e' supportata la funzione Fold (Non e' supportata per i record)
                if(foldFunc) {
                    int foldPar;
                    printf("Inserire il parametro massimo per la Fold (Deve essere compreso tra 0 e %d): \n", MaxIntAbsRndVal);
                    getUserChoice(&foldPar, 0, MaxIntAbsRndVal);

                    // Se il valore booleano isString e' true allora devo comportarmi diversamente
                    if(isString) {
                        int fullLength = 0;
                        foldType(graphObject, &foldSizeOfStr, &fullLength, &foldPar);
                        fullLength++;

                        char* fullStr = (char*) malloc(sizeof(char) * fullLength);
                        strcpy(fullStr, "");

                        foldType(graphObject, foldFunc, fullStr, &foldPar);

                        printf("La stringa concatenata: %s\n\n", fullStr);
                        free(fullStr);
                    }
                    else {
                        DataObject* foldResult = adtConstruct(dataType);
                        foldType(graphObject, foldFunc, foldResult->value, &foldPar);
                        printf("Il risultato: "); adtWriteToMonitor(foldResult); printf("\n\n");
                        adtDestruct(foldResult);
                    }
                }
                else {
                    printf("La funzione fold non e' supportata per il tipo di dato scelto.");
                }
                break;

            case 4:
                printf("Procedo ad effettuare la Map %s per il tipo di dato, il grafo attuale:\n", mapName);
                mapType(graphObject, &mapPrint, NULL);

                if(isString) {
                    DataObject* parameter = adtConstruct(dataType);
                    adtReadFromKeyboard(parameter);
                    mapType(graphObject, mapFunc, parameter->value);
                    adtDestruct(parameter);
                }
                else {
                    mapType(graphObject, mapFunc, NULL);
                }

                printf("\nIl grafo dopo aver effettuato la Map: \n");
                mapType(graphObject, &mapPrint, NULL);
                break;

            case 5:
                graphEmpty(graphObject) ? printf("Si', il grafo e' vuoto") : printf("No, il grafo non e' vuoto");
                break;

            case 6:
                printf("Il grafo contiene %d archi e %d vertici", graphEdgeNumber(graphObject), graphVertexNumber(graphObject));
                break;

            case 7:
                graphClear(graphObject);
                printf("Eliminazione di tutti gli elementi effettuata con successo, il grafo contiene %d archi e %d vertici", graphEdgeNumber(graphObject), graphVertexNumber(graphObject));
                break;

            case 8:
                printf("Il grafo originale ha la seguente forma:\n");
                printAllAdjacents(graphObject);

                transpose = graphTranspose(graphObject);

                printf("Ho creato il grafo trasposto, il suo contenuto:\n");
                printAllAdjacents(transpose);

                printf("\nIl grafo ed il suo trasposto sono identici: %d", graphEqual(graphObject, transpose));

                graphDestruct(transpose);
                break;

            case 9:
                printf("Il grafo originale ha la seguente forma:\n");
                printAllAdjacents(graphObject);

                clone = graphClone(graphObject);

                printf("Ho creato il clone, il suo contenuto:\n");
                printAllAdjacents(clone);

                printf("\nIl grafo ed il suo clone sono identici: %d", graphEqual(graphObject, clone));

                graphDestruct(clone);
                break;

            case 10:
                printf("Il grafo originale ha la seguente forma:\n");
                printAllAdjacents(graphObject);

                clone = graphClone(graphObject);

                printf("Ho creato il clone, il suo contenuto:\n");
                printAllAdjacents(clone);

                printf("Inserire il nome del vertice che si intende eliminare dal clone (se non esiste, non verra' rimosso nulla): ");
                scanf("%d", &vertexName);
                graphRemoveVertex(clone, vertexName);

                printf("\nIl grafo ed il suo clone sono identici: %d", graphEqual(graphObject, clone));

                graphDestruct(clone);
                break;

            case 11:
                do {
                    printf("Inserire il nome del vertice che si intende inserire nel grafo (puo' essere anche negativo ma deve essere diverso da 0): ");
                    scanf("%d", &vertexName);

                    if(vertexName == 0) {
                        printf("\nNon e' possibile assegnare 0 come nome ad un vertice\n");
                    }
                } while(vertexName == 0);

                adtReadFromKeyboard(dataObject);
                graphInsertVertex(graphObject, dataObject, vertexName);

                printf("\nIl grafo adesso ha la seguente forma:\n");
                printAllAdjacents(graphObject);
                break;

            case 12:
                printf("Inserire il nome del vertice che si intende rimuovere dal grafo (se non esiste, non verra' rimosso nulla): ");
                scanf("%d", &vertexName);

                graphRemoveVertex(graphObject, vertexName);

                printf("\nIl grafo adesso ha la seguente forma:\n");
                printAllAdjacents(graphObject);
                break;

            case 13:
                printf("Inserire il nome del vertice con arco uscente (se non esiste, non verra' inserito nulla): ");
                scanf("%d", &edge1);

                printf("Inserire il nome del vertice con arco entrante (se non esiste, non verra' inserito nulla): ");
                scanf("%d", &edge2);

                graphInsertEdge(graphObject, edge1, edge2);

                printf("\nIl grafo adesso ha la seguente forma:\n");
                printAllAdjacents(graphObject);
                break;

            case 14:
                printf("Inserire il nome del vertice con arco uscente (se non esiste, non verra' rimosso nulla): ");
                scanf("%d", &edge1);

                printf("Inserire il nome del vertice con arco entrante (se non esiste, non verra' rimosso nulla): ");
                scanf("%d", &edge2);

                graphRemoveEdge(graphObject, edge1, edge2);

                printf("\nIl grafo adesso ha la seguente forma:\n");
                printAllAdjacents(graphObject);
                break;

            case 15:
                printf("Inserire il nome del vertice che si intende sapere se esiste nel grafo: ");
                scanf("%d", &vertexName);

                printf("\nIl vertice con nome %d esiste nel grafo: %d", vertexName, graphExistsVertex(graphObject, vertexName));
                break;

            case 16:
                printf("Inserire il nome del vertice con arco uscente: ");
                scanf("%d", &edge1);

                printf("Inserire il nome del vertice con arco entrante: ");
                scanf("%d", &edge2);

                printf("\nL'arco che va da %d a %d esiste nel grafo: %d", edge1, edge2, graphExistsEdge(graphObject, edge1, edge2));
                break;

            case 17:
                printf("Inserire il nome del vertice che si intende sapere il suo valore: ");
                scanf("%d", &vertexName);

                if(graphExistsVertex(graphObject, vertexName)) {
                    printer = graphGetVertexData(graphObject, vertexName);
                    printf("Il vertice %d contiene il valore: ", vertexName);
                    adtWriteToMonitor(printer);
                    printf("\n");
                    adtDestruct(printer);
                }
                else {
                    printf("\nNon esiste nessun vertice con il nome scelto!\n");
                }
                break;

            case 18:
                printf("Inserire il nome del vertice che si intende modificare il suo valore: ");
                scanf("%d", &vertexName);

                if(graphExistsVertex(graphObject, vertexName)) {
                    printf("Inserire il nuovo valore che si intende inserire al vertice %d:\n", vertexName);
                    adtReadFromKeyboard(dataObject);
                    graphSetVertexData(graphObject, vertexName, dataObject);
                    printf("\nIl vertice %d ora contiene il valore: ", vertexName);
                    adtWriteToMonitor(dataObject);
                    printf("\n");
                }
                else {
                    printf("\nNon esiste nessun vertice con il nome scelto!\n");
                }
                break;

            case 19:
                printf("Inserire il valore che si intende cercare nel grafo:");
                adtReadFromKeyboard(dataObject);

                printf("\nIl vertice con il valore inserito esiste nel grafo: %d", graphExistsVertexWithData(graphObject, dataObject));
                break;

            case 20:
                printf("Il grafo e' aciclico: %d", graphIsAcyclic(graphObject));
                break;

            case 21:
                if(graphIsAcyclic(graphObject)) {
                    printf("Stampo i vertici ordinati dall'ordinamento topologico:\n");
                    topologicalOrder = graphTopologicalOrder(graphObject);

                    int i = 1;
                    while(!itrTerminated(topologicalOrder)) {
                        uint* vertexId = itrElement(topologicalOrder);
                        GraphVertex* vertex = graphVertexFromPointer(graphObject, vertexId);

                        printf("Vertice %d, nome %d, etichetta:\n", i, vertex->name);
                        adtWriteToMonitor(vertex->dataObject);
                        printf("\n");

                        itrSuccessor(topologicalOrder);
                    }
                    destructIteratorAndType(topologicalOrder);
                }
                else {
                    printf("Il grafo presenta dei cicli e di conseguenza non e' possibile farne l'ordinamento topologico.\n");
                }
                break;

            case 22:
                printf("Inserire il nome del vertice di cui si vuole sapere il sottografo massimale: ");
                scanf("%d", &vertexName);

                maximal = graphMaximalReachableSubgraph(graphObject, vertexName);
                printf("\nIl massimo sottografo raggiungibile da %d e' il seguente:\n", vertexName);
                printAllAdjacents(maximal);
                graphDestruct(maximal);
                break;

            case 23:
                printf("Inserire il nome del vertice con arco uscente: ");
                scanf("%d", &edge1);

                printf("Inserire il nome del vertice con arco entrante: ");
                scanf("%d", &edge2);

                printf("\nIl percorso minimo che va da %d a %d: %d", edge1, edge2, graphShortestPath(graphObject, edge1, edge2));
                break;

            case 24:
                printf("Il grafo ha la seguente forma:\n");
                printAllAdjacents(graphObject);

                printf("\nIl suo grafo delle CFC e' il seguente:\n");
                sccGraph = graphSCCGraph(graphObject);

                printAllAdjacents(sccGraph);
                graphDestruct(sccGraph);
                break;

            case 25:
                printf("Dopo questa azione il 50%% dei vertici del grafo verra' distrutto, ma l'altro 50%% continuera' ad esistere, sei sicuro?\n1. Si'\n2. No.\n");
                getUserChoice(&subChoice, 1, 2);

                if(subChoice == 1) {

                    // Salvo i vari vertici del grafo
                    int* vertexNames = (int*) calloc(graphObject->numberOfVertex, sizeof(int));
                    ITRObject* vertices = graphVertices(graphObject);
                    int i = 0;
                    while(!itrTerminated(vertices)) {
                        uint* index = itrElement(vertices);
                        GraphVertex* vertex = graphVertexFromPointer(graphObject, index);
                        vertexNames[i] = vertex->name;
                        itrSuccessor(vertices);
                        i++;
                    }
                    destructIteratorAndType(vertices);

                    int elementsToRemove = graphObject->numberOfVertex / 2;

                    int j = 0;
                    while(j < elementsToRemove) {
                        int index = rndNum(0, graphObject->numberOfVertex - 1);

                        if(vertexNames[index] != 0) {
                            graphRemoveVertex(graphObject, vertexNames[index]);
                            vertexNames[index] = 0;
                            j++;
                        }
                    }

                    free(vertexNames);
                }

                printf("\nIl grafo a seguito della scelta effettuata:\n");
                printAllAdjacents(graphObject);

                break;

            case 26:
                printMenu = true;
                break;

            default:
                break;
        }
    } while(*userMenuChoice != -1 && *userMenuChoice != 0);

    printf("\nDistruzione del grafo e dei suoi %d elementi in corso...\n\n", graphVertexNumber(graphObject));
    free(mapName);
    free(foldName);

    adtDestruct(dataObject);
    graphDestruct(graphObject);
    printf("*****************************************************************\n\n");
}
