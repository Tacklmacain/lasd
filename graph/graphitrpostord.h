#ifndef GRAPHITRPOSTORD_H
#define GRAPHITRPOSTORD_H

/* ************************************************************************** */

#include "../itr/itr.h"

#include "../stack/stack.h"

#include "graph.h"

/* ************************************************************************** */

/** Struttura per l'iteratore in PostOrder di un grafo
  * @attribute graph
  * Il grafo su cui si intende iterare
  *
  * @attribute currentVertex
  * Il vertice attuale dell'iterazione
  *
  * @attribute currentIndex
  * Indica la corrispondenza del vertice nell'array dei vertici
  *
  * @attribute color
  * Array di colori per i vari vertici, ha come possibili valori:
  * 'b' (vertice non scoperto), 'g' (vertice scoperto), 'n' (vertice visitato)
  *
  * @attribute StackVertices/StackAdjacent/StackIndexes
  * Stack per simulare la risalita
  *
  * @attribute verticesIterator/currentAdjIterator
  * Iteratori dei vertici ed adiacenti del currentVertex
  *
  * @attribute ptrType
  * Tipo per gli elementi dello stack, evita memory leak
  */
typedef struct GraphPostOrderIterator {
    GraphObject* graph;
    GraphVertex* currentVertex;
    uint* currentIndex;
    char* color;

    StackObject* stackVertices;
    StackObject* stackAdjacent;
    StackObject* stackIndexes;

    ITRObject* verticesIterator;
    ITRObject* currentAdjIterator;

    DataType *ptrType;
} GraphPostOrderIterator;

/* ************************************************************************** */

ITRType* ConstructGraphPostOrderIterator();
void DestructGraphPostOrderIterator(ITRType*);

void* GraphPostOrderIteratorConstruct(void*);
void GraphPostOrderIteratorDestruct(void*);
bool GraphPostOrderIteratorTerminated(void*);
void* GraphPostOrderIteratorElement(void*);
void GraphPostOrderIteratorSuccessor(void*);

/* ************************************************************************** */



#endif
