#include "graph.h"

/* ************************************************************************** */

GraphObject* graphConstruct(GraphType* type) {
    GraphObject* graph = (GraphObject*) malloc(sizeof(GraphObject));

    graph->type = type;
    graph->numberOfVertex = 0;
    graph->numberOfEdges = 0;
    graph->structure = type->representation->construct();

    return graph;
}

void graphDestruct(GraphObject* graph) {
    assert(graph && graph->structure);
    graph->type->representation->destruct(graph->structure);
    free(graph);
}

bool graphEmpty(GraphObject* graph) {
    assert(graph && graph->structure);
    return graph->type->representation->isEmpty(graph->structure);
}

uint graphVertexNumber(GraphObject* graph) {
    assert(graph);
    return graph->numberOfVertex;
}

uint graphEdgeNumber(GraphObject* graph) {
    assert(graph);
    return graph->numberOfEdges;
}

void graphClear(GraphObject* graph) {
    assert(graph && graph->structure);

    // Distruggo il grafo precedente e ne creo uno nuovo
    graph->type->representation->destruct(graph->structure);
    graph->structure = graph->type->representation->construct();

    graph->numberOfVertex = 0;
    graph->numberOfEdges = 0;
}

GraphObject* graphClone(GraphObject* graph) {
    assert(graph && graph->structure);
    GraphObject* clone = (GraphObject*) malloc(sizeof(GraphObject));

    clone->type = graph->type;
    clone->numberOfVertex = graph->numberOfVertex;
    clone->numberOfEdges = graph->numberOfEdges;
    clone->structure = graph->type->representation->clone(graph->structure);

    return clone;
}

GraphObject* graphTranspose(GraphObject* graph) {
    assert(graph && graph->structure);
    GraphObject* transpose = (GraphObject*) malloc(sizeof(GraphObject));

    transpose->type = graph->type;
    transpose->numberOfVertex = graph->numberOfVertex;
    transpose->numberOfEdges = graph->numberOfEdges;
    transpose->structure = graph->type->representation->transpose(graph->structure);

    return transpose;
}

void graphInsertVertex(GraphObject* graph, DataObject* data, int label) {
    assert(graph && graph->structure && data);

    if(graph->type->representation->insertVertex(graph->structure, data, label)) {
        graph->numberOfVertex++;
    }
}

void graphRemoveVertex(GraphObject* graph, int label) {
    assert(graph && graph->structure);

    int edgesRemoved = 0;
    if(graph->type->representation->removeVertex(graph->structure, label, &edgesRemoved)) {
        graph->numberOfVertex--;
        graph->numberOfEdges -= edgesRemoved;
    }
}

void graphInsertEdge(GraphObject* graph, int label1, int label2) {
    assert(graph && graph->structure);

    if(graph->type->representation->insertEdge(graph->structure, label1, label2)) {
        graph->numberOfEdges++;
    }
}

void graphRemoveEdge(GraphObject* graph, int label1, int label2) {
    assert(graph && graph->structure);

    if(graph->type->representation->removeEdge(graph->structure, label1, label2)) {
        graph->numberOfEdges--;
    }
}

bool graphExistsVertex(GraphObject* graph, int label) {
    assert(graph && graph->structure);
    return graph->type->representation->existsVertex(graph->structure, label);
}

bool graphExistsEdge(GraphObject* graph, int label1, int label2) {
    assert(graph && graph->structure);
    return graph->type->representation->existsEdge(graph->structure, label1, label2);
}

DataObject* graphGetVertexData(GraphObject* graph, int label) {
    assert(graph && graph->structure);
    return graph->type->representation->getVertexData(graph->structure, label);
}

void graphSetVertexData(GraphObject* graph, int label, DataObject* data) {
    assert(graph && graph->structure);
    graph->type->representation->setVertexData(graph->structure, label, data);
}

GraphVertex* graphVertexFromPointer(GraphObject* graph, void* vertexId) {
    assert(graph && graph->structure && vertexId);
    return graph->type->representation->vertexFromPointer(graph->structure, vertexId);
}

ITRObject* graphVertices(GraphObject* graph) {
    assert(graph && graph->structure);
    return graph->type->representation->vertices(graph->structure);
}

ITRObject* graphVertexEdges(GraphObject* graph, int label) {
    assert(graph && graph->structure);
    return graph->type->representation->vertexEdges(graph->structure, label);
}

bool graphEqual(GraphObject* graph1, GraphObject* graph2) {
    assert(graph1 && graph2);

    /* Se hanno numero di vertici o numero archi diversi, sono diversi*/
    if( graph1->numberOfVertex != graph2->numberOfVertex ||
        graph1->numberOfEdges != graph2->numberOfEdges) {
        return false;
    }

    //Siccome il tipo è indifferente posso usare il compare di graph1
    return graph1->type->equal(graph1, graph2);
}

bool graphExistsVertexWithData(GraphObject* graph, DataObject* data) {
    assert(graph && data);
    bool exists = false;
    // Passo il GraphObject in quanto richiamo il metodo astratto
    graphBreadthFold(graph, &foldValueExists, &exists, data);
    return exists;
}

bool graphIsAcyclic(GraphObject* graph) {
    assert(graph);

    // Se il grafo è vuoto, è banalmente aciclico
    if(graphEmpty(graph)) {
        return true;
    }

    return graph->type->isAcyclic(graph);
}

ITRObject* graphTopologicalOrder(GraphObject* graph) {
    assert(graph);
    return graph->type->topologicalOrder(graph);
}

GraphObject* graphMaximalReachableSubgraph(GraphObject* graph, int vertexName) {
    assert(graph);
    return graph->type->maximalReachableSubgraph(graph, vertexName);
}

int graphShortestPath(GraphObject* graph, int start, int arrival) {
    assert(graph);
    return graph->type->shortestPath(graph, start, arrival);
}

GraphObject* graphSCCGraph(GraphObject* graph) {
    assert(graph);
    return graph->type->sccGraph(graph);
}

void graphPreOrderMap(GraphObject* graph, MapFun mapFunction, void* parameter) {
    assert(graph);
    graph->type->preOrderMap(graph, mapFunction, parameter);
}

void graphPostOrderMap(GraphObject* graph, MapFun mapFunction, void* parameter) {
    assert(graph);
    graph->type->postOrderMap(graph, mapFunction, parameter);
}

void graphBreadthMap(GraphObject* graph, MapFun mapFunction, void* parameter) {
    assert(graph);
    graph->type->breadthMap(graph, mapFunction, parameter);
}

void graphPreOrderFold(GraphObject* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(graph);
    graph->type->preOrderFold(graph, foldFunction, accumulator, parameter);
}

void graphPostOrderFold(GraphObject* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(graph);
    graph->type->postOrderFold(graph, foldFunction, accumulator, parameter);
}

void graphBreadthFold(GraphObject* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    assert(graph);
    graph->type->breadthFold(graph, foldFunction, accumulator, parameter);
}