#include "graphmatverticesitr.h"

/* ************************************************************************** */

ITRType* ConstructGraphMatVertType() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &graphMatVertConstruct;
    type->destruct = &graphMatVertDestruct;
    type->element = &graphMatVertElement;
    type->successor = &graphMatVertSuccessor;
    type->isTerminated = &graphMatVertTerminated;

    return type;
}

void DestructGraphMatVertType(ITRType* type) {
    free(type);
}

void* graphMatVertConstruct(void* graph) {
    GraphMatVerticesItr* iter = (GraphMatVerticesItr*) malloc(sizeof(GraphMatVerticesItr));
    StackType* type = ConstructStackVecType();

    iter->graph = graph;
    iter->ptrType = ConstructPtrDataType();
    iter->stack = stkConstruct(type);
    iter->node = iter->graph->treeOfNames->root;

    // Controllo il nodo in quanto la radice potrebbe essere NULL se l'albero è vuoto
    if(iter->node) {
        DataObject* ptr = adtConstruct(iter->ptrType);

        // Mi sposto a sinistra e salvo ogni elemento nel percorso
        while(iter->node->left) {
            adtSetValue(ptr, iter->node);
            stkPush(iter->stack, ptr);
            iter->node = iter->node->left;
        }
        adtDestruct(ptr);
    }

    return iter;
}

void graphMatVertDestruct(void* iterator) {
    GraphMatVerticesItr* iter = (GraphMatVerticesItr*) iterator;
    StackType* type = iter->stack->type;

    stkClear(iter->stack);
    stkDestruct(iter->stack);
    DestructStackVecType(type);
    free(iter->ptrType);
    free(iter);
}

bool graphMatVertTerminated(void* iterator) {
    GraphMatVerticesItr* iter = (GraphMatVerticesItr*) iterator;

    // Ritorno true solo se non ho un corrente e lo stack è vuoto
    return !iter->node && stkEmpty(iter->stack);
}

void* graphMatVertElement(void* iterator) {
    GraphMatVerticesItr* iter = (GraphMatVerticesItr*) iterator;

    // Prendo l'ADT di mapping dall'albero
    ADTVertexMap* vertex = adtGetValue(iter->node->value);
    iter->current = vertex->identifier;
    free(vertex);

    // Mi ritorno la sua posizione nell'array
    return &iter->current;
}

void graphMatVertSuccessor(void* iterator) {
    GraphMatVerticesItr* iter = (GraphMatVerticesItr*) iterator;
    DataObject* ptr = adtConstruct(iter->ptrType);

    // Se ho un nodo destro devo scendere e cercare il minimo del suo sottoalbero
    iter->node = iter->node->right;

    if(iter->node) {
        while(iter->node->left) {
            adtSetValue(ptr, iter->node);
            stkPush(iter->stack, ptr);
            iter->node = iter->node->left;
        }
    }
    // Se non ho figlio destro, allora verifico se devo salire
    else if (!stkEmpty(iter->stack)) {
        DataObject* tmp = stkTopNPop(iter->stack);
        iter->node = (BSTNode*)adtGetValue(tmp);
        adtDestruct(tmp);
    }

    adtDestruct(ptr);
}
