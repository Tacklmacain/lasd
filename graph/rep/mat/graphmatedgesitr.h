#ifndef EXERCISE3_GRAPHMATEDGESITR_H
#define EXERCISE3_GRAPHMATEDGESITR_H

/* ************************************************************************** */

#include "../../../list/list.h"
#include "../../../itr/itr.h"
#include "graphmat.h"

/* ************************************************************************** */

/** Iteratore su tutti gli adiacendi di un vertice di una matrice
  * @attribute graph
  * Il graphMat relativo all'iterazione, permette di accedere ai suoi elementi
  *
  * @attribute listEdges
  * La lista di archi di quel vertice, permette di ottenere un ordinamento e
  * restituirli in maniera ordinata
  *
  * @attribute element
  * L'elemento da restituire, rappresenta l'indice del vertice nell'array
  * dei vertici del grafo
  *
  * @attribute intType
  * DataType relativo agli elementi della lista, evita memory leak
  */
typedef struct GraphMatEdgesItr {
    GraphMat* graph;
    ListObject* listEdges;
    uint element;

    DataType* intType;
} GraphMatEdgesItr;

/* ************************************************************************** */

ITRType* ConstructGraphMatEdgesType();
void DestructGraphMatEdgesType(ITRType*);

void* graphMatEdgesConstruct(void*);
void graphMatEdgesDestruct(void*);
bool graphMatEdgesTerminated(void*);
void* graphMatEdgesElement(void*);
void graphMatEdgesSuccessor(void*);

/* ************************************************************************** */

#endif