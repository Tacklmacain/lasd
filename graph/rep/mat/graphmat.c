#include "graphmat.h"
#include "graphmatverticesitr.h"
#include "graphmatedgesitr.h"

/* ************************************************************************** */

GraphRepresentation* ConstructGraphMat() {
    GraphRepresentation* representation = (GraphRepresentation*) malloc(sizeof(GraphRepresentation));

    representation->construct = &GraphMatConstruct;
    representation->destruct = &GraphMatDestruct;
    representation->isEmpty = &GraphMatEmpty;
    representation->clone = &GraphMatClone;
    representation->transpose = &GraphMatTranspose;
    representation->insertVertex = &GraphMatInsertVertex;
    representation->removeVertex = &GraphMatRemoveVertex;
    representation->insertEdge = &GraphMatInsertEdge;
    representation->removeEdge = &GraphMatRemoveEdge;
    representation->existsVertex = &GraphMatExistsVertex;
    representation->existsEdge = &GraphMatExistsEdge;
    representation->getVertexData = &GraphMatGetVertexData;
    representation->setVertexData = &GraphMatSetVertexData;
    representation->vertexFromPointer = &GraphMatVertexFromPointer;
    representation->vertices = &GraphMatVertices;
    representation->vertexEdges = &GraphMatVertexEdges;

    return representation;
}

void DestructGraphMat(GraphRepresentation* repr) {
    free(repr);
}

void* GraphMatConstruct() {
    GraphMat* newGraph = (GraphMat*) malloc(sizeof(GraphMat));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    newGraph->actualIndex = 0;
    newGraph->numberOfVertices = 0;
    newGraph->numberOfEdges = 0;
    newGraph->matrixSize = 4;

    newGraph->vertexType = ConstructGraphMapDataType();
    newGraph->intType = ConstructIntDataType();

    newGraph->vertices = (GraphVertex**) malloc(newGraph->matrixSize * sizeof(GraphVertex*));
    newGraph->adjMatrix = (bool**) malloc(newGraph->matrixSize * sizeof(bool*));
    newGraph->treeOfNames = bstConstruct(bstType);
    newGraph->queueOfIds = queConstruct(queueType);

    /* Alloco la matrice, la calloc alloca un numero di righe pari a matrixSize in automatico
     * ed inizializza i vari puntatori di vertices a NULL */
    for(uint i = 0; i < newGraph->matrixSize; ++i) {
        newGraph->adjMatrix[i] = calloc(newGraph->matrixSize, sizeof(bool));
        newGraph->vertices[i] = NULL;
    }

    return newGraph;
}

void GraphMatDestruct(void* graph) {
    GraphMat* graphToDestroy = (GraphMat*) graph;

    // Dealloco la matrice
    for (uint i = 0; i < graphToDestroy->matrixSize; ++i) {
        free(graphToDestroy->adjMatrix[i]);
    }
    free(graphToDestroy->adjMatrix);

    // Salvo il type, dealloco la queue e dealloco il type
    QueueType* queTypeToDestroy = graphToDestroy->queueOfIds->type;
    queClear(graphToDestroy->queueOfIds);
    queDestruct(graphToDestroy->queueOfIds);
    DestructQueueVecType(queTypeToDestroy);

    // Salvo il type, dealloco il bst e dealloco il type
    BSTType* bstTypetoDestroy = graphToDestroy->treeOfNames->type;
    bstDestruct(graphToDestroy->treeOfNames);
    DestructBSTRecursive(bstTypetoDestroy);

    /* Dealloco i vari vertici, siccome la grandezza dell'array
     * coincide con quella della matrice, scorro con questa size*/
    for (uint i = 0; i < graphToDestroy->matrixSize; ++i) {
        if(graphToDestroy->vertices[i]) {
            adtDestruct(graphToDestroy->vertices[i]->dataObject);
            free(graphToDestroy->vertices[i]);
        }
    }
    free(graphToDestroy->vertices);

    DestructGraphMapDataType(graphToDestroy->vertexType);
    DestructIntDataType(graphToDestroy->intType);
    free(graphToDestroy);
}

bool GraphMatEmpty(void* graph) {
    return ((GraphMat*) graph)->numberOfVertices == 0;
}

/** Clona il grafo sia in struttura che in elementi
  * @param graph il grafo che si intende duplicare
  * @return GraphMat il clone del grafo in ingresso
  */
void* GraphMatClone(void* graph) {
    GraphMat* original = (GraphMat*) graph;

    /* Alloco tutto ciò che mi serve per il clone, devo allocare un nuovo queueType
     * e bstType per non rischiare di perderli cancellando la prima matrice*/
    GraphMat* clone = (GraphMat*) malloc(sizeof(GraphMat));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    clone->actualIndex = original->actualIndex;
    clone->numberOfVertices = original->numberOfVertices;
    clone->numberOfEdges = original->numberOfEdges;
    clone->matrixSize = original->matrixSize;

    clone->vertexType = ConstructGraphMapDataType();
    clone->intType = ConstructIntDataType();

    clone->queueOfIds = queClone(original->queueOfIds);
    clone->queueOfIds->type = queueType;
    clone->treeOfNames = bstClone(original->treeOfNames);
    clone->treeOfNames->type = bstType;
    clone->adjMatrix = (bool**) malloc(original->matrixSize * sizeof(bool*));
    clone->vertices = (GraphVertex**) malloc(original->matrixSize * sizeof(GraphVertex*));

    /* Assegno il type corretto in quanto con la clone il bst e la queue sono
     * pieni di ADT che puntano al type della struttura precedente */
    bstBreadthMap(clone->treeOfNames, &changeType, clone->vertexType);
    queMap(clone->queueOfIds, &changeType, clone->intType);

    for (uint i = 0; i < original->matrixSize; ++i) {
        // Alloco le righe della matrice
        clone->adjMatrix[i] = (bool*) malloc(sizeof(bool) * original->matrixSize);

        // Copio le righe
        for (uint j = 0; j < original->matrixSize; ++j) {
            clone->adjMatrix[i][j] = original->adjMatrix[i][j];
        }

        // Se esiste il vertice lo duplico, altrimenti assegno NULL
        if(original->vertices[i]) {
            GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
            newVertex->name = original->vertices[i]->name;
            newVertex->dataObject = adtClone(original->vertices[i]->dataObject);
            clone->vertices[i] = newVertex;
        }
        else {
            clone->vertices[i] = NULL;
        }
    }
    return clone;
}

/** Fa il trasposto di un grafo
  * @param graph il grafo che si intende trasporre
  * @return traspose il traposto del grafo in ingresso
  */
void* GraphMatTranspose(void* graph) {
    GraphMat* original = (GraphMat*) graph;

    /* Alloco tutto ciò che mi serve per il trasposto, devo allocare un nuovo queueType
     * e bstType per non rischiare di perderli cancellando la prima matrice*/
    GraphMat* transpose = (GraphMat*) malloc(sizeof(GraphMat));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    transpose->actualIndex = original->actualIndex;
    transpose->numberOfVertices = original->numberOfVertices;
    transpose->numberOfEdges = original->numberOfEdges;
    transpose->matrixSize = original->matrixSize;

    transpose->vertexType = ConstructGraphMapDataType();
    transpose->intType = ConstructIntDataType();

    transpose->queueOfIds = queClone(original->queueOfIds);
    transpose->queueOfIds->type = queueType;
    transpose->treeOfNames = bstClone(original->treeOfNames);
    transpose->treeOfNames->type = bstType;
    transpose->adjMatrix = (bool**) malloc(original->matrixSize * sizeof(bool*));
    transpose->vertices = (GraphVertex**) malloc(original->matrixSize * sizeof(GraphVertex*));

    /* Assegno il type corretto in quanto con la clone il bst e la queue sono
     * pieni di ADT che puntano al type della struttura precedente */
    bstBreadthMap(transpose->treeOfNames, &changeType, transpose->vertexType);
    queMap(transpose->queueOfIds, &changeType, transpose->intType);

    for (uint i = 0; i < original->matrixSize; ++i) {
        // Alloco le righe della matrice
        transpose->adjMatrix[i] = (bool*) malloc(sizeof(bool) * original->matrixSize);

        // Faccio il trasposto [i][j] = [j][i]
        for (uint j = 0; j < original->matrixSize; ++j) {
            transpose->adjMatrix[i][j] = original->adjMatrix[j][i];
        }

        // Se esiste il vertice lo duplico, altrimenti assegno NULL
        if(original->vertices[i]) {
            GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
            newVertex->name = original->vertices[i]->name;
            newVertex->dataObject = adtClone(original->vertices[i]->dataObject);
            transpose->vertices[i] = newVertex;
        }
        else {
            transpose->vertices[i] = NULL;
        }
    }
    return transpose;
}

/** Inserisce un vertice nel grafo
  * @param graph il grafo
  * @param data il DataObject che si intende inserire
  * @param vertexName il nome che si intende assegnare al vertice
  * @return true se l'elemento è stato inserito con successo, false altrimenti
  */
bool GraphMatInsertVertex(void* graph, DataObject* data, int vertexName) {
    GraphMat* graphMat = (GraphMat*) graph;

    DataObject* key = adtConstruct(graphMat->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    // Verifico se la chiave già esiste, se esiste già allora ritorno che è fallito l'inserimento
    if(bstExists(graphMat->treeOfNames, key)) {
        adtDestruct(key);
        return false;
    }

    // Creo il nuovo vertice
    GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
    newVertex->dataObject = adtClone(data);
    newVertex->name = vertexName;

    graphMat->numberOfVertices++;

    // Se il numero vertici coincide allora devo reallocare la matrice e l'array di vertici
    if(graphMat->numberOfVertices == graphMat->matrixSize) {
        graphMat->matrixSize *= 2;

        graphMat->vertices = (GraphVertex**) realloc(graphMat->vertices, sizeof(GraphVertex*) * graphMat->matrixSize);
        graphMat->adjMatrix = (bool**) realloc(graphMat->adjMatrix, sizeof(bool*) * graphMat->matrixSize);

        // Realloco la matrice, inizio ad aggiungere spazi a destra e settarli a 0
        for(uint i = 0; i < graphMat->matrixSize / 2; ++i){
            graphMat->adjMatrix[i] = (bool*) realloc(graphMat->adjMatrix[i], sizeof(bool) * graphMat->matrixSize);
            for (uint j = graphMat->matrixSize/2; j < graphMat->matrixSize; ++j) {
                graphMat->adjMatrix[i][j] = false;
            }
        }

        /* Alloco gli spazi in basso alla matrice direttamente con una calloc, setto anche
         * gli indirizzi dell'array a NULL per evitare letture non inizializzate */
        for(uint i = graphMat->matrixSize / 2; i < graphMat->matrixSize; ++i){
            graphMat->adjMatrix[i] = (bool*) calloc(graphMat->matrixSize, sizeof(bool));
            graphMat->vertices[i] = NULL;
        }
    }

    // Se la coda non è vuota posso riciclare un indice
    if(!queEmpty(graphMat->queueOfIds)) {
        DataObject* recycledIndex = queHeadNDequeue(graphMat->queueOfIds);
        int* index = adtGetValue(recycledIndex);

        // Assegno l'indice riciclato (dovrebbe esserci NULL)
        graphMat->vertices[*index] = newVertex;

        ((ADTVertexMap*) key->value)->identifier = *index;

        free(index);
        adtDestruct(recycledIndex);
    }
    else { // Altrimenti devo creare un nuovo indice
        ((ADTVertexMap*) key->value)->identifier = graphMat->actualIndex;
        graphMat->vertices[graphMat->actualIndex++] = newVertex;
    }

    // Inserisco la nuova chiave nell'albero delle mappe
    bstInsert(graphMat->treeOfNames, key);

    adtDestruct(key);
    return true;
}

/** Rimuove un vertice nel grafo
  * @param graph il grafo
  * @param vertexName il nome del vertice da rimuovere
  * @param numberOfRemovedEdges il numero di archi rimossi a seguito dell'eliminazione del vertice
  * @return true se l'elemento è stato rimosso con successo, false altrimenti
  */
bool GraphMatRemoveVertex(void* graph, int vertexName, int* numberOfRemovedEdges) {
    GraphMat* graphMat = (GraphMat*) graph;
    bool ret = false;

    // Dichiaro un DataObject di supporto per cercare e togliere l'elemento
    DataObject* key = adtConstruct(graphMat->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    // Provo a prendere dall'albero l'elemento
    DataObject* index = bstGetValue(graphMat->treeOfNames, key);

    // Se diverso da NULL allora l'elemento esiste
    if(index) {
        // Procedo a toglierlo dall'albero
        bstRemove(graphMat->treeOfNames, key);

        /* Lo tolgo dall'array, per farlo faccio il getValue di index che mi ritorna
         * la sua posizione nell'array*/
        int* ind = adtGetValue(index);
        adtDestruct(graphMat->vertices[*ind]->dataObject);
        free(graphMat->vertices[*ind]);
        graphMat->vertices[*ind] = NULL;

        /* Usando lo stesso indice inizio a rimuovere i suoi archi dalla matrice
         * incrementando il numero di archi rimossi per ogni arco rimosso*/
        for (uint i = 0; i < graphMat->matrixSize; ++i) {
             // Se esiste un arco uscente, lo rimuovo
             if(graphMat->adjMatrix[i][*ind]) {
                 graphMat->adjMatrix[i][*ind] = false;
                 (*numberOfRemovedEdges)++;
                 graphMat->numberOfEdges--;
             }

             // Se esiste un arco entrante, lo rimuovo
             if(graphMat->adjMatrix[*ind][i]) {
                graphMat->adjMatrix[*ind][i] = false;
                 (*numberOfRemovedEdges)++;
                 graphMat->numberOfEdges--;
             }
        }

        // Aggiungo questo vertice alla coda dichiarandomi un DO con quel valore
        DataObject* newQueueItem = adtConstruct(graphMat->intType);
        adtSetValue(newQueueItem, ind);
        queEnqueue(graphMat->queueOfIds, newQueueItem);

        adtDestruct(newQueueItem);
        free(ind);
        graphMat->numberOfVertices--;
        ret = true;
        adtDestruct(index);

        //Se utilizzo 1/4 della matrice devo dimezzare matrice ed array
        if(graphMat->numberOfVertices * 4 == graphMat->matrixSize) {
            reduceAdjMatrix(graphMat);
        }
    }

    adtDestruct(key);
    return ret;
}

/** Inserisce un arco nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco è stato inserito con successo, false altrimenti
  */
bool GraphMatInsertEdge(void* graph, int vertexName1, int vertexName2) {
    GraphMat* graphMat = (GraphMat*) graph;
    bool ret = false;

    // Verifico se i e j esistano entrambi, se uno dei due non esiste allora ritorno subito false
    uint* i = getIndexFromNameMat(graphMat, vertexName1);
    if(!i) {
        return false;
    }

    uint* j = getIndexFromNameMat(graphMat, vertexName2);
    if(!j) {
        free(i);
        return false;
    }

    // Controllo che il vertice non esista
    if(!graphMat->adjMatrix[*i][*j]) {
        graphMat->adjMatrix[*i][*j] = true;
        graphMat->numberOfEdges++;
        ret = true;
    }

    free(i);
    free(j);

    return ret;
}

/** Rimuove un arco nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco è stato rimosso con successo, false altrimenti
  */
bool GraphMatRemoveEdge(void* graph, int vertexName1, int vertexName2) {
    GraphMat* graphMat = (GraphMat*) graph;
    bool ret = false;

    // Se il numero di archi è 0, banalmente ritorno false
    if(graphMat->numberOfEdges == 0) {
        return false;
    }

    // Verifico se i e j esistano entrambi, se uno dei due non esiste allora ritorno subito false
    uint* i = getIndexFromNameMat(graphMat, vertexName1);
    if(!i) {
        return false;
    }

    uint* j = getIndexFromNameMat(graphMat, vertexName2);
    if(!j) {
        free(i);
        return false;
    }

    // Controllo che il vertice esista
    if(graphMat->adjMatrix[*i][*j]) {
        graphMat->adjMatrix[*i][*j] = false;
        graphMat->numberOfEdges--;
        ret = true;
    }

    free(i);
    free(j);
    return ret;
}

/** Verifica se un vertice esiste nel grafo
  * @param graph il grafo
  * @param vertexName il nome del vertice da cercare
  * @return true se il vertice con tale nome esiste esiste, false altrimenti
  */
bool GraphMatExistsVertex(void* graph, int vertexName) {
    GraphMat* graphMat = (GraphMat*) graph;

    // Se non ho vertici, allora non esiste
    if(graphMat->numberOfVertices == 0) {
        return false;
    }

    // Richiamo la funzione di map, ritorna NULL se non lo trova
    uint* tmp = getIndexFromNameMat(graphMat, vertexName);

    if(tmp) {
        free(tmp);
        return true;
    }
    return false;
}

/** Verifica se un arco esiste nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco esiste, false altrimenti
  */
bool GraphMatExistsEdge(void* graph, int vertexName1, int vertexName2) {
    GraphMat* graphMat = (GraphMat*) graph;
    bool ret;

    // Se il numero di archi è 0, banalmente ritorno false
    if(graphMat->numberOfEdges == 0) {
        return false;
    }

    // Verifico se i e j esistano entrambi, se uno dei due non esiste allora ritorno subito false
    uint* i = getIndexFromNameMat(graphMat, vertexName1);
    if(!i) {
        return false;
    }

    uint* j = getIndexFromNameMat(graphMat, vertexName2);
    if(!j) {
        free(i);
        return false;
    }

    // Verifica effettiva se il vertice esiste
    ret = (graphMat->adjMatrix[*i][*j]) ? true : false;

    free(i);
    free(j);
    return ret;
}

/** Ritorna una copia del DataObject collegato a quel nome
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @return DataObject la copia del DataObject se esiste, NULL altrimenti
  */
DataObject* GraphMatGetVertexData(void* graph, int vertexName) {
    GraphMat* graphMat = (GraphMat*) graph;

    // Se il grafo non ha vertici, banalmente ritorno NULL
    if(graphMat->numberOfVertices == 0) {
        return NULL;
    }

    // Verifico se esiste in corrispondenza nell'albero
    uint* i = getIndexFromNameMat(graphMat, vertexName);
    if(!i) {
        return NULL;
    }

    // Se arrivo qui allora esiste e posso ritornarne una copia
    DataObject* copy = adtClone(graphMat->vertices[*i]->dataObject);
    free(i);
    return copy;
}

/** Sovrascrive il DataObject collegato a quel nome
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @param newData Il nuovo dato che si intende inserire
  */
void GraphMatSetVertexData(void* graph, int vertexName, DataObject* newData) {
    GraphMat* graphMat = (GraphMat*) graph;

    // Il grafo deve avere almeno un vertice per fare la set
    if(graphMat->numberOfVertices > 0) {
        // Verifico se esiste la sua corrispondenza nell'albero
        uint *i = getIndexFromNameMat(graphMat, vertexName);

        if (i) {
            // Se arrivo qui allora esiste, elimino il precedente ed assegno una copia
            adtDestruct(graphMat->vertices[*i]->dataObject);
            graphMat->vertices[*i]->dataObject = adtClone(newData);
            free(i);
        }
    }
}

/** Ritorna un vertice partendo da un puntatore
  * @param graph il grafo che contiene quel vertice
  * @param vertexId la posizione del vertice nell'array del grafo
  * @return GraphVertex il vertice del grafo
  */
GraphVertex* GraphMatVertexFromPointer(void* graph, void* vertexId) {
    GraphMat* graphMat = (GraphMat*) graph;
    uint index = *((uint*) vertexId);

    assert(index < graphMat->matrixSize);

    return graphMat->vertices[index];
}

/** Ritorna un iteratore su tutti i vertici del grafo
  * @param grafo su cui si intende iterare
  * @return ITRObject che permette l'iterazione
  */
void* GraphMatVertices(void* graph) {
    ITRType* graphMatVert = ConstructGraphMatVertType();
    ITRObject* iterator = itrConstruct(graphMatVert, graph);

    return iterator;
}

/** Ritorna un iteratore sugli adiacenti di un dato vertice
  * @param grafo su cui si intende iterare
  * @param vertexName il nome del vertice su cui cercare gli adiacenti
  * @return ITRObject che permette l'iterazione
  */
void* GraphMatVertexEdges(void* graph, int vertexName) {
    // Creo un graphMatSupport
    GraphMatSupport* support = (GraphMatSupport*) malloc(sizeof(GraphMatSupport));
    GraphMat* graphMat = (GraphMat*) graph;

    support->graph = graph;

    // Converto il nome in indice per l'array se esiste
    uint* tmp = getIndexFromNameMat(graph, vertexName);

    if(!tmp) {
        // Se il vertice non esiste ritorno matrixSize per forzare subito la terminazione
        support->vertexIndex = graphMat->matrixSize;
    }
    else {
        support->vertexIndex = *tmp;
        free(tmp);
    }

    ITRType* graphMatEdges = ConstructGraphMatEdgesType();
    ITRObject* iterator = itrConstruct(graphMatEdges, support);
    free(support);
    return iterator;
}

/** Ridimensiona correttamente la matrice e l'array dei vertici
  * @param graph il grafo
  */
void reduceAdjMatrix(GraphMat* graphMat) {
    // Creo una nuova matrice più piccola
    bool** newMatrix = (bool**) malloc((graphMat->matrixSize / 2) * sizeof(bool*));

    // Alloco la matrice, la calloc alloca un numero di righe pari a matrixSize/2 in automatico
    for(uint i = 0; i < graphMat->matrixSize / 2; ++i) {
        newMatrix[i] = calloc(graphMat->matrixSize / 2, sizeof(bool));
    }

    // Creo un nuovo array più piccolo che riempirò man mano dei nuovi vertici in posizione corretta
    GraphVertex** newArray = (GraphVertex**) calloc(graphMat->matrixSize / 2, sizeof(GraphVertex*));

    /* Scorro l'array dei vertici per scoprire quali locazioni della vecchia matrice devo esplorare
     * i è la posizione i-esima dell'array
     * j è l'elemento j-esimo dell'array
     * k è la riga k-esima della nuova matrice
     * l è la colonna l-esima della nuova matrice
     */
    uint k = 0, l = 0;
    for (uint i = 0; i < graphMat->matrixSize; ++i) {

        // Ho trovato un vertice candidato in posizione i
        if(graphMat->vertices[i]) {

            // Imposto l'indice delle colonne a 0
            l = 0;

            // Cerco tutti gli altri vertici j che possono essere collegati ad i
            for (uint j = 0; j < graphMat->matrixSize; ++j) {

                // Se il vertice j esiste, allora devo prendere il loro collegamento nella nuova matrice
                if(graphMat->vertices[j]) {
                    newMatrix[k][l] = graphMat->adjMatrix[i][j];
                    l++;
                }
            }

            // Refresho l'indice nell'albero
            DataObject* tmp = adtConstruct(graphMat->vertexType);
            ((ADTVertexMap*) tmp->value)->vertexName = graphMat->vertices[i]->name;

            DataObject* oldVertex = bstGetValue(graphMat->treeOfNames, tmp);
            ((ADTVertexMap*) oldVertex->value)->identifier = k;
            bstRemove(graphMat->treeOfNames, tmp);
            bstInsert(graphMat->treeOfNames, oldVertex);

            adtDestruct(oldVertex);
            adtDestruct(tmp);

            //Inserisco il vertice corrente nel nuovo array
            newArray[k] = graphMat->vertices[i];

            //Indipendentemente dall'esito, avanzo alla riga successiva
            k++;
        }
    }

    // Svuoto la queue degli indici, potrebbe essere sporca e puntare ad elementi fuori dalla nuova matrice
    queClear(graphMat->queueOfIds);

    // Il nuovo actualIndex deve partire da k
    graphMat->actualIndex = k;

    // Libero la memoria del vechio array
    free(graphMat->vertices);

    // Dealloco la matrice
    for (uint i = 0; i < graphMat->matrixSize; ++i) {
        free(graphMat->adjMatrix[i]);
    }
    free(graphMat->adjMatrix);

    // Sovrascrivo i vecchi valori e dimezzo la grandezza
    graphMat->vertices = newArray;
    graphMat->adjMatrix = newMatrix;
    graphMat->matrixSize /= 2;
}

/** Converte una nome in un indice per la matrice e l'array
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @return uint indice se esiste, NULL altrimenti
  */
uint* getIndexFromNameMat(GraphMat *graph, int vertexName) {
    GraphMat* graphMat = (GraphMat*) graph;
    uint* ret = NULL;

    // Dichiaro un DataObject di supporto per verificare l'esistenza del nome
    DataObject* key = adtConstruct(graphMat->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    DataObject* index = bstGetValue(graphMat->treeOfNames, key);

    // Se mi è tornato un valore allora esiste corrispondenza
    if(index) {
        ret = adtGetValue(index);
    }

    adtDestruct(key);
    adtDestruct(index);
    return ret;
}