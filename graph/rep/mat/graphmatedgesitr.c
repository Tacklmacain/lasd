#include "graphmat.h"
#include "graphmatedgesitr.h"

/* ************************************************************************** */

ITRType* ConstructGraphMatEdgesType() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &graphMatEdgesConstruct;
    type->destruct = &graphMatEdgesDestruct;
    type->element = &graphMatEdgesElement;
    type->successor = &graphMatEdgesSuccessor;
    type->isTerminated = &graphMatEdgesTerminated;

    return type;
}

void DestructGraphMatEdgesType(ITRType* type) {
    free(type);
}

void* graphMatEdgesConstruct(void* graphSupport) {
    GraphMatEdgesItr* iter = (GraphMatEdgesItr*) malloc(sizeof(GraphMatEdgesItr));
    GraphMatSupport* graphMatSupport = (GraphMatSupport*) graphSupport;

    iter->graph = graphMatSupport->graph;
    iter->intType = ConstructIntDataType();
    iter->listEdges = lstConstruct();

    uint i = graphMatSupport->vertexIndex, current = 0;
    DataObject* newElem = adtConstruct(iter->intType);

    // Controllo che l'elemento corrente sia lecito
    if(i < iter->graph->matrixSize) {
        // Scorro fino alla fine della matrice
        while(current < iter->graph->matrixSize) {
            // Se l'arco esiste lo aggiungo in lista
            if(iter->graph->adjMatrix[i][current]) {
                int vertexName = graphMatSupport->graph->vertices[current]->name;
                adtSetValue(newElem, &vertexName);
                lstInsert(iter->listEdges, newElem);
            }
            current++;
        }
    }

    adtDestruct(newElem);
    return iter;
}

void graphMatEdgesDestruct(void* iterator) {
    GraphMatEdgesItr* iter = (GraphMatEdgesItr*) iterator;

    lstDestruct(iter->listEdges);
    DestructIntDataType(iter->intType);
    free(iterator);
}

bool graphMatEdgesTerminated(void* iterator) {
    GraphMatEdgesItr* iter = (GraphMatEdgesItr*) iterator;
    return lstEmpty(iter->listEdges);
}

void* graphMatEdgesElement(void* iterator) {
    GraphMatEdgesItr* iter = (GraphMatEdgesItr*) iterator;

    DataObject* tmp = iter->listEdges->head->data;
    int* vertexName = (int*) adtGetValue(tmp);
    uint* index = getIndexFromNameMat(iter->graph, *vertexName);
    iter->element = *index;

    free(vertexName);
    free(index);

    return &iter->element;
}

void graphMatEdgesSuccessor(void* iterator) {
    GraphMatEdgesItr* iter = (GraphMatEdgesItr*) iterator;

    DataObject* tmp = iter->listEdges->head->data;
    lstRemove(iter->listEdges, tmp);
}