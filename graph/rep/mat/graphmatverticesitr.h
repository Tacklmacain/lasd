#ifndef EXERCISE3_GRAPHMATVERTICESITR_H
#define EXERCISE3_GRAPHMATVERTICESITR_H

/* ************************************************************************** */

#include "../../../adt/ptr/adtptr.h"
#include "../../../stack/vec/stackvec.h"
#include "../../../stack/stack.h"
#include "../../../list/list.h"
#include "../../../itr/itr.h"
#include "graphmat.h"

/* ************************************************************************** */

/** Iteratore su tutti i vertici del grafo
  * @attribute graph
  * Il grafo relativo all'iterazione, permette di accedere ai suoi elementi
  *
  * @attribute stack
  * Memorizza i vari elementi siccome itero sull'albero
  *
  * @attribute node
  * Indica l'elemento corrente dell'albero
  *
  * @attribute current
  * Serve per restituire il valore di output
  *
  * @attribute ptrType
  * DataType relativo agli elementi dello stack, evita memory leak
  */
typedef struct GraphMatVerticesItr {
    GraphMat* graph;
    StackObject* stack;
    BSTNode* node;
    uint current;

    DataType* ptrType;
} GraphMatVerticesItr;

/* ************************************************************************** */

ITRType* ConstructGraphMatVertType();
void DestructGraphMatVertType(ITRType*);

void* graphMatVertConstruct(void*);
void graphMatVertDestruct(void*);
bool graphMatVertTerminated(void*);
void* graphMatVertElement(void*);
void graphMatVertSuccessor(void*);

/* ************************************************************************** */

#endif