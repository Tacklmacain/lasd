
#ifndef GRAPHMAT_H
#define GRAPHMAT_H

/* ************************************************************************** */

#include "../../graph.h"

/* ************************************************************************** */

/** Struttura del grafo rappresentato con matrici di adiacenza
  * @attribute adjMatrix
  * Matrice di adiacenza, dichiarata come bool indica se esiste o meno l'arco
  *
  * @attribute treeOfNames
  * Albero Binario che si occupa di fare corrispondenza fra nome del vertice ed il suo indice
  * nell'array, utilizza l'adt "vertex"
  *
  * @attribute vertices
  * Array di vertici, messo in corrispondenza con treeOfNames
  *
  * @attribute queueOfIds
  * Queue di interi che gestisce gli indici da riciclare, se un vertice viene eliminato il suo
  * identificativo finisce in questa coda e può essere utilizzato al prossimo inserimento
  *
  * @attribute numberOfVertices, numberOfEdges
  * Rappresentano il numero di vertici ed archi, velocizzano alcune funzioni
  *
  * @attribute matrixSize
  * Indica la grandezza della matrice, viene utilizzato per reallocarla se necessario
  *
  * @attribute actualIndex
  * Indica l'indice massimo attualmente utilizzato nella matrice, un grafo di n elementi avrà
  * come valore massimo di actualIndex n-1, si incrementa se la matrice è piena all'inserimento
  *
  * @attribute vertexType
  * Contiene il DataType relativo ai vertici che hanno identificatore e nome, evita memory leak
  *
  * @attribute intType
  * Contiene il DataType di interi per la queue, evita memory leak
  */
typedef struct GraphMat {
    bool** adjMatrix;
    BSTObject* treeOfNames;
    GraphVertex** vertices;
    QueueObject* queueOfIds;

    uint numberOfVertices, numberOfEdges;
    uint matrixSize;
    uint actualIndex;

    DataType *vertexType, *intType;
} GraphMat;

/** Struttura di supporto per poter construire un iteratore sugli adiacenti
  * @attribute graph
  * Il grafo a matrici di adiacenza su cui si intende iterare
  *
  * @attribute vertexIndex
  * L'indice di quel vertice
  */
typedef struct GraphMatSupport {
    GraphMat* graph;
    uint vertexIndex;
} GraphMatSupport;

/* ************************************************************************** */

GraphRepresentation* ConstructGraphMat();
void DestructGraphMat(GraphRepresentation*);

void* GraphMatConstruct();
void GraphMatDestruct(void*);

bool GraphMatEmpty(void*);

void* GraphMatClone(void*);
void* GraphMatTranspose(void*);

bool GraphMatInsertVertex(void*, DataObject*, int);
bool GraphMatRemoveVertex(void*, int, int*);

bool GraphMatInsertEdge(void*, int, int);
bool GraphMatRemoveEdge(void*, int, int);

bool GraphMatExistsVertex(void*, int);
bool GraphMatExistsEdge(void*, int, int);

DataObject* GraphMatGetVertexData(void*, int);
void GraphMatSetVertexData(void*, int, DataObject*);

GraphVertex* GraphMatVertexFromPointer(void*, void*);

void* GraphMatVertices(void*);
void* GraphMatVertexEdges(void*, int);

/* ************************************************************************** */
// Funzioni di supporto
void reduceAdjMatrix(GraphMat* graphMat);
uint* getIndexFromNameMat(GraphMat *graph, int vertexName);


#endif
