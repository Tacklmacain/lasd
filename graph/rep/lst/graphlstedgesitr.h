#ifndef EXERCISE3_GRAPHLSTEDGESITR_H
#define EXERCISE3_GRAPHLSTEDGESITR_H

/* ************************************************************************** */

#include "../../../list/list.h"
#include "../../../itr/itr.h"
#include "graphlst.h"

/* ************************************************************************** */

/** Iteratore su tutti gli adiacendi di un vertice di grafo con liste di adiacenza
  * @attribute graph
  * Il graphLst relativo all'iterazione, permette di accedere ai suoi elementi
  *
  * @attribute current
  * Indica il nodo attuale nella lista
  *
  * @attribute currentIndex
  * Indica l'elemento corrente nell'array dei vertici
  */
typedef struct GraphLstEdgesItr {
    GraphLst* graph;
    ListNode* current;
    uint currentIndex;
} GraphLstEdgesItr;

/* ************************************************************************** */

ITRType* ConstructGraphLstEdgesType();
void DestructGraphLstEdgesType(ITRType*);

void* graphLstEdgesConstruct(void*);
void graphLstEdgesDestruct(void*);
bool graphLstEdgesTerminated(void*);
void* graphLstEdgesElement(void*);
void graphLstEdgesSuccessor(void*);

/* ************************************************************************** */

#endif