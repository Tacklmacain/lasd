
#ifndef GRAPHLST_H
#define GRAPHLST_H

/* ************************************************************************** */

#include "../../../list/list.h"
#include "../../graph.h"

/* ************************************************************************** */

/** Struttura del grafo rappresentato con liste di adiacenza
  * @attribute adjList
  * La lista di adiacenza
  *
  * @attribute treeOfNames
  * Albero Binario che si occupa di fare corrispondenza fra nome del vertice ed il suo indice
  * nell'array, utilizza l'adt "vertex"
  *
  * @attribute vertices
  * Array di vertici, messo in corrispondenza con treeOfNames
  *
  * @attribute queueOfIds
  * Queue di interi che gestisce gli indici da riciclare, se un vertice viene eliminato il suo
  * identificativo finisce in questa coda e può essere utilizzato al prossimo inserimento
  *
  * @attribute numberOfVertices, numberOfEdges
  * Rappresentano il numero di vertici ed archi, velocizzano alcune funzioni
  *
  * @attribute arraySize
  * Indica la grandezza dell'array di liste di adiacenza, viene utilizzato per reallocarla se necessario
  *
  * @attribute actualIndex
  * Indica l'indice massimo attualmente utilizzato nell'array, un grafo di n elementi avrà
  * come valore massimo di actualIndex n-1, si incrementa se l'array è pieno all'inserimento
  *
  * @attribute vertexType
  * Contiene il DataType relativo ai vertici che hanno identificatore e nome, evita memory leak
  *
  * @attribute intType
  * Contiene il DataType di interi per la queue, evita memory leak
  */
typedef struct GraphLst {
    ListObject** adjList;
    BSTObject* treeOfNames;
    GraphVertex** vertices;
    QueueObject* queueOfIds;

    uint numberOfVertices, numberOfEdges;
    uint arraySize;
    uint actualIndex;

    DataType *vertexType, *intType;
} GraphLst;

/** Struttura di supporto per poter construire un iteratore sugli adiacenti
  * @attribute graph
  * Il grafo a matrici di adiacenza su cui si intende iterare
  *
  * @attribute vertexAdj
  * Il primo elemento della lista di adiacenza di quel nodo
  */
typedef struct GraphLstSupport {
    GraphLst* graph;
    ListNode* vertexAdj;
} GraphLstSupport;

/* ************************************************************************** */

GraphRepresentation* ConstructGraphLst();
void DestructGraphLst(GraphRepresentation*);

void* GraphLstConstruct();
void GraphLstDestruct(void*);

bool GraphLstEmpty(void*);

void* GraphLstClone(void*);
void* GraphLstTranspose(void*);

bool GraphLstInsertVertex(void*, DataObject*, int);
bool GraphLstRemoveVertex(void*, int, int*);

bool GraphLstInsertEdge(void*, int, int);
bool GraphLstRemoveEdge(void*, int, int);

bool GraphLstExistsVertex(void*, int);
bool GraphLstExistsEdge(void*, int, int);

DataObject* GraphLstGetVertexData(void*, int);
void GraphLstSetVertexData(void*, int, DataObject*);

GraphVertex* GraphLstVertexFromPointer(void*, void*);

void* GraphLstVertices(void*);
void* GraphLstVertexEdges(void*, int);

/* ************************************************************************** */
// Funzioni di supporto
void reduceAdjList(GraphLst*);
uint* getIndexFromNameLst(GraphLst*, int);

#endif