#include "graphlst.h"
#include "graphlstverticesitr.h"
#include "graphlstedgesitr.h"

/* ************************************************************************** */

GraphRepresentation* ConstructGraphLst() {
    GraphRepresentation* representation = (GraphRepresentation*) malloc(sizeof(GraphRepresentation));

    representation->construct = &GraphLstConstruct;
    representation->destruct = &GraphLstDestruct;
    representation->isEmpty = &GraphLstEmpty;
    representation->clone = &GraphLstClone;
    representation->transpose = &GraphLstTranspose;
    representation->insertVertex = &GraphLstInsertVertex;
    representation->removeVertex = &GraphLstRemoveVertex;
    representation->insertEdge = &GraphLstInsertEdge;
    representation->removeEdge = &GraphLstRemoveEdge;
    representation->existsVertex = &GraphLstExistsVertex;
    representation->existsEdge = &GraphLstExistsEdge;
    representation->getVertexData = &GraphLstGetVertexData;
    representation->setVertexData = &GraphLstSetVertexData;
    representation->vertexFromPointer = &GraphLstVertexFromPointer;
    representation->vertices = &GraphLstVertices;
    representation->vertexEdges = &GraphLstVertexEdges;

    return representation;
}

void DestructGraphLst(GraphRepresentation* repr) {
    free(repr);
}

void* GraphLstConstruct() {
    GraphLst* newGraph = (GraphLst*) malloc(sizeof(GraphLst));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    newGraph->actualIndex = 0;
    newGraph->numberOfVertices = 0;
    newGraph->numberOfEdges = 0;
    newGraph->arraySize = 4;

    newGraph->vertexType = ConstructGraphMapDataType();
    newGraph->intType = ConstructIntDataType();

    newGraph->vertices = (GraphVertex**) malloc(newGraph->arraySize * sizeof(GraphVertex*));
    newGraph->adjList = (ListObject**) malloc(newGraph->arraySize * sizeof(ListObject*));
    newGraph->treeOfNames = bstConstruct(bstType);
    newGraph->queueOfIds = queConstruct(queueType);

    // Imposto i vari indici della lista di adiacenza ed array dei vertici a NULL
    for(uint i = 0; i < newGraph->arraySize; ++i) {
        newGraph->adjList[i] = NULL;
        newGraph->vertices[i] = NULL;
    }

    return newGraph;
}

void GraphLstDestruct(void* graph) {
    GraphLst* graphToDestroy = (GraphLst*) graph;

    // Dealloco le liste di adiacenza
    for (uint i = 0; i < graphToDestroy->arraySize; ++i) {
        if(graphToDestroy->adjList[i]) {
            lstDestruct(graphToDestroy->adjList[i]);
        }
    }
    free(graphToDestroy->adjList);

    // Salvo il type, dealloco la queue e dealloco il type
    QueueType* queTypeToDestroy = graphToDestroy->queueOfIds->type;
    queClear(graphToDestroy->queueOfIds);
    queDestruct(graphToDestroy->queueOfIds);
    DestructQueueVecType(queTypeToDestroy);

    // Salvo il type, dealloco il bst e dealloco il type
    BSTType* bstTypetoDestroy = graphToDestroy->treeOfNames->type;
    bstDestruct(graphToDestroy->treeOfNames);
    DestructBSTRecursive(bstTypetoDestroy);

    /* Dealloco i vari vertici e libero man mano */
    for (uint i = 0; i < graphToDestroy->arraySize; ++i) {
        if(graphToDestroy->vertices[i]) {
            adtDestruct(graphToDestroy->vertices[i]->dataObject);
            free(graphToDestroy->vertices[i]);
        }
    }
    free(graphToDestroy->vertices);

    DestructGraphMapDataType(graphToDestroy->vertexType);
    DestructIntDataType(graphToDestroy->intType);
    free(graphToDestroy);
}

bool GraphLstEmpty(void* graph) {
    return ((GraphLst*) graph)->numberOfVertices == 0;
}

/** Clona il grafo sia in struttura che in elementi
  * @param graph il grafo che si intende duplicare
  * @return GraphLst clone del grafo in ingresso
  */
void* GraphLstClone(void* graph) {
    GraphLst* original = (GraphLst*) graph;

    /* Alloco tutto ciò che mi serve per il clone, devo allocare un nuovo queueType
     * e bstType per non rischiare di perderli cancellando il grafo originale */
    GraphLst* clone = (GraphLst*) malloc(sizeof(GraphLst));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    clone->actualIndex = original->actualIndex;
    clone->numberOfVertices = original->numberOfVertices;
    clone->numberOfEdges = original->numberOfEdges;
    clone->arraySize = original->arraySize;

    clone->vertexType = ConstructGraphMapDataType();
    clone->intType = ConstructIntDataType();

    clone->queueOfIds = queClone(original->queueOfIds);
    clone->queueOfIds->type = queueType;
    clone->treeOfNames = bstClone(original->treeOfNames);
    clone->treeOfNames->type = bstType;
    clone->adjList = (ListObject**) malloc(original->arraySize * sizeof(ListObject*));
    clone->vertices = (GraphVertex**) malloc(original->arraySize * sizeof(GraphVertex*));

    /* Assegno il type corretto in quanto con la clone il bst e la queue sono
     * pieni di ADT che puntano al type della struttura precedente */
    bstBreadthMap(clone->treeOfNames, &changeType, clone->vertexType);
    queMap(clone->queueOfIds, &changeType, clone->intType);

    // Ciclo la lista del grafo originale e duplico man mano vertici e liste
    for (uint i = 0; i < original->arraySize; ++i) {

        /* Se esiste il vertice lo duplico altrimenti assegno NULL a lui
         * ed alla sua lista di adiacenza */
        if(original->vertices[i]) {
            GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
            newVertex->name = original->vertices[i]->name;
            newVertex->dataObject = adtClone(original->vertices[i]->dataObject);
            clone->vertices[i] = newVertex;

            /* Se questo vertice ha una lista di adiacenza la clono,
             * altrimenti assegno NULL */
            if(original->adjList[i]) {
                clone->adjList[i] = lstClone(original->adjList[i]);

                // Assegno il tipo corretto agli elementi della lista
                lstMap(clone->adjList[i], &changeType, clone->intType);
            }
            else {
                clone->adjList[i] = NULL;
            }
        }
        else {
            clone->vertices[i] = NULL;
            clone->adjList[i] = NULL;
        }
    }

    return clone;
}

/** Fa il trasposto di un grafo
  * @param graph il grafo che si intende trasporre
  * @return traspose il traposto del grafo in ingresso
  */
void* GraphLstTranspose(void* graph) {
    GraphLst* original = (GraphLst*) graph;

    /* Alloco tutto ciò che mi serve per il trasposto, devo allocare un nuovo queueType
     * e bstType per non rischiare di perderli cancellando il grafo originale */
    GraphLst* transpose = (GraphLst*) malloc(sizeof(GraphLst));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    transpose->actualIndex = original->actualIndex;
    transpose->numberOfVertices = original->numberOfVertices;
    transpose->numberOfEdges = original->numberOfEdges;
    transpose->arraySize = original->arraySize;

    transpose->vertexType = ConstructGraphMapDataType();
    transpose->intType = ConstructIntDataType();

    transpose->queueOfIds = queClone(original->queueOfIds);
    transpose->queueOfIds->type = queueType;
    transpose->treeOfNames = bstClone(original->treeOfNames);
    transpose->treeOfNames->type = bstType;
    transpose->adjList = (ListObject**) malloc(original->arraySize * sizeof(ListObject*));
    transpose->vertices = (GraphVertex**) malloc(original->arraySize * sizeof(GraphVertex*));

    /* Assegno il type corretto in quanto con la clone il bst e la queue sono
     * pieni di ADT che puntano al type della struttura precedente */
    bstBreadthMap(transpose->treeOfNames, &changeType, transpose->vertexType);
    queMap(transpose->queueOfIds, &changeType, transpose->intType);

    // Inizializzo la lista di adiacenza di transpose a NULL
    for (int i = 0; i < transpose->arraySize; ++i) {
        transpose->adjList[i] = NULL;
    }

    // Ciclo la lista del grafo originale e traspongo man mano i vertici
    DataObject* vertexName = adtConstruct(original->intType);
    for (uint i = 0; i < original->arraySize; ++i) {
        /* Se esiste il vertice lo duplico altrimenti assegno NULL a lui
         * ed alla sua lista di adiacenza */
        if (original->vertices[i]) {
            GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
            newVertex->name = original->vertices[i]->name;
            newVertex->dataObject = adtClone(original->vertices[i]->dataObject);
            transpose->vertices[i] = newVertex;

            // Se questo vertice ha una lista di adiacenza la traspongo
            if (original->adjList[i]) {
                // Memorizzo il nome del vertice corrente ed inizio a scorrere
                adtSetValue(vertexName, &transpose->vertices[i]->name);
                ListNode *travel = original->adjList[i]->head;

                /* Scorro la lista di i e per ognuno dei suoi vertici aggiungo l'arco
                 * verso i */
                while (travel) {
                    int* name = (int*) adtGetValue(travel->data);
                    uint* index = getIndexFromNameLst(original, *name);

                    // Se la lista adiacenti di questo nodo non esiste, la creo
                    if (!transpose->adjList[*index]) {
                        transpose->adjList[*index] = lstConstruct();
                    }

                    // Inserisco l'elemento
                    lstInsert(transpose->adjList[*index], vertexName);
                    travel = travel->next;

                    free(name);
                    free(index);
                }
            }
        }
        else {
            transpose->vertices[i] = NULL;
        }
    }

    /* Assegno il type corretto in quanto con la lstInsert la lista è
     * piena di ADT che puntano al type della struttura precedente */
    for (int i = 0; i < transpose->arraySize; ++i) {
        if(transpose->adjList[i]) {
            lstMap(transpose->adjList[i], &changeType, transpose->intType);
        }
    }

    adtDestruct(vertexName);

    return transpose;
}

/** Inserisce un vertice nel grafo
  * @param graph il grafo
  * @param data il DataObject che si intende inserire
  * @param vertexName il nome che si intende assegnare al vertice
  * @return true se l'elemento è stato inserito con successo, false altrimenti
  */
bool GraphLstInsertVertex(void* graph, DataObject* data, int vertexName) {
    GraphLst* graphLst = (GraphLst*) graph;

    DataObject* key = adtConstruct(graphLst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    /* Verifico se la chiave già esiste, se esiste già allora la distruggo e
     * ritorno che è fallito l'inserimento */
    if(bstExists(graphLst->treeOfNames, key)) {
        adtDestruct(key);
        return false;
    }

    // Creo il nuovo vertice
    GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
    newVertex->dataObject = adtClone(data);
    newVertex->name = vertexName;

    graphLst->numberOfVertices++;

    // Se il numero vertici coincide allora devo reallocare l'array di vertici e liste
    if(graphLst->numberOfVertices == graphLst->arraySize) {
        graphLst->arraySize *= 2;

        graphLst->vertices = (GraphVertex**) realloc(graphLst->vertices, sizeof(GraphVertex*) * graphLst->arraySize);
        graphLst->adjList = (ListObject**) realloc(graphLst->adjList, sizeof(ListObject*) * graphLst->arraySize);

        /* Imposto i nuovi spazi allocati a NULL per evitare letture non inizializzate */
        for(uint i = graphLst->arraySize / 2; i < graphLst->arraySize; ++i){
            graphLst->adjList[i] = NULL;
            graphLst->vertices[i] = NULL;
        }
    }

    // Se la coda non è vuota posso riciclare un indice
    if(!queEmpty(graphLst->queueOfIds)) {
        DataObject* recycledIndex = queHeadNDequeue(graphLst->queueOfIds);
        uint* index = adtGetValue(recycledIndex);

        // Assegno l'indice riciclato (dovrebbe esserci NULL)
        graphLst->vertices[*index] = newVertex;

        ((ADTVertexMap*) key->value)->identifier = *index;

        free(index);
        adtDestruct(recycledIndex);
    }
    else { // Altrimenti devo creare un nuovo indice
        ((ADTVertexMap*) key->value)->identifier = graphLst->actualIndex;
        graphLst->vertices[graphLst->actualIndex++] = newVertex;
    }

    // Inserisco la nuova chiave nell'albero delle mappe
    bstInsert(graphLst->treeOfNames, key);

    adtDestruct(key);
    return true;
}

/** Rimuove un vertice nel grafo
  * @param graph il grafo
  * @param vertexName il nome del vertice da rimuovere
  * @param numberOfRemovedEdges il numero di archi rimossi a seguito dell'eliminazione del vertice
  * @return true se l'elemento è stato rimosso con successo, false altrimenti
  */
bool GraphLstRemoveVertex(void* graph, int vertexName, int* numberOfRemovedEdges) {
    GraphLst* graphLst = (GraphLst*) graph;
    bool ret = false;

    // Dichiaro un DataObject di supporto per cercare e togliere l'elemento
    DataObject* key = adtConstruct(graphLst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    // Provo a prendere dall'albero l'elemento
    DataObject* index = bstGetValue(graphLst->treeOfNames, key);

    // Se diverso da NULL allora l'elemento esiste
    if(index) {
        // Inizio ad impostare che il valore di ritorno sarà true
        ret = true;

        // Procedo a toglierlo dall'albero
        bstRemove(graphLst->treeOfNames, key);

        /* Lo tolgo dall'array, per farlo faccio il getValue di index che mi
         * ritorna a sua posizione nell'array*/
        int* ind = adtGetValue(index);
        adtDestruct(graphLst->vertices[*ind]->dataObject);
        free(graphLst->vertices[*ind]);
        graphLst->vertices[*ind] = NULL;

        // Procedo ad eliminare eventuali archi uscenti se esistono
        if(graphLst->adjList[*ind]) {
            // Tolgo il numero di archi ed aggiorno la variabile
            graphLst->numberOfEdges -= graphLst->adjList[*ind]->size;
            *numberOfRemovedEdges += graphLst->adjList[*ind]->size;
            lstDestruct(graphLst->adjList[*ind]);
            graphLst->adjList[*ind] = NULL;
        }

        // Procedo ad eliminare eventuali archi entranti
        DataObject* oldName = adtConstruct(graphLst->intType);
        adtSetValue(oldName, &vertexName);

        for (uint i = 0; i < graphLst->arraySize; ++i) {
            // Verifico se c'è una lista per questo vertice
            if(graphLst->adjList[i]) {
                // Se la rimozione ha successo, allora incremento gli archi tolti
                if(lstRemove(graphLst->adjList[i], oldName)) {
                    graphLst->numberOfEdges--;
                    (*numberOfRemovedEdges)++;

                    // Se la lista è vuota allora devo distruggerla
                    if(lstEmpty(graphLst->adjList[i])) {
                        lstDestruct(graphLst->adjList[i]);
                        graphLst->adjList[i] = NULL;
                    }
                }
            }
        }

        // Aggiungo il nuovo indice alla coda
        queEnqueue(graphLst->queueOfIds, oldName);
        graphLst->numberOfVertices--;

        adtDestruct(index);
        adtDestruct(oldName);
        free(ind);

        // Se utilizzo 1/4 dell'array devo dimezzarlo
        if(graphLst->numberOfVertices * 4 == graphLst->arraySize) {
            reduceAdjList(graphLst);
        }
    }

    adtDestruct(key);
    return ret;
}

/** Inserisce un arco nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco è stato inserito con successo, false altrimenti
  */
bool GraphLstInsertEdge(void* graph, int vertexName1, int vertexName2) {
    GraphLst* graphLst = (GraphLst*) graph;
    bool ret = false;

    // Verifico se vertexId esista nell'albero tramite la funzione di supporto
    uint* vertexId = getIndexFromNameLst(graphLst, vertexName1);
    if(!vertexId) {
        return false;
    }

    // Verifico se vertex2 esiste senza utility perché mi serve il DataObject
    DataObject* key = adtConstruct(graphLst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName2;
    DataObject* vertex2 = bstGetValue(graphLst->treeOfNames, key);

    if(vertex2) {
        // Dichiaro un DataObject con un intero per andare a cercare nel bst
        DataObject* newElem = adtConstruct(graphLst->intType);
        adtSetValue(newElem, &vertexName2);

        // Se la lista è vuota, allora la creo ed inserisco l'intero
        if(graphLst->adjList[*vertexId] == NULL) {
            graphLst->adjList[*vertexId] = lstConstruct();
            lstInsert(graphLst->adjList[*vertexId], newElem);

            graphLst->numberOfEdges++;
            ret = true;
        }
        else {
            // Se l'arco non esiste, allora lo aggiungo
            if(!lstExists(graphLst->adjList[*vertexId], newElem)) {
                lstInsert(graphLst->adjList[*vertexId], newElem);

                graphLst->numberOfEdges++;
                ret = true;
            }
        }

        adtDestruct(newElem);
        adtDestruct(vertex2);
    }

    adtDestruct(key);
    free(vertexId);
    return ret;
}

/** Rimuove un arco nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco è stato rimosso con successo, false altrimenti
  */
bool GraphLstRemoveEdge(void* graph, int vertexName1, int vertexName2) {
    GraphLst* graphLst = (GraphLst*) graph;
    bool ret = false;

    // Se il numero di archi è 0, banalmente ritorno false
    if(graphLst->numberOfEdges == 0) {
        return false;
    }

    // Verifico se vertexId esista nell'albero tramite la funzione di supporto
    uint* vertexId = getIndexFromNameLst(graphLst, vertexName1);
    if(!vertexId) {
        return false;
    }

    // Verifico se vertex2 esiste senza utility perché mi serve il DataObject
    DataObject* key = adtConstruct(graphLst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName2;
    DataObject* vertex2 = bstGetValue(graphLst->treeOfNames, key);

    if(vertex2) {
        // Se la lista non è null e l'elemento esiste allora procedo a rimuoverlo
        if(graphLst->adjList[*vertexId] && lstExists(graphLst->adjList[*vertexId], vertex2)) {
            lstRemove(graphLst->adjList[*vertexId], vertex2);

            // Se la lista è vuota la distruggo e setto il puntatore a NULL
            if(lstEmpty(graphLst->adjList[*vertexId])) {
                lstDestruct(graphLst->adjList[*vertexId]);
                graphLst->adjList[*vertexId] = NULL;
            }

            ret = true;
        }
        // Se arrivo qui sono sicuro che vertex != NULL e posso liberarlo
        adtDestruct(vertex2);
    }

    adtDestruct(key);
    free(vertexId);
    return ret;
}

/** Verifica se un vertice esiste nel grafo
  * @param graph il grafo
  * @param vertexName il nome del vertice da cercare
  * @return true se il vertice con tale nome esiste esiste, false altrimenti
  */
bool GraphLstExistsVertex(void* graph, int vertexName) {
    GraphLst* graphLst = (GraphLst*) graph;

    // Se non ho vertici, allora non esiste
    if(graphLst->numberOfVertices == 0) {
        return false;
    }

    // Richiamo la funzione di supporto, ritorna NULL se non lo trova
    uint* vertexId = getIndexFromNameLst(graphLst, vertexName);

    if(vertexId) {
        free(vertexId);
        return true;
    }
    return false;
}

/** Verifica se un arco esiste nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco esiste, false altrimenti
  */
bool GraphLstExistsEdge(void* graph, int vertexName1, int vertexName2) {
    GraphLst* graphLst = (GraphLst*) graph;
    bool ret = false;

    // Verifico se vertexId esista nell'albero tramite la funzione di supporto
    uint* vertexId = getIndexFromNameLst(graphLst, vertexName1);
    if(!vertexId) {
        return false;
    }

    // Verifico se vertex2 esiste senza utility perché mi serve il DataObject
    DataObject* key = adtConstruct(graphLst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName2;
    DataObject* vertex2 = bstGetValue(graphLst->treeOfNames, key);

    // Se vertex e la lista non sono null e l'elemento esiste, ritorno true
    if(vertex2) {
        if(graphLst->adjList[*vertexId]) {
            DataObject* newElem = adtConstruct(graphLst->intType);
            adtSetValue(newElem, &vertexName2);

            if(lstExists(graphLst->adjList[*vertexId], newElem)) {
                ret = true;
            }

            adtDestruct(newElem);
        }
        // Se sono qui vertex != NULL e posso liberarlo
        adtDestruct(vertex2);
    }

    adtDestruct(key);
    free(vertexId);
    return ret;
}

/** Ritorna una copia del DataObject collegato a quel nome
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @return DataObject la copia del DataObject se esiste, NULL altrimenti
  */
DataObject* GraphLstGetVertexData(void* graph, int vertexName) {
    GraphLst* graphLst = (GraphLst*) graph;

    // Se il grafo non ha vertici, banalmente ritorno NULL
    if(graphLst->numberOfVertices == 0) {
        return NULL;
    }

    // Verifico se esiste in corrispondenza nell'albero
    uint* vertexId = getIndexFromNameLst(graphLst, vertexName);
    if(!vertexId) {
        return NULL;
    }

    // Se arrivo qui allora esiste e posso ritornarne una copia
    DataObject* copy = adtClone(graphLst->vertices[*vertexId]->dataObject);

    free(vertexId);
    return copy;
}

/** Sovrascrive il DataObject collegato a quel nome
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @param newData Il nuovo dato che si intende inserire
  */
void GraphLstSetVertexData(void* graph, int vertexName, DataObject* newData) {
    GraphLst* graphLst = (GraphLst*) graph;

    // Il grafo deve avere almeno un vertice per fare la set
    if(graphLst->numberOfVertices > 0) {
        // Verifico se esiste la sua corrispondenza nell'albero
        uint *vertexId = getIndexFromNameLst(graphLst, vertexName);

        if (vertexId) {
            // Se arrivo qui allora esiste, elimino il precedente ed assegno una copia
            adtDestruct(graphLst->vertices[*vertexId]->dataObject);
            graphLst->vertices[*vertexId]->dataObject = adtClone(newData);
            free(vertexId);
        }
    }
}

/** Ritorna un vertice partendo da un puntatore
  * @param graph il grafo che contiene quel vertice
  * @param vertexId la posizione del vertice nell'array del grafo
  * @return GraphVertex il vertice del grafo
  */
GraphVertex* GraphLstVertexFromPointer(void* graph, void* vertexId) {
    GraphLst* graphLst = (GraphLst*) graph;
    uint index = *((uint*) vertexId);

    assert(index < graphLst->arraySize);

    return graphLst->vertices[index];
}

/** Ritorna un iteratore su tutti i vertici del grafo
  * @param grafo su cui si intende iterare
  * @return ITRObject che permette l'iterazione
  */
void* GraphLstVertices(void* graph){
    ITRType* graphLstType = ConstructGraphLstVertType();
    ITRObject* iterator = itrConstruct(graphLstType, graph);

    return iterator;
}

/** Ritorna un iteratore sugli adiacenti di un dato vertice
  * @param grafo su cui si intende iterare
  * @param vertexName il nome del vertice su cui cercare gli adiacenti
  * @return ITRObject che permette l'iterazione
  */
void* GraphLstVertexEdges(void* graph, int vertexName) {
    // Creo un graphLstSupport
    GraphLstSupport* support = (GraphLstSupport*) malloc(sizeof(GraphLstSupport));
    GraphLst* graphLst = (GraphLst*) graph;
    support->graph = graph;

    // Converto il nome in indice per l'array se esiste
    uint* tmp = getIndexFromNameLst(graph, vertexName);

    if(!tmp) {
        support->vertexAdj = NULL;
    }
    else {
        // Verifico se esistono adiacenti a quell'elemento
        if(graphLst->adjList[*tmp]) {
            support->vertexAdj = graphLst->adjList[*tmp]->head;
        }
        else {
            /* Non esiste una lista per quel vertice, imposto NULL
             * così tornerà subito iteratore terminato */
            support->vertexAdj = NULL;
        }
        free(tmp);
    }

    ITRType* graphMatEdges = ConstructGraphLstEdgesType();
    ITRObject* iterator = itrConstruct(graphMatEdges, support);

    free(support);
    return iterator;
}

/** Ridimensiona correttamente l'array e la lista di adiacenza
  * @param graph il grafo
  */
void reduceAdjList(GraphLst* graphLst) {
    // Creo una nuova lista ed un nuovo array più piccoli
    GraphVertex** newArray = (GraphVertex**) malloc(((graphLst->arraySize) / 2) * sizeof(GraphVertex*));
    ListObject** newAdjList = (ListObject**) malloc(((graphLst->arraySize) / 2) * sizeof(ListObject*));

    // Scorro l'array dei vertici per scoprire quali locazioni devo prendere
    uint j = 0;
    for(uint i = 0; i < graphLst->arraySize; ++i) {

        // Ho trovato un vertice candidato in posizione i
        if(graphLst->vertices[i]) {

            // Salvo il riferimento a questo valore
            newArray[j] = graphLst->vertices[i];

            // Sposto anche la sua lista di adiacenza
            newAdjList[j] = graphLst->adjList[i];

            // Refresho l'indice nell'albero
            DataObject* tmp = adtConstruct(graphLst->vertexType);
            ((ADTVertexMap*) tmp->value)->vertexName = graphLst->vertices[i]->name;

            DataObject* oldVertex = bstGetValue(graphLst->treeOfNames, tmp);
            ((ADTVertexMap*) oldVertex->value)->identifier = j;
            bstRemove(graphLst->treeOfNames, tmp);
            bstInsert(graphLst->treeOfNames, oldVertex);

            adtDestruct(oldVertex);
            adtDestruct(tmp);

            // Avanzo l'indice
            j++;
        }
    }

    /* Imposto gli indici non usati dell'array e della lista a NULL per non avere
     * letture di valori non inizializzati */
    for (int k = j; k < graphLst->arraySize / 2; ++k) {
        newAdjList[k] = NULL;
        newArray[k] = NULL;
    }

    // Svuoto la queue degli indici, potrebbe essere sporca e puntare ad elementi fuori dall'array
    queClear(graphLst->queueOfIds);

    // Il nuovo actualIndex deve partire da j
    graphLst->actualIndex = j;

    // Libero la memoria del vecchio array e adjList
    free(graphLst->vertices);
    free(graphLst->adjList);

    // Sovrascrivo i vecchi valori e dimezzo la grandezza
    graphLst->vertices = newArray;
    graphLst->adjList = newAdjList;
    graphLst->arraySize /= 2;
}

/** Converte una nome in un indice per la matrice e l'array
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @return uint indice se esiste, NULL altrimenti
  */
uint* getIndexFromNameLst(GraphLst *graph, int vertexName) {
    GraphLst* graphMat = (GraphLst*) graph;
    uint* ret = NULL;

    // Dichiaro un DataObject di supporto per verificare l'esistenza del nome
    DataObject* key = adtConstruct(graphMat->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    DataObject* index = bstGetValue(graphMat->treeOfNames, key);

    // Se mi è tornato un valore allora esiste corrispondenza
    if(index) {
        ret = adtGetValue(index);
    }

    adtDestruct(key);
    adtDestruct(index);
    return ret;
}