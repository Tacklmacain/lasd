#include "graphlstverticesitr.h"

/* ************************************************************************** */

ITRType* ConstructGraphLstVertType() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &graphLstVertConstruct;
    type->destruct = &graphLstVertDestruct;
    type->element = &graphLstVertElement;
    type->successor = &graphLstVertSuccessor;
    type->isTerminated = &graphLstVertTerminated;

    return type;
}

void DestructGraphLstVertType(ITRType* type) {
    free(type);
}

void* graphLstVertConstruct(void* graph) {
    GraphLstVerticesItr* iter = (GraphLstVerticesItr*) malloc(sizeof(GraphLstVerticesItr));
    StackType* type = ConstructStackVecType();

    iter->graph = graph;
    iter->ptrType = ConstructPtrDataType();
    iter->stack = stkConstruct(type);
    iter->node = iter->graph->treeOfNames->root;

    // Controllo il nodo in quanto la radice potrebbe essere NULL se l'albero è vuoto
    if(iter->node) {
        DataObject* ptr = adtConstruct(iter->ptrType);

        // Mi sposto a sinistra e salvo ogni elemento nel percorso
        while(iter->node->left) {
            adtSetValue(ptr, iter->node);
            stkPush(iter->stack, ptr);
            iter->node = iter->node->left;
        }
        adtDestruct(ptr);
    }

    return iter;
}

void graphLstVertDestruct(void* iterator) {
    GraphLstVerticesItr* iter = (GraphLstVerticesItr*) iterator;
    StackType* type = iter->stack->type;

    stkClear(iter->stack);
    stkDestruct(iter->stack);
    DestructStackVecType(type);
    free(iter->ptrType);
    free(iter);
}

bool graphLstVertTerminated(void* iterator) {
    GraphLstVerticesItr* iter = (GraphLstVerticesItr*) iterator;

    // Ritorno true solo se non ho un corrente e lo stack è vuoto
    return !iter->node && stkEmpty(iter->stack);
}

void* graphLstVertElement(void* iterator) {
    GraphLstVerticesItr* iter = (GraphLstVerticesItr*) iterator;

    // Prendo l'ADT di mapping dall'albero
    ADTVertexMap* vertex = adtGetValue(iter->node->value);
    iter->current = vertex->identifier;
    free(vertex);

    // Mi ritorno la sua posizione nell'array
    return &iter->current;
}

void graphLstVertSuccessor(void* iterator) {
    GraphLstVerticesItr* iter = (GraphLstVerticesItr*) iterator;
    DataObject* ptr = adtConstruct(iter->ptrType);

    // Se ho un nodo destro devo scendere e cercare il minimo del suo sottoalbero
    iter->node = iter->node->right;

    if(iter->node) {
        while(iter->node->left) {
            adtSetValue(ptr, iter->node);
            stkPush(iter->stack, ptr);
            iter->node = iter->node->left;
        }
    }
        // Se non ho figlio destro, allora verifico se devo salire
    else if (!stkEmpty(iter->stack)) {
        DataObject* tmp = stkTopNPop(iter->stack);
        iter->node = (BSTNode*)adtGetValue(tmp);
        adtDestruct(tmp);
    }

    adtDestruct(ptr);
}
