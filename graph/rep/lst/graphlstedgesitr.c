#include "graphlst.h"
#include "graphlstedgesitr.h"

/* ************************************************************************** */

ITRType* ConstructGraphLstEdgesType() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &graphLstEdgesConstruct;
    type->destruct = &graphLstEdgesDestruct;
    type->element = &graphLstEdgesElement;
    type->successor = &graphLstEdgesSuccessor;
    type->isTerminated = &graphLstEdgesTerminated;

    return type;
}

void DestructGraphLstEdgesType(ITRType* type) {
    free(type);
}

void* graphLstEdgesConstruct(void* graphSupport) {
    GraphLstEdgesItr* iter = (GraphLstEdgesItr*) malloc(sizeof(GraphLstEdgesItr));
    GraphLstSupport* graphLstSupport = (GraphLstSupport*) graphSupport;

    iter->graph = graphLstSupport->graph;
    iter->current = graphLstSupport->vertexAdj;
    iter->currentIndex = 0; // Lo inizializzo anche se al primo element viene aggiornato

    return iter;
}

void graphLstEdgesDestruct(void* iterator) {
    free(iterator);
}

bool graphLstEdgesTerminated(void* iterator) {
    // Se l'adiacente punta a NULL, l'iteratore è finito
    return ((GraphLstEdgesItr*) iterator)->current == NULL;
}

void* graphLstEdgesElement(void* iterator) {
    GraphLstEdgesItr* iter = (GraphLstEdgesItr*) iterator;

    // Per poter tornare il vertex devo convertire il nome in indice
    int* name = adtGetValue(iter->current->data);
    uint* index = getIndexFromNameLst(iter->graph, *name);

    iter->currentIndex = *index;
    free(name);
    free(index);

    return &iter->currentIndex;
}

void graphLstEdgesSuccessor(void* iterator) {
    GraphLstEdgesItr* iter = (GraphLstEdgesItr*) iterator;
    iter->current = iter->current->next;
}