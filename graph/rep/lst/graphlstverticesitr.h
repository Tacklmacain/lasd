#ifndef EXERCISE3_GRAPHLSTVERTICESITR_H
#define EXERCISE3_GRAPHLSTVERTICESITR_H

/* ************************************************************************** */

#include "../../../stack/vec/stackvec.h"
#include "../../../stack/stack.h"
#include "../../../itr/itr.h"
#include "graphlst.h"

/* ************************************************************************** */

/** Iteratore su tutti i vertici del grafo
  * @attribute graph
  * Il grafo relativo all'iterazione, permette di accedere ai suoi elementi
  *
  * @attribute stack
  * Memorizza i vari elementi siccome itero sull'albero
  *
  * @attribute node
  * Indica l'elemento corrente dell'albero
  *
  * @attribute current
  * Serve per restituire il valore di output
  *
  * @attribute ptrType
  * DataType relativo agli elementi dello stack, evita memory leak
  */
typedef struct GraphLstVerticesItr {
    GraphLst* graph;
    StackObject* stack;
    BSTNode* node;
    uint current;

    DataType* ptrType;
} GraphLstVerticesItr;

/* ************************************************************************** */

ITRType* ConstructGraphLstVertType();
void DestructGraphLstVertType(ITRType*);

void* graphLstVertConstruct(void*);
void graphLstVertDestruct(void*);
bool graphLstVertTerminated(void*);
void* graphLstVertElement(void*);
void graphLstVertSuccessor(void*);

/* ************************************************************************** */

#endif