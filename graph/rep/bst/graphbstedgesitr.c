#include "graphbstedgesitr.h"

/* ************************************************************************** */

ITRType* ConstructGraphBstEdgesType() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &graphBstEdgesConstruct;
    type->destruct = &graphBstEdgesDestruct;
    type->element = &graphBstEdgesElement;
    type->successor = &graphBstEdgesSuccessor;
    type->isTerminated = &graphBstEdgesTerminated;

    return type;
}

void DestructGraphBstEdgesType(ITRType* type) {
    free(type);
}

void* graphBstEdgesConstruct(void* graphSupport) {
    GraphBstEdgesItr* iter = (GraphBstEdgesItr*) malloc(sizeof(GraphBstEdgesItr));
    GraphBstSupport* graphLstSupport = (GraphBstSupport*) graphSupport;
    StackType* type = ConstructStackVecType();
    BSTNode* node = graphLstSupport->root;

    iter->graph = graphLstSupport->graph;
    iter->ptrType = ConstructPtrDataType();
    iter->stack = stkConstruct(type);

    // Mi sposto a sinistra e salvo ogni elemento nel percorso
    if(node) {
        DataObject* ptr = adtConstruct(iter->ptrType);
        while(node->left) {
            adtSetValue(ptr, node);
            stkPush(iter->stack, ptr);
            node = node->left;
        }
        adtDestruct(ptr);
    }

    /* Assegno current come node in quanto come caso base node = root mentre
     * se sono sceso nel percorso node = min */
    iter->current = node;

    return iter;

}

void graphBstEdgesDestruct(void* iterator) {
    GraphBstEdgesItr* iter = (GraphBstEdgesItr*) iterator;
    StackType* type = iter->stack->type;

    DestructPtrDataType(iter->ptrType);
    stkClear(iter->stack);
    stkDestruct(iter->stack);
    DestructStackVecType(type);
    free(iter);
}

bool graphBstEdgesTerminated(void* iterator) {
    GraphBstEdgesItr* iter = (GraphBstEdgesItr*) iterator;

    // Ritorno true solo se non ho un corrente e lo stack è vuoto
    return !iter->current && stkEmpty(iter->stack);
}

void* graphBstEdgesElement(void* iterator) {
    GraphBstEdgesItr* iter = (GraphBstEdgesItr*) iterator;

    // Per poter tornare il vertex devo convertire il nome in indice
    int* name = adtGetValue(iter->current->value);
    uint* index = getIndexFromNameBst(iter->graph, *name);

    iter->currentIndex = *index;
    free(name);
    free(index);

    return &iter->currentIndex;
}

void graphBstEdgesSuccessor(void* iterator) {
    GraphBstEdgesItr* iter = (GraphBstEdgesItr*) iterator;

    // Se ho un nodo destro devo scendere e cercare il minimo del suo sottoalbero
    iter->current = iter->current->right;

    if(iter->current) {
        while(iter->current->left) {
            DataObject* ptr = adtConstruct(iter->ptrType);
            adtSetValue(ptr, iter->current);
            stkPush(iter->stack, ptr);
            iter->current = iter->current->left;
            adtDestruct(ptr);
        }
    }
    // Se non ho figlio destro, allora devo salire
    else if(!stkEmpty(iter->stack)) {
        DataObject* tmp = stkTopNPop(iter->stack);
        iter->current = adtGetValue(tmp);
        adtDestruct(tmp);
    }
}