
#ifndef GRAPHBST_H
#define GRAPHBST_H

/* ************************************************************************** */

#include "../../graph.h"
#include "../../../bst/bst.h"

/* ************************************************************************** */

/** Struttura del grafo rappresentato con alberi di adiacenza
  * @attribute adjTree
  * L'albero di adiacenza
  *
  * @attribute treeOfNames
  * Albero Binario che si occupa di fare corrispondenza fra nome del vertice ed il suo indice
  * nell'array, utilizza l'adt "vertex"
  *
  * @attribute vertices
  * Array di vertici, messo in corrispondenza con treeOfNames
  *
  * @attribute queueOfIds
  * Queue di interi che gestisce gli indici da riciclare, se un vertice viene eliminato il suo
  * identificativo finisce in questa coda e può essere utilizzato al prossimo inserimento
  *
  * @attribute numberOfVertices, numberOfEdges
  * Rappresentano il numero di vertici ed archi, velocizzano alcune funzioni
  *
  * @attribute arraySize
  * Indica la grandezza dell'array dell'albero di adiacenza, viene utilizzato per reallocarlo se necessario
  *
  * @attribute actualIndex
  * Indica l'indice massimo attualmente utilizzato nell'array, un grafo di n elementi avrà
  * come valore massimo di actualIndex n-1, si incrementa se l'array è pieno all'inserimento
  *
  * @attribute bstType
  * Contiene il BSTType relativo ai singoli alberi di adiacenza, evita memory leak
  *
  * @attribute vertexType
  * Contiene il DataType relativo ai vertici che hanno identificatore e nome, evita memory leak
  *
  * @attribute intType
  * Contiene il DataType di interi per la queue, evita memory leak
  */
typedef struct GraphBst {
    BSTObject** adjTree;
    BSTObject* treeOfNames;
    GraphVertex** vertices;
    QueueObject* queueOfIds;

    uint numberOfVertices, numberOfEdges;
    uint arraySize;
    uint actualIndex;

    BSTType* bstType;
    DataType *vertexType, *intType;
} GraphBst;

/** Struttura di supporto per poter construire un iteratore sugli adiacenti
  * @attribute graph
  * Il grafo ad alberi di adiacenza su cui si intende iterare
  *
  * @attribute vertexAdj
  * Il primo elemento dell'albero di adiacenza di quel nodo
  */
typedef struct GraphBstSupport {
    GraphBst* graph;
    BSTNode* root;
} GraphBstSupport;

/* ************************************************************************** */

GraphRepresentation* ConstructGraphBst();
void DestructGraphBst(GraphRepresentation*);

void* GraphBstConstruct();
void GraphBstDestruct(void*);

bool GraphBstEmpty(void*);

void* GraphBstClone(void*);
void* GraphBstTranspose(void*);

bool GraphBstInsertVertex(void*, DataObject*, int);
bool GraphBstRemoveVertex(void*, int, int*);

bool GraphBstInsertEdge(void*, int, int);
bool GraphBstRemoveEdge(void*, int, int);

bool GraphBstExistsVertex(void*, int);
bool GraphBstExistsEdge(void*, int, int);

DataObject* GraphBstGetVertexData(void*, int);
void GraphBstSetVertexData(void*, int, DataObject*);

GraphVertex* GraphBstVertexFromPointer(void*, void*);

void* GraphBstVertices(void*);
void* GraphBstVertexEdges(void*, int);

/* ************************************************************************** */
// Funzioni di supporto
void reduceAdjTree(GraphBst*);
uint* getIndexFromNameBst(GraphBst*, int);

#endif