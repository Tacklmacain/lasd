#ifndef EXERCISE3_GRAPHBSTVERTICESITR_H
#define EXERCISE3_GRAPHBSTVERTICESITR_H

/* ************************************************************************** */

#include "../../../stack/vec/stackvec.h"
#include "../../../stack/stack.h"
#include "../../../itr/itr.h"
#include "graphbst.h"

/* ************************************************************************** */

/** Iteratore su tutti i vertici del grafo
  * @attribute graph
  * Il grafo relativo all'iterazione, permette di accedere ai suoi elementi
  *
  * @attribute stack
  * Memorizza i vari elementi siccome itero sull'albero
  *
  * @attribute node
  * Indica l'elemento corrente dell'albero
  *
  * @attribute current
  * Serve per restituire il valore di output
  *
  * @attribute ptrType
  * DataType relativo agli elementi dello stack, evita memory leak
  */
typedef struct GraphBstVerticesItr {
    GraphBst* graph;
    StackObject* stack;
    BSTNode* node;
    uint current;

    DataType* ptrType;
} GraphBstVerticesItr;

/* ************************************************************************** */

ITRType* ConstructGraphBstVertType();
void DestructGraphBstVertType(ITRType*);

void* graphBstVertConstruct(void*);
void graphBstVertDestruct(void*);
bool graphBstVertTerminated(void*);
void* graphBstVertElement(void*);
void graphBstVertSuccessor(void*);

/* ************************************************************************** */


#endif