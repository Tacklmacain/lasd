#ifndef EXERCISE3_GRAPHBSTEDGESITR_H
#define EXERCISE3_GRAPHBSTEDGESITR_H

/* ************************************************************************** */

#include "../../../stack/vec/stackvec.h"
#include "../../../stack/stack.h"
#include "../../../itr/itr.h"
#include "graphbst.h"

/* ************************************************************************** */


/** Iteratore su tutti gli adiacendi di un vertice di grafo con alberi di adiacenza
  * @attribute graph
  * Il graphBst relativo all'iterazione, permette di accedere ai suoi elementi
  *
  * @attribute current
  * Indica il nodo attuale
  *
  * @attribute stack
  * Stack utilizzato per la risalita
  *
  * @attribute ptrType
  * DataType necessario per gli elementi dello stack, evita memory leak
  *
  * @attribute currentIndex
  * Indica l'indice nell'array del nodo attuale
  */
typedef struct GraphBstEdgesItr {
    GraphBst* graph;
    BSTNode* current;
    StackObject* stack;
    uint currentIndex;

    DataType* ptrType;
} GraphBstEdgesItr;

/* ************************************************************************** */

ITRType* ConstructGraphBstEdgesType();
void DestructGraphBstEdgesType(ITRType*);

void* graphBstEdgesConstruct(void*);
void graphBstEdgesDestruct(void*);
bool graphBstEdgesTerminated(void*);
void* graphBstEdgesElement(void*);
void graphBstEdgesSuccessor(void*);

/* ************************************************************************** */





#endif 