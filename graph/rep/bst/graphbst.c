#include "graphbst.h"
#include "graphbstverticesitr.h"
#include "graphbstedgesitr.h"
#include "../../../bst/bstitrinord.h"

/* ************************************************************************** */

GraphRepresentation* ConstructGraphBst() {
    GraphRepresentation* representation = (GraphRepresentation*) malloc(sizeof(GraphRepresentation));

    representation->construct = &GraphBstConstruct;
    representation->destruct = &GraphBstDestruct;
    representation->isEmpty = &GraphBstEmpty;
    representation->clone = &GraphBstClone;
    representation->transpose = &GraphBstTranspose;
    representation->insertVertex = &GraphBstInsertVertex;
    representation->removeVertex = &GraphBstRemoveVertex;
    representation->insertEdge = &GraphBstInsertEdge;
    representation->removeEdge = &GraphBstRemoveEdge;
    representation->existsVertex = &GraphBstExistsVertex;
    representation->existsEdge = &GraphBstExistsEdge;
    representation->getVertexData = &GraphBstGetVertexData;
    representation->setVertexData = &GraphBstSetVertexData;
    representation->vertexFromPointer = &GraphBstVertexFromPointer;
    representation->vertices = &GraphBstVertices;
    representation->vertexEdges = &GraphBstVertexEdges;

    return representation;
}

void DestructGraphBst(GraphRepresentation* repr) {
    free(repr);
}

void* GraphBstConstruct() {
    GraphBst* newGraph = (GraphBst*) malloc(sizeof(GraphBst));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    newGraph->actualIndex = 0;
    newGraph->numberOfVertices = 0;
    newGraph->numberOfEdges = 0;
    newGraph->arraySize = 4;

    newGraph->vertexType = ConstructGraphMapDataType();
    newGraph->intType = ConstructIntDataType();
    newGraph->bstType = bstType;

    newGraph->vertices = (GraphVertex**) malloc(newGraph->arraySize * sizeof(GraphVertex*));
    newGraph->adjTree = (BSTObject**) malloc(newGraph->arraySize * sizeof(BSTObject*));
    newGraph->treeOfNames = bstConstruct(bstType);
    newGraph->queueOfIds = queConstruct(queueType);

    // Imposto i vari indici dell0albero di adiacenza ed array dei vertici a NULL
    for(uint i = 0; i < newGraph->arraySize; ++i) {
        newGraph->adjTree[i] = NULL;
        newGraph->vertices[i] = NULL;
    }

    return newGraph;
}

void GraphBstDestruct(void* graph) {
    GraphBst* graphToDestroy = (GraphBst*) graph;

    // Dealloco i vari alberi di adiacenza
    for (uint i = 0; i < graphToDestroy->arraySize; ++i) {
        if(graphToDestroy->adjTree[i]) {
            bstDestruct(graphToDestroy->adjTree[i]);
        }
    }
    free(graphToDestroy->adjTree);

    // Salvo il type, dealloco la queue e dealloco il type
    QueueType* queTypeToDestroy = graphToDestroy->queueOfIds->type;
    queClear(graphToDestroy->queueOfIds);
    queDestruct(graphToDestroy->queueOfIds);
    DestructQueueVecType(queTypeToDestroy);

    bstDestruct(graphToDestroy->treeOfNames);

    /* Dealloco i vari vertici e libero man mano */
    for (uint i = 0; i < graphToDestroy->arraySize; ++i) {
        if(graphToDestroy->vertices[i]) {
            adtDestruct(graphToDestroy->vertices[i]->dataObject);
            free(graphToDestroy->vertices[i]);
        }
    }
    free(graphToDestroy->vertices);

    DestructGraphMapDataType(graphToDestroy->vertexType);
    DestructIntDataType(graphToDestroy->intType);
    DestructBSTIterative(graphToDestroy->bstType);
    free(graphToDestroy);
}

bool GraphBstEmpty(void* graph) {
    return ((GraphBst*) graph)->numberOfVertices == 0;
}

/** Clona il grafo sia in struttura che in elementi
  * @param graph il grafo che si intende duplicare
  * @return GraphBst clone del grafo in ingresso
  */
void* GraphBstClone(void* graph) {
    GraphBst* original = (GraphBst*) graph;

    /* Alloco tutto ciò che mi serve per il clone, devo allocare un nuovo queueType
     * e bstType per non rischiare di perderli cancellando il grafo originale */
    GraphBst* clone = (GraphBst*) malloc(sizeof(GraphBst));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    clone->actualIndex = original->actualIndex;
    clone->numberOfVertices = original->numberOfVertices;
    clone->numberOfEdges = original->numberOfEdges;
    clone->arraySize = original->arraySize;

    clone->vertexType = ConstructGraphMapDataType();
    clone->intType = ConstructIntDataType();
    clone->bstType = bstType;

    clone->queueOfIds = queClone(original->queueOfIds);
    clone->queueOfIds->type = queueType;
    clone->treeOfNames = bstClone(original->treeOfNames);
    clone->treeOfNames->type = bstType;
    clone->adjTree = (BSTObject**) malloc(original->arraySize * sizeof(BSTObject*));
    clone->vertices = (GraphVertex**) malloc(original->arraySize * sizeof(GraphVertex*));

    /* Assegno il type corretto in quanto con la clone il bst e la queue sono
     * pieni di ADT che puntano al type della struttura precedente */
    bstBreadthMap(clone->treeOfNames, &changeType, clone->vertexType);
    queMap(clone->queueOfIds, &changeType, clone->intType);

    // Ciclo la lista del grafo originale e duplico man mano vertici e alberi
    for (uint i = 0; i < original->arraySize; ++i) {

        /* Se esiste il vertice lo duplico altrimenti assegno NULL a lui
         * ed al suo albero di adiacenza */
        if(original->vertices[i]) {
            GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
            newVertex->name = original->vertices[i]->name;
            newVertex->dataObject = adtClone(original->vertices[i]->dataObject);
            clone->vertices[i] = newVertex;

            /* Se questo vertice ha un albero di adiacenza lo clono,
             * altrimenti assegno NULL */
            if(original->adjTree[i]) {
                clone->adjTree[i] = bstClone(original->adjTree[i]);

                // Assegno il tipo corretto agli elementi dell'albero
                bstBreadthMap(clone->adjTree[i], &changeType, clone->intType);

                // Assegno il tipo corretto all'albero
                clone->adjTree[i]->type = clone->bstType;
            }
            else {
                clone->adjTree[i] = NULL;
            }
        }
        else {
            clone->vertices[i] = NULL;
            clone->adjTree[i] = NULL;
        }
    }

    return clone;
}

/** Fa il trasposto di un grafo
  * @param graph il grafo che si intende trasporre
  * @return traspose il traposto del grafo in ingresso
  */
void* GraphBstTranspose(void* graph) {
    GraphBst* original = (GraphBst*) graph;

    /* Alloco tutto ciò che mi serve per il transpose, devo allocare un nuovo queueType
     * e bstType per non rischiare di perderli cancellando il grafo originale */
    GraphBst* transpose = (GraphBst*) malloc(sizeof(GraphBst));
    QueueType* queueType = ConstructQueueVecType();
    BSTType* bstType = ConstructBSTIterative();

    transpose->actualIndex = original->actualIndex;
    transpose->numberOfVertices = original->numberOfVertices;
    transpose->numberOfEdges = original->numberOfEdges;
    transpose->arraySize = original->arraySize;

    transpose->vertexType = ConstructGraphMapDataType();
    transpose->intType = ConstructIntDataType();
    transpose->bstType = bstType;

    transpose->queueOfIds = queClone(original->queueOfIds);
    transpose->queueOfIds->type = queueType;
    transpose->treeOfNames = bstClone(original->treeOfNames);
    transpose->treeOfNames->type = bstType;
    transpose->adjTree = (BSTObject**) malloc(original->arraySize * sizeof(BSTObject*));
    transpose->vertices = (GraphVertex**) malloc(original->arraySize * sizeof(GraphVertex*));

    /* Assegno il type corretto in quanto con la clone il bst e la queue sono
     * pieni di ADT che puntano al type della struttura precedente */
    bstBreadthMap(transpose->treeOfNames, &changeType, transpose->vertexType);
    queMap(transpose->queueOfIds, &changeType, transpose->intType);

    // Inizializzo la lista di adiacenza di transpose a NULL
    for (int i = 0; i < transpose->arraySize; ++i) {
        transpose->adjTree[i] = NULL;
    }

    // Ciclo la lista del grafo originale e traspongo man mano i vertici
    DataObject* vertexName = adtConstruct(original->intType);
    for (uint i = 0; i < original->arraySize; ++i) {
        /* Se esiste il vertice lo duplico altrimenti assegno NULL a lui
         * ed alla sua lista di adiacenza */
        if (original->vertices[i]) {
            GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
            newVertex->name = original->vertices[i]->name;
            newVertex->dataObject = adtClone(original->vertices[i]->dataObject);
            transpose->vertices[i] = newVertex;

            // Se questo vertice ha una lista di adiacenza la traspongo
            if (original->adjTree[i]) {
                /* Memorizzo il nome del vertice corrente ed inizio a scorrere
                 * sfruttando un iteratore dei BST */
                adtSetValue(vertexName, &transpose->vertices[i]->name);

                ITRType* type = ConstructBSTInOrderIterator();
                ITRObject* iterator = itrConstruct(type, original->adjTree[i]->root);
                BSTNode* node;

                while(!itrTerminated(iterator)) {
                    node = (BSTNode*) itrElement(iterator);

                    int* name = (int*) adtGetValue(node->value);
                    uint* index = getIndexFromNameBst(original, *name);

                    // Se l'albero di adiacenti di questo nodo non esiste, la creo
                    if (!transpose->adjTree[*index]) {
                        transpose->adjTree[*index] = bstConstruct(transpose->bstType);
                    }

                    // Inserisco l'elemento
                    bstInsert(transpose->adjTree[*index], vertexName);
                    itrSuccessor(iterator);

                    free(name);
                    free(index);
                }
                itrDestruct(iterator);
                DestructBSTInOrderIterator(type);
            }
        }
        else {
            transpose->vertices[i] = NULL;
        }
    }
    adtDestruct(vertexName);

    /* Assegno il type corretto in quanto con la bstInsert l'albero è
     * pieno di ADT che puntano al type della struttura precedente */
    for (int i = 0; i < transpose->arraySize; ++i) {
        if(transpose->adjTree[i]) {
            bstBreadthMap(transpose->adjTree[i], &changeType, transpose->intType);
        }
    }

    return transpose;
}

/** Inserisce un vertice nel grafo
  * @param graph il grafo
  * @param data il DataObject che si intende inserire
  * @param vertexName il nome che si intende assegnare al vertice
  * @return true se l'elemento è stato inserito con successo, false altrimenti
  */
bool GraphBstInsertVertex(void* graph, DataObject* data, int vertexName) {
    GraphBst* graphBst = (GraphBst*) graph;

    DataObject* key = adtConstruct(graphBst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    /* Verifico se la chiave già esiste, se esiste già allora la distruggo e
     * ritorno che è fallito l'inserimento */
    if(bstExists(graphBst->treeOfNames, key)) {
        adtDestruct(key);
        return false;
    }

    // Creo il nuovo vertice
    GraphVertex* newVertex = (GraphVertex*) malloc(sizeof(GraphVertex));
    newVertex->dataObject = adtClone(data);
    newVertex->name = vertexName;

    graphBst->numberOfVertices++;

    // Se il numero vertici coincide allora devo reallocare l'array di vertici e liste
    if(graphBst->numberOfVertices == graphBst->arraySize) {
        graphBst->arraySize *= 2;

        graphBst->vertices = (GraphVertex**) realloc(graphBst->vertices, sizeof(GraphVertex*) * graphBst->arraySize);
        graphBst->adjTree = (BSTObject**) realloc(graphBst->adjTree, sizeof(BSTObject*) * graphBst->arraySize);

        /* Imposto i nuovi spazi allocati a NULL per evitare letture non inizializzate */
        for(uint i = graphBst->arraySize / 2; i < graphBst->arraySize; ++i){
            graphBst->adjTree[i] = NULL;
            graphBst->vertices[i] = NULL;
        }
    }

    // Se la coda non è vuota posso riciclare un indice
    if(!queEmpty(graphBst->queueOfIds)) {
        DataObject* recycledIndex = queHeadNDequeue(graphBst->queueOfIds);
        uint* index = adtGetValue(recycledIndex);

        // Assegno l'indice riciclato (dovrebbe esserci NULL)
        graphBst->vertices[*index] = newVertex;

        ((ADTVertexMap*) key->value)->identifier = *index;

        free(index);
        adtDestruct(recycledIndex);
    }
    else { // Altrimenti devo creare un nuovo indice
        ((ADTVertexMap*) key->value)->identifier = graphBst->actualIndex;
        graphBst->vertices[graphBst->actualIndex++] = newVertex;
    }

    // Inserisco la nuova chiave nell'albero delle mappe
    bstInsert(graphBst->treeOfNames, key);

    adtDestruct(key);
    return true;
}

/** Rimuove un vertice nel grafo
  * @param graph il grafo
  * @param vertexName il nome del vertice da rimuovere
  * @param numberOfRemovedEdges il numero di archi rimossi a seguito dell'eliminazione del vertice
  * @return true se l'elemento è stato rimosso con successo, false altrimenti
  */
bool GraphBstRemoveVertex(void* graph, int vertexName, int* numberOfRemovedEdges) {
    GraphBst* graphBst = (GraphBst*) graph;
    bool ret = false;

    // Dichiaro un DataObject di supporto per cercare e togliere l'elemento
    DataObject* key = adtConstruct(graphBst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    // Provo a prendere dall'albero l'elemento
    DataObject* index = bstGetValue(graphBst->treeOfNames, key);

    // Se diverso da NULL allora l'elemento esiste
    if(index) {
        // Inizio ad impostare che il valore di ritorno sarà true
        ret = true;

        // Procedo a toglierlo dall'albero
        bstRemove(graphBst->treeOfNames, key);

        /* Lo tolgo dall'array, per farlo faccio il getValue di index che mi
         * ritorna a sua posizione nell'array*/
        int* ind = adtGetValue(index);
        adtDestruct(graphBst->vertices[*ind]->dataObject);
        free(graphBst->vertices[*ind]);
        graphBst->vertices[*ind] = NULL;

        // Procedo ad eliminare eventuali archi uscenti se esistono
        if(graphBst->adjTree[*ind]) {
            // Tolgo il numero di archi ed aggiorno la variabile
            graphBst->numberOfEdges -= graphBst->adjTree[*ind]->size;
            *numberOfRemovedEdges += graphBst->adjTree[*ind]->size;
            bstDestruct(graphBst->adjTree[*ind]);
            graphBst->adjTree[*ind] = NULL;
        }

        // Procedo ad eliminare eventuali archi entranti
        DataObject* oldName = adtConstruct(graphBst->intType);
        adtSetValue(oldName, &vertexName);

        for (uint i = 0; i < graphBst->arraySize; ++i) {
            // Verifico se c'è una lista per questo vertice
            if(graphBst->adjTree[i]) {
                // Se la rimozione ha successo, allora incremento gli archi tolti
                if(bstRemove(graphBst->adjTree[i], oldName)) {
                    graphBst->numberOfEdges--;
                    (*numberOfRemovedEdges)++;

                    // Se la lista è vuota allora devo distruggerla
                    if(bstEmpty(graphBst->adjTree[i])) {
                        bstDestruct(graphBst->adjTree[i]);
                        graphBst->adjTree[i] = NULL;
                    }
                }
            }
        }

        // Aggiungo il nuovo indice alla coda
        queEnqueue(graphBst->queueOfIds, oldName);
        graphBst->numberOfVertices--;

        adtDestruct(index);
        adtDestruct(oldName);
        free(ind);

        // Se utilizzo 1/4 dell'array devo dimezzarlo
        if(graphBst->numberOfVertices * 4 == graphBst->arraySize) {
            reduceAdjTree(graphBst);
        }
    }

    adtDestruct(key);
    return ret;
}

/** Inserisce un arco nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco è stato inserito con successo, false altrimenti
  */
bool GraphBstInsertEdge(void* graph, int vertexName1, int vertexName2) {
    GraphBst* graphBst = (GraphBst*) graph;
    bool ret = false;

    // Verifico se vertexId esista nell'albero tramite la funzione di supporto
    uint* vertexId = getIndexFromNameBst(graphBst, vertexName1);
    if(!vertexId) {
        return false;
    }

    // Verifico se vertex2 esiste senza utility perché mi serve il DataObject
    DataObject* key = adtConstruct(graphBst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName2;
    DataObject* vertex2 = bstGetValue(graphBst->treeOfNames, key);

    if(vertex2) {
        // Dichiaro un DataObject con un intero per andare a cercare nel bst
        DataObject* newElem = adtConstruct(graphBst->intType);
        adtSetValue(newElem, &vertexName2);

        // Se la lista è vuota, allora la creo ed inserisco l'intero
        if(graphBst->adjTree[*vertexId] == NULL) {
            graphBst->adjTree[*vertexId] = bstConstruct(graphBst->bstType);
            bstInsert(graphBst->adjTree[*vertexId], newElem);

            graphBst->numberOfEdges++;
            ret = true;
        }
        else {
            // Se l'arco non esiste, allora lo aggiungo
            if(!bstExists(graphBst->adjTree[*vertexId], newElem)) {
                bstInsert(graphBst->adjTree[*vertexId], newElem);

                graphBst->numberOfEdges++;
                ret = true;
            }
        }

        adtDestruct(newElem);
        adtDestruct(vertex2);
    }

    adtDestruct(key);
    free(vertexId);
    return ret;
}

/** Rimuove un arco nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco è stato rimosso con successo, false altrimenti
  */
bool GraphBstRemoveEdge(void* graph, int vertexName1, int vertexName2) {
    GraphBst* graphBst = (GraphBst*) graph;
    bool ret = false;

    // Se il numero di archi è 0, banalmente ritorno false
    if(graphBst->numberOfEdges == 0) {
        return false;
    }

    // Verifico se vertexId esista nell'albero tramite la funzione di supporto
    uint* vertexId = getIndexFromNameBst(graphBst, vertexName1);
    if(!vertexId) {
        return false;
    }

    // Verifico se vertex2 esiste senza utility perché mi serve il DataObject
    DataObject* key = adtConstruct(graphBst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName2;
    DataObject* vertex2 = bstGetValue(graphBst->treeOfNames, key);

    if(vertex2) {
        // Se la lista non è null e l'elemento esiste allora procedo a rimuoverlo
        if(graphBst->adjTree[*vertexId] && bstExists(graphBst->adjTree[*vertexId], vertex2)) {
            bstRemove(graphBst->adjTree[*vertexId], vertex2);

            // Se la lista è vuota la distruggo e setto il puntatore a NULL
            if(bstEmpty(graphBst->adjTree[*vertexId])) {
                bstDestruct(graphBst->adjTree[*vertexId]);
                graphBst->adjTree[*vertexId] = NULL;
            }

            ret = true;
        }
        // Se arrivo qui sono sicuro che vertex != NULL e posso liberarlo
        adtDestruct(vertex2);
    }

    adtDestruct(key);
    free(vertexId);
    return ret;
}

/** Verifica se un vertice esiste nel grafo
  * @param graph il grafo
  * @param vertexName il nome del vertice da cercare
  * @return true se il vertice con tale nome esiste esiste, false altrimenti
  */
bool GraphBstExistsVertex(void* graph, int vertexName) {
    GraphBst* graphBst = (GraphBst*) graph;

    // Se non ho vertici, allora non esiste
    if(graphBst->numberOfVertices == 0) {
        return false;
    }

    // Richiamo la funzione di supporto, ritorna NULL se non lo trova
    uint* vertexId = getIndexFromNameBst(graphBst, vertexName);

    if(vertexId) {
        free(vertexId);
        return true;
    }
    return false;
}

/** Verifica se un arco esiste nel grafo
  * @param graph il grafo
  * @param vertexName1 il nome del vertice con arco uscente
  * @param vertexName2 il nome del vertice con arco entrante
  * @return true se l'arco esiste, false altrimenti
  */
bool GraphBstExistsEdge(void* graph, int vertexName1, int vertexName2) {
    GraphBst* graphBst = (GraphBst*) graph;
    bool ret = false;

    // Verifico se vertexId esista nell'albero tramite la funzione di supporto
    uint* vertexId = getIndexFromNameBst(graphBst, vertexName1);
    if(!vertexId) {
        return false;
    }

    // Verifico se vertex2 esiste senza utility perché mi serve il DataObject
    DataObject* key = adtConstruct(graphBst->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName2;
    DataObject* vertex2 = bstGetValue(graphBst->treeOfNames, key);

    // Se vertex e la lista non sono null e l'elemento esiste, ritorno true
    if(vertex2) {
        if(graphBst->adjTree[*vertexId]) {
            DataObject* newElem = adtConstruct(graphBst->intType);
            adtSetValue(newElem, &vertexName2);

            if(bstExists(graphBst->adjTree[*vertexId], newElem)) {
                ret = true;
            }

            adtDestruct(newElem);
        }
        // Se sono qui vertex != NULL e posso liberarlo
        adtDestruct(vertex2);
    }

    adtDestruct(key);
    free(vertexId);
    return ret;
}

/** Ritorna una copia del DataObject collegato a quel nome
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @return DataObject la copia del DataObject se esiste, NULL altrimenti
  */
DataObject* GraphBstGetVertexData(void* graph, int vertexName) {
    GraphBst* graphBst = (GraphBst*) graph;

    // Se il grafo non ha vertici, banalmente ritorno NULL
    if(graphBst->numberOfVertices == 0) {
        return NULL;
    }

    // Verifico se esiste in corrispondenza nell'albero
    uint* vertexId = getIndexFromNameBst(graphBst, vertexName);
    if(!vertexId) {
        return NULL;
    }

    // Se arrivo qui allora esiste e posso ritornarne una copia
    DataObject* copy = adtClone(graphBst->vertices[*vertexId]->dataObject);

    free(vertexId);
    return copy;
}

/** Sovrascrive il DataObject collegato a quel nome
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @param newData Il nuovo dato che si intende inserire
  */
void GraphBstSetVertexData(void* graph, int vertexName, DataObject* newData) {
    GraphBst* graphBst = (GraphBst*) graph;

    // Il grafo deve avere almeno un vertice per fare la set
    if(graphBst->numberOfVertices > 0) {
        // Verifico se esiste la sua corrispondenza nell'albero
        uint *vertexId = getIndexFromNameBst(graphBst, vertexName);

        if (vertexId) {
            // Se arrivo qui allora esiste, elimino il precedente ed assegno una copia
            adtDestruct(graphBst->vertices[*vertexId]->dataObject);
            graphBst->vertices[*vertexId]->dataObject = adtClone(newData);
            free(vertexId);
        }
    }
}

/** Ritorna un vertice partendo da un puntatore
  * @param graph il grafo che contiene quel vertice
  * @param vertexId la posizione del vertice nell'array del grafo
  * @return GraphVertex il vertice del grafo
  */
GraphVertex* GraphBstVertexFromPointer(void* graph, void* vertexId) {
    GraphBst* graphBst = (GraphBst*) graph;
    uint index = *((uint*) vertexId);

    assert(index < graphBst->arraySize);

    return graphBst->vertices[index];
}

/** Ritorna un iteratore su tutti i vertici del grafo
  * @param grafo su cui si intende iterare
  * @return ITRObject che permette l'iterazione
  */
void* GraphBstVertices(void* graph){
    ITRType* graphBstType = ConstructGraphBstVertType();
    ITRObject* iterator = itrConstruct(graphBstType, graph);

    return iterator;
}

/** Ritorna un iteratore sugli adiacenti di un dato vertice
  * @param grafo su cui si intende iterare
  * @param vertexName il nome del vertice su cui cercare gli adiacenti
  * @return ITRObject che permette l'iterazione
  */
void* GraphBstVertexEdges(void* graph, int vertexName) {
    // Creo un graphBstSupport
    GraphBstSupport* support = (GraphBstSupport*) malloc(sizeof(GraphBstSupport));
    GraphBst* graphLst = (GraphBst*) graph;
    support->graph = graph;

    // Converto il nome in indice per l'array se esiste
    uint* tmp = getIndexFromNameBst(graph, vertexName);

    if(!tmp) {
        support->root = NULL;
    }
    else {
        // Verifico se esistono adiacenti a quell'elemento
        if(graphLst->adjTree[*tmp]) {
            support->root = graphLst->adjTree[*tmp]->root;
        }
        else {
            /* Non esiste una lista per quel vertice, imposto NULL
             * così tornerà subito iteratore terminato */
            support->root = NULL;
        }
        free(tmp);
    }

    ITRType* graphMatEdges = ConstructGraphBstEdgesType();
    ITRObject* iterator = itrConstruct(graphMatEdges, support);

    free(support);
    return iterator;
}

/** Ridimensiona correttamente l'array e la lista di adiacenza
  * @param graph il grafo
  */
void reduceAdjTree(GraphBst* graphBst) {
    // Creo una nuova lista ed un nuovo array più piccoli
    GraphVertex** newArray = (GraphVertex**) malloc(((graphBst->arraySize) / 2) * sizeof(GraphVertex*));
    BSTObject** newAdjTree = (BSTObject**) malloc(((graphBst->arraySize) / 2) * sizeof(BSTObject*));

    // Scorro l'array dei vertici per scoprire quali locazioni devo prendere
    uint j = 0;
    for(uint i = 0; i < graphBst->arraySize; ++i) {

        // Ho trovato un vertice candidato in posizione i
        if(graphBst->vertices[i]) {

            // Salvo il riferimento a questo valore
            newArray[j] = graphBst->vertices[i];

            // Sposto anche la sua lista di adiacenza
            newAdjTree[j] = graphBst->adjTree[i];

            // Refresho l'indice nell'albero
            DataObject* tmp = adtConstruct(graphBst->vertexType);
            ((ADTVertexMap*) tmp->value)->vertexName = graphBst->vertices[i]->name;

            DataObject* oldVertex = bstGetValue(graphBst->treeOfNames, tmp);
            ((ADTVertexMap*) oldVertex->value)->identifier = j;
            bstRemove(graphBst->treeOfNames, tmp);
            bstInsert(graphBst->treeOfNames, oldVertex);

            adtDestruct(oldVertex);
            adtDestruct(tmp);

            // Avanzo l'indice
            j++;
        }
    }

    /* Imposto gli indici non usati dell'array e della lista a NULL per non avere
     * letture di valori non inizializzati */
    for (int k = j; k < graphBst->arraySize / 2; ++k) {
        newAdjTree[k] = NULL;
        newArray[k] = NULL;
    }

    // Svuoto la queue degli indici, potrebbe essere sporca e puntare ad elementi fuori dall'array
    queClear(graphBst->queueOfIds);

    // Il nuovo actualIndex deve partire da j
    graphBst->actualIndex = j;

    // Libero la memoria del vecchio array e adjList
    free(graphBst->vertices);
    free(graphBst->adjTree);

    // Sovrascrivo i vecchi valori e dimezzo la grandezza
    graphBst->vertices = newArray;
    graphBst->adjTree = newAdjTree;
    graphBst->arraySize /= 2;
}

/** Converte una nome in un indice per la matrice e l'array
  * @param graph il grafo
  * @param vertexName il nome del vertice
  * @return uint indice se esiste, NULL altrimenti
  */
uint* getIndexFromNameBst(GraphBst *graph, int vertexName) {
    GraphBst* graphMat = (GraphBst*) graph;
    uint* ret = NULL;

    // Dichiaro un DataObject di supporto per verificare l'esistenza del nome
    DataObject* key = adtConstruct(graphMat->vertexType);
    ((ADTVertexMap*) key->value)->vertexName = vertexName;

    DataObject* index = bstGetValue(graphMat->treeOfNames, key);

    // Se mi è tornato un valore allora esiste corrispondenza
    if(index) {
        ret = adtGetValue(index);
    }

    adtDestruct(key);
    adtDestruct(index);
    return ret;
}
