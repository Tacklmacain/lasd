#include "../stack/vec/stackvec.h"
#include "graphitrpostord.h"

/* ************************************************************************** */

ITRType* ConstructGraphPostOrderIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &GraphPostOrderIteratorConstruct;
    type->destruct = &GraphPostOrderIteratorDestruct;
    type->element = &GraphPostOrderIteratorElement;
    type->successor = &GraphPostOrderIteratorSuccessor;
    type->isTerminated = &GraphPostOrderIteratorTerminated;

    return type;
}

void DestructGraphPostOrderIterator(ITRType* type) {
    free(type);
}

void* GraphPostOrderIteratorConstruct(void* graph) {
    GraphPostOrderIterator* iter = (GraphPostOrderIterator*)malloc(sizeof(GraphPostOrderIterator));
    GraphObject* graphObject = (GraphObject*) graph;

    // Assegno il grafo, alloco i colori e li inizializzo
    iter->graph = graphObject;
    iter->color = (char*) malloc(graphObject->numberOfVertex * sizeof(char));

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        iter->color[i] = 'b';
    }

    StackType* type = ConstructStackVecType();
    iter->stackVertices = stkConstruct(type);
    iter->stackAdjacent = stkConstruct(type);
    iter->stackIndexes = stkConstruct(type);
    iter->ptrType = ConstructPtrDataType();
    iter->verticesIterator = graphVertices(graph);

    DataObject* data = adtConstruct(iter->ptrType);

    // Richiamo l'iteratore per prendere il primo vertice
    if(!itrTerminated(iter->verticesIterator)) {
        iter->currentIndex = (uint*) itrElement(iter->verticesIterator);
        iter->currentVertex = graphVertexFromPointer(graphObject, iter->currentIndex);
        iter->color[*iter->currentIndex] = 'g';

        itrSuccessor(iter->verticesIterator);

        iter->currentAdjIterator = graphVertexEdges(iter->graph, iter->currentVertex->name);
        while(!itrTerminated(iter->currentAdjIterator)) {
            uint* adjIndex = itrElement(iter->currentAdjIterator);
            GraphVertex* adjVertex = graphVertexFromPointer(iter->graph, adjIndex);

            itrSuccessor(iter->currentAdjIterator);

            // Se il vertice è bianco continuo con la discesa
            if(iter->color[*adjIndex] == 'b') {
                // Salvo gli stati nello stack e continuo la discesa
                adtSetValue(data, iter->currentIndex);
                stkPush(iter->stackIndexes, data);
                adtSetValue(data, iter->currentVertex);
                stkPush(iter->stackVertices, data);
                adtSetValue(data, iter->currentAdjIterator);
                stkPush(iter->stackAdjacent, data);

                // Il vertice bianco diventa il nuovo vertice corrente
                iter->currentVertex = adjVertex;
                iter->color[*adjIndex] = 'g';

                // Il nuovo iteratore degli adiacenti è relativo a questo vertice bianco
                iter->currentAdjIterator = graphVertexEdges(iter->graph, adjVertex->name);
            }
        }
        destructIteratorAndType(iter->currentAdjIterator);
    }
    else {
        iter->currentVertex = NULL;
    }

    adtDestruct(data);
    return iter;
}

void GraphPostOrderIteratorDestruct(void* iterator) {
    GraphPostOrderIterator* iter = (GraphPostOrderIterator*) iterator;
    StackType* stackType = iter->stackVertices->type;

    // Distruggo l'iteratore ed il suo tipo
    destructIteratorAndType(iter->verticesIterator);
    //destructIteratorAndType(iter->currentAdjIterator);

    stkClear(iter->stackVertices);
    stkClear(iter->stackAdjacent);
    stkClear(iter->stackIndexes);
    stkDestruct(iter->stackIndexes);
    stkDestruct(iter->stackVertices);
    stkDestruct(iter->stackAdjacent);
    DestructStackVecType(stackType);
    DestructPtrDataType(iter->ptrType);
    free(iter->color);
    free(iter);
}

bool GraphPostOrderIteratorTerminated(void* iterator) {
    // Ritorno true solo se non ho un corrente
    return ((GraphPostOrderIterator*) iterator)->currentVertex == NULL;
}

void* GraphPostOrderIteratorElement(void* iterator) {
    return ((GraphPostOrderIterator*) iterator)->currentVertex->dataObject;
}

void GraphPostOrderIteratorSuccessor(void* iterator) {
    GraphPostOrderIterator* iter = (GraphPostOrderIterator*) iterator;
    DataObject* dataptr;
    DataObject* data = adtConstruct(iter->ptrType);

    // Se lo stack è vuoto non si può risalire e si setta itr->curr a null per forzare l'uscita
    if(!stkEmpty(iter->stackVertices)) {

        // Prendo dallo stack i nuovi elementi correnti e faccio la ricerca dei prossimi
        dataptr = stkTopNPop(iter->stackVertices);
        iter->currentVertex = adtGetValue(dataptr);
        adtDestruct(dataptr);

        dataptr = stkTopNPop(iter->stackIndexes);
        iter->currentIndex = adtGetValue(dataptr);
        adtDestruct(dataptr);


        dataptr = stkTopNPop(iter->stackAdjacent);
        iter->currentAdjIterator = adtGetValue(dataptr);
        adtDestruct(dataptr);

        //terminerà quando troverà un elemento senza adiacenti (o senza adiacenti bianchi)
        while(!itrTerminated(iter->currentAdjIterator)) {
            uint* adjIndex = itrElement(iter->currentAdjIterator);
            GraphVertex* adjVertex = graphVertexFromPointer(iter->graph, adjIndex);

            itrSuccessor(iter->currentAdjIterator);

            if(iter->color[*adjIndex] == 'b') {
                // Salvo gli stati nello stack e continuo la discesa
                adtSetValue(data, iter->currentIndex);
                stkPush(iter->stackIndexes, data);
                adtSetValue(data, iter->currentVertex);
                stkPush(iter->stackVertices, data);
                adtSetValue(data, iter->currentAdjIterator);
                stkPush(iter->stackAdjacent, data);

                // Il vertice bianco diventa il nuovo vertice corrente
                iter->currentVertex = adjVertex;
                iter->color[*adjIndex] = 'g';

                // Il nuovo iteratore degli adiacenti è relativo a questo vertice bianco
                iter->currentAdjIterator = graphVertexEdges(iter->graph, adjVertex->name);
            }
        }
        destructIteratorAndType(iter->currentAdjIterator);
    }
    else {
        iter->currentVertex = NULL;
    }

    if(!iter->currentVertex) {
        // Controllo se ci sono altri vertici bianchi da cui far partire la dfs
        while(!itrTerminated(iter->verticesIterator) && !iter->currentVertex) {
            uint* nextIndex = itrElement(iter->verticesIterator);
            GraphVertex* nextVertex = graphVertexFromPointer(iter->graph, nextIndex);

            if(iter->color[*nextIndex] == 'b') {
                // Se trovo un vertice bianco sarà il prossimo vertice della dfs
                iter->currentVertex = nextVertex;
                iter->currentIndex = nextIndex;

                iter->color[*iter->currentIndex] = 'g';
                iter->currentAdjIterator = graphVertexEdges(iter->graph, nextVertex->name);

                //terminerà quando troverà un elemento senza adiacenti (o senza adiacenti bianchi)
                while(!itrTerminated(iter->currentAdjIterator)) {
                    uint* innerIndex = itrElement(iter->currentAdjIterator);
                    GraphVertex* innerVertex = graphVertexFromPointer(iter->graph, innerIndex);

                    itrSuccessor(iter->currentAdjIterator);

                    if(iter->color[*innerIndex] == 'b') {
                        // Salvo gli stati nello stack e continuo la discesa
                        adtSetValue(data, iter->currentIndex);
                        stkPush(iter->stackIndexes, data);
                        adtSetValue(data, iter->currentVertex);
                        stkPush(iter->stackVertices, data);

                        adtSetValue(data, iter->currentAdjIterator);
                        stkPush(iter->stackAdjacent, data);

                        // Il vertice bianco diventa il nuovo vertice corrente
                        iter->currentVertex = innerVertex;
                        iter->currentIndex = innerIndex;
                        iter->color[*innerIndex] = 'g';

                        // Il nuovo iteratore degli adiacenti è relativo a questo vertice bianco
                        iter->currentAdjIterator = graphVertexEdges(iter->graph, innerVertex->name);
                    }
                }
                destructIteratorAndType(iter->currentAdjIterator);
            }

            itrSuccessor(iter->verticesIterator);
        }
    }

    adtDestruct(data);
}