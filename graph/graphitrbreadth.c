#include "graphitrbreadth.h"
#include "../queue/vec/queuevec.h"

/* ************************************************************************** */

ITRType* ConstructGraphBreadthIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &GraphBreadthIteratorConstruct;
    type->destruct = &GraphBreadthIteratorDestruct;
    type->element = &GraphBreadthIteratorElement;
    type->successor = &GraphBreadthIteratorSuccessor;
    type->isTerminated = &GraphBreadthIteratorTerminated;

    return type;
}

void DestructGraphBreadthIterator(ITRType* type) {
    free(type);
}

void* GraphBreadthIteratorConstruct(void* graph) {
    GraphBreadthIterator* iter = (GraphBreadthIterator*) malloc(sizeof(GraphBreadthIterator));
    GraphObject* graphObject = (GraphObject*) graph;
    QueueType* queueType = ConstructQueueVecType();

    iter->graph = graph;
    iter->color = (char*) malloc(graphObject->numberOfVertex * sizeof(char));
    iter->queue = queConstruct(queueType);
    iter->intType = ConstructIntDataType();

    // Inizializzo tutti i colori a 'b'
    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        iter->color[i] = 'b';
    }

    DataObject* newElem = adtConstruct(iter->intType);

    /* Se il grafo ha almeno un elemento, è possibile fare la BST, utilizzo vertices
     * in quanto non so la posizione del primo vertice nell'array dei vertici */
    iter->verticesIterator = graphVertices(graph);
    if(!itrTerminated(iter->verticesIterator)) {
        uint* index = itrElement(iter->verticesIterator);
        iter->color[*index] = 'g';

        GraphVertex* vertex = graphVertexFromPointer(iter->graph, index);
        iter->currentVertex = vertex;


        // Scorro tutti gli adiacenti di questo vertice e li accodo se bianchi
        ITRObject* adjIter = graphVertexEdges(iter->graph, vertex->name);
        while(!itrTerminated(adjIter)) {
            uint* innerId = itrElement(adjIter);

            if(iter->color[*innerId] == 'b') {
                adtSetValue(newElem, innerId);
                queEnqueue(iter->queue, newElem);
                iter->color[*innerId] = 'g';
            }
            itrSuccessor(adjIter);
        }
        destructIteratorAndType(adjIter);
        itrSuccessor(iter->verticesIterator);
    }
    else {
        iter->currentVertex = NULL;
    }

    adtDestruct(newElem);
    return iter;
}

void GraphBreadthIteratorDestruct(void* iterator) {
    GraphBreadthIterator* iter = (GraphBreadthIterator*) iterator;
    QueueType* type = iter->queue->type;

    destructIteratorAndType(iter->verticesIterator);
    queDestruct(iter->queue);
    DestructQueueVecType(type);
    DestructIntDataType(iter->intType);
    free(iter->color);
    free(iter);
}

bool GraphBreadthIteratorTerminated(void* iterator) {
    GraphBreadthIterator* iter = (GraphBreadthIterator*) iterator;

    // Ritorno true solo se non ho un corrente e la coda è vuota
    return !iter->currentVertex && queEmpty(iter->queue);
}

void* GraphBreadthIteratorElement(void* iterator) {
    return ((GraphBreadthIterator*) iterator)->currentVertex->dataObject;
}

void GraphBreadthIteratorSuccessor(void* iterator) {
    GraphBreadthIterator* iter = (GraphBreadthIterator*) iterator;
    DataObject* newElem = adtConstruct(iter->intType);
    bool found = false;

    // Ciclo finché non trovo un elemento
    while(!found) {

        // Controllo se posso prenderlo dalla coda
        if(!queEmpty(iter->queue)) {
            // Prendo il vertice dalla coda e lo assegno a currentVertex
            DataObject* name = queHeadNDequeue(iter->queue);
            uint* currIndex = adtGetValue(name);
            GraphVertex* vertex = graphVertexFromPointer(iter->graph, currIndex);
            iter->currentVertex = vertex;
            found = true;

            // Imposto che il suo colore è ora nero, siccome l'ho raggiunto
            iter->color[*currIndex] = 'n';

            // Scorro tutti gli adiacenti di questo vertice e li accodo se bianchi
            ITRObject* adjIter = graphVertexEdges(iter->graph, vertex->name);
            while(!itrTerminated(adjIter)) {
                uint* innerId = itrElement(adjIter);

                if(iter->color[*innerId] == 'b') {
                    adtSetValue(newElem, innerId);
                    queEnqueue(iter->queue, newElem);
                    iter->color[*innerId] = 'g';
                }
                itrSuccessor(adjIter);
            }
            destructIteratorAndType(adjIter);
            adtDestruct(name);
            free(currIndex);
        }
        // Se non posso prenderlo dalla coda, verifico se ho vertici bianchi nel grafo
        else if(!itrTerminated(iter->verticesIterator)) {

            // Ciclo finché non termino i vertici e li incodo
            while(!itrTerminated(iter->verticesIterator)) {
                uint* index = itrElement(iter->verticesIterator);

                if(iter->color[*index] == 'b') {
                    adtSetValue(newElem, index);
                    queEnqueue(iter->queue, newElem);
                    iter->color[*index] = 'g';
                }

                itrSuccessor(iter->verticesIterator);
            }
        }
        /* Se arrivo qui allora non ho vertici bianchi né in coda, posso settare
         * currentVertex a NULL in modo da realizzare la condizione di terminazione */
        else {
            iter->currentVertex = NULL;
            found = true;
        }
    }

    adtDestruct(newElem);
}