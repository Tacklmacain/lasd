
#ifndef GRAPHITRBREADTH_H
#define GRAPHITRBREADTH_H

/* ************************************************************************** */

#include "../itr/itr.h"
#include "../queue/queue.h"
#include "graph.h"

/* ************************************************************************** */

/** Struttura per l'iteratore in ampiezza di un grafo
  * @attribute graph
  * Il grafo su cui si intende iterare
  *
  * @attribute queue
  * La coda dei vertici da esplorare
  *
  * @attribute currentVertex
  * Il vertice corrente
  *
  * @attribute verticesIterator
  * Iteratore dei vertici, permette di esplorare vertici che non sono
  * connessi in un grafo non connesso
  *
  * @attribute color
  * Array di colori per i vari vertici, ha come possibili valori:
  * 'b' (vertice non scoperto), 'g' (vertice scoperto), 'n' (visitato)
  *
  * @attribute intType
  * Tipo per gli elementi della queue, evita memory leak
  */
typedef struct GraphBreadthIterator {
    GraphObject* graph;
    QueueObject* queue;
    GraphVertex* currentVertex;
    ITRObject* verticesIterator;
    char* color;

    DataType* intType;
} GraphBreadthIterator;

/* ************************************************************************** */

ITRType* ConstructGraphBreadthIterator();
void DestructGraphBreadthIterator(ITRType*);

void* GraphBreadthIteratorConstruct(void*);
void GraphBreadthIteratorDestruct(void*);
bool GraphBreadthIteratorTerminated(void*);
void* GraphBreadthIteratorElement(void*);
void GraphBreadthIteratorSuccessor(void*);

/* ************************************************************************** */

#endif
