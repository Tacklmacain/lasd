#ifndef GRAPH_H
#define GRAPH_H

/* ************************************************************************** */

#include "../utility.h"
#include "../adt/adt.h"
#include "../adt/vertex/vertex.h"
#include "../itr/itr.h"
#include "../bst/bst.h"
#include "../bst/rec/bstrec.h"
#include "../bst/itr/bstitr.h"

/* ************************************************************************** */

// La struttura del singolo vertice, ha un nome ed un dato
typedef struct GraphVertex {
    int name;
    DataObject* dataObject;
} GraphVertex;

/* ************************************************************************** */

// Funzioni della Rappresentazione:
typedef void* (*GraphConstruct)();
typedef void (*GraphDestruct)(void*);

typedef bool (*GraphEmpty)(void*);

typedef void* (*GraphClone)(void*);
typedef void* (*GraphTranspose)(void*);

typedef bool (*GraphInsertVertex)(void*, DataObject*, int);
typedef bool (*GraphRemoveVertex)(void*, int, int*);

typedef bool (*GraphInsertEdge)(void*, int, int);
typedef bool (*GraphRemoveEdge)(void*, int, int);

typedef bool (*GraphExistsVertex)(void*, int);
typedef bool (*GraphExistsEdge)(void*, int, int);

typedef DataObject* (*GraphGetVertexData)(void*, int);
typedef void (*GraphSetVertexData)(void*, int, DataObject*);

typedef GraphVertex* (*GraphVertexFromPointer)(void*, void*);

typedef void* (*GraphVertices)(void*);
typedef void* (*GraphVertexEdges)(void*, int);



// Funzioni del Type:
typedef bool (*GraphEqual)(void*, void*);

// Torna -1 se non esiste il percorso e la distanza se esiste
typedef int (*GraphShortestPath)(void* grafo, int indice, int indice2);

typedef bool (*GraphIsAcyclic)(void*);

// Torna un iteratore e si vede controllare che non sia ciclico (assert(graphIsAscyclic(graph))
typedef void* (*GraphTopologicalOrder)(void*);

typedef void* (*GraphMaximalReachableSubgraph)(void*, int indice);

typedef void* (*GraphSCCGraph)(void*);

typedef void (*GraphPreOrderMap)(void*, MapFun, void*);
typedef void (*GraphPostOrderMap)(void*, MapFun, void*);
typedef void (*GraphBreadthMap)(void*, MapFun, void*);
typedef void (*GraphPreOrderFold)(void*, FoldFun, void*, void*);
typedef void (*GraphPostOrderFold)(void*, FoldFun, void*, void*);
typedef void (*GraphBreadthFold)(void*, FoldFun, void*, void*);

/* ************************************************************************** */

typedef struct GraphRepresentation {
    GraphConstruct construct;
    GraphDestruct destruct;
    GraphEmpty isEmpty;
    GraphClone clone;
    GraphTranspose transpose;
    GraphInsertVertex insertVertex;
    GraphRemoveVertex removeVertex;
    GraphInsertEdge insertEdge;
    GraphRemoveEdge removeEdge;
    GraphExistsVertex existsVertex;
    GraphExistsEdge existsEdge;
    GraphGetVertexData getVertexData;
    GraphSetVertexData setVertexData;
    GraphVertexFromPointer vertexFromPointer;
    GraphVertices vertices;
    GraphVertexEdges vertexEdges;
} GraphRepresentation;

typedef struct GraphType {
    GraphRepresentation* representation;
    GraphEqual equal;
    GraphShortestPath shortestPath;
    GraphIsAcyclic isAcyclic;
    GraphTopologicalOrder topologicalOrder;
    GraphMaximalReachableSubgraph maximalReachableSubgraph;
    GraphSCCGraph sccGraph;
    GraphPreOrderMap preOrderMap;
    GraphPostOrderMap postOrderMap;
    GraphBreadthMap breadthMap;
    GraphPreOrderFold preOrderFold;
    GraphPostOrderFold postOrderFold;
    GraphBreadthFold breadthFold;
} GraphType;

typedef struct GraphObject {
    GraphType* type;
    void* structure;
    uint numberOfVertex, numberOfEdges;
} GraphObject;

/* ************************************************************************** */

GraphObject* graphConstruct(GraphType*);
void graphDestruct(GraphObject*);

bool graphEmpty(GraphObject*);

uint graphVertexNumber(GraphObject*);
uint graphEdgeNumber(GraphObject*);

void graphClear(GraphObject*);

GraphObject* graphClone(GraphObject*);
GraphObject* graphTranspose(GraphObject*);

void graphInsertVertex(GraphObject*, DataObject*, int);
void graphRemoveVertex(GraphObject*, int);

void graphInsertEdge(GraphObject*, int, int);
void graphRemoveEdge(GraphObject*, int, int);

bool graphExistsVertex(GraphObject*, int);
bool graphExistsEdge(GraphObject*, int, int);

DataObject* graphGetVertexData(GraphObject*, int);
void graphSetVertexData(GraphObject*, int, DataObject*);

GraphVertex* graphVertexFromPointer(GraphObject*, void*);

ITRObject* graphVertices(GraphObject*);
ITRObject* graphVertexEdges(GraphObject*, int);

bool graphEqual(GraphObject*, GraphObject*);

bool graphExistsVertexWithData(GraphObject*, DataObject*);

int graphShortestPath(GraphObject*, int, int);

bool graphIsAcyclic(GraphObject*);
ITRObject* graphTopologicalOrder(GraphObject*);

GraphObject* graphMaximalReachableSubgraph(GraphObject*, int);

GraphObject* graphSCCGraph(GraphObject*);

void graphPreOrderMap(GraphObject*, MapFun, void*);
void graphPostOrderMap(GraphObject*, MapFun, void*);
void graphBreadthMap(GraphObject*, MapFun, void*);
void graphPreOrderFold(GraphObject*, FoldFun, void*, void*);
void graphPostOrderFold(GraphObject*, FoldFun, void*, void*);
void graphBreadthFold(GraphObject*, FoldFun, void*, void*);

/* ************************************************************************** */

#endif
