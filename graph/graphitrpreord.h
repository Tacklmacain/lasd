#ifndef GRAPHITRPREORD_H
#define GRAPHITRPREORD_H

/* ************************************************************************** */

#include "../itr/itr.h"
#include "../stack/stack.h"
#include "graph.h"

/* ************************************************************************** */

/** Struttura per l'iteratore in PreOrder di un grafo
  * @attribute graph
  * Il grafo su cui si intende iterare
  *
  * @attribute currentVertex
  * Il vertice attuale dell'iterazione
  *
  * @attribute color
  * Array di colori per i vari vertici, ha come possibili valori:
  * 'b' (vertice non scoperto), 'g' (vertice scoperto), 'n' (visitato)
  *
  * @attribute StackVertices/StackAdjacent
  * Stack per simulare la risalita
  *
  * @attribute verticesIterator/currentAdjIterator
  * Iteratori dei vertici ed adiacenti del currentVertex
  *
  * @attribute ptrType
  * Tipo per gli elementi dello stack, evita memory leak
  */
typedef struct GraphPreOrderIterator {
    GraphObject* graph;
    GraphVertex* currentVertex;
    char* color;

    StackObject* stackVertices;
    StackObject* stackAdjacent;

    ITRObject* verticesIterator;
    ITRObject* currentAdjIterator;

    DataType *ptrType;
} GraphPreOrderIterator;

/* ************************************************************************** */

ITRType* ConstructGraphPreOrderIterator();
void DestructGraphPreOrderIterator(ITRType*);

void* GraphPreOrderIteratorConstruct(void*);
void GraphPreOrderIteratorDestruct(void*);
bool GraphPreOrderIteratorTerminated(void*);
void* GraphPreOrderIteratorElement(void*);
void GraphPreOrderIteratorSuccessor(void*);

/* ************************************************************************** */

#endif
