#include "graphrec.h"

/* ************************************************************************** */

// Prototipi per le funzioni ricorsive di DFS_Visit
void DFSVisitPreOrderMap(GraphObject*, char*, uint, MapFun, void*);
void DFSVisitPreOrderFold(GraphObject*, char*, uint, FoldFun, void*, void*);
void DFSVisitPostOrderMap(GraphObject*, char*, uint, MapFun, void*);
void DFSVisitPostOrderFold(GraphObject*, char*, uint, FoldFun, void*, void*);

// Prototipi di funzioni di supporto ricorsive
bool equalRecSupport(ITRObject*, ITRObject*, GraphObject*, GraphObject*);
bool isAcyclicRecSupport(GraphObject*, char*, uint);
void* maximalReachableSubGraphRecSupport(GraphObject*, GraphObject*, char*, uint);
StackObject* topologicalOrderRecSupport(GraphObject*, StackObject*, char*, uint, DataType*);
StackObject* DFS1_SCCGGraph(GraphObject*, int*, uint, StackObject*, DataType*, int);
void DFS2_SCCGGraph(GraphObject*, int*, uint, int, GraphObject*);

/* ************************************************************************** */

GraphType* ConstructGraphRecursive(GraphRepresentation* representation) {
    GraphType* type = (GraphType*) malloc(sizeof(GraphType));

    type->representation = representation;
    type->equal = graphEqualRec;
    type->shortestPath = graphShortestPathRec;
    type->isAcyclic = graphIsAcyclicRec;
    type->topologicalOrder = graphTopologicalOrderRec;
    type->maximalReachableSubgraph = graphMaximalReachableSubgraphRec;
    type->sccGraph = graphSCCGraphRec;
    type->preOrderMap = graphPreOrderMapRec;
    type->postOrderMap = graphPostOrderMapRec;
    type->breadthMap = graphBreadthMapRec;
    type->preOrderFold = graphPreOrderFoldRec;
    type->postOrderFold = graphPostOrderFoldRec;
    type->breadthFold = graphBreadthFoldRec;

    return type;
}

void DestructGraphRecursive(GraphType* type) {
    free(type);
}

/** Verifica se due grafi in ingresso sono uguali in struttura ed in elementi
  * @param graph1 il primo grafo
  * @param graph2 il secondo grafo
  * @return true se sono identici, false altrimenti
  */
bool graphEqualRec(void* graph1, void* graph2) {
    // Dichiaro 2 iteratori
    ITRObject *vertices1 = graphVertices(graph1), *vertices2 = graphVertices(graph2);

    // Richiamo al funzione ricorsiva di supporto
    bool result = equalRecSupport(vertices1, vertices2, graph1, graph2);

    destructIteratorAndType(vertices1);
    destructIteratorAndType(vertices2);
    return result;
}

bool equalRecSupport(ITRObject* iterator1, ITRObject* iterator2, GraphObject* graph1, GraphObject* graph2) {
    // Caso base, l'iteratore e' terminato ed il ritorno e' true
    if(itrTerminated(iterator1)) {
        return true;
    }
    // Caso ricorsivo, itero sull'elemento corrente e mi richiamo ricorsivamente
    else {
        uint *index1 = itrElement(iterator1), *index2 = itrElement(iterator2);

        GraphVertex* vertex1 = graphVertexFromPointer(graph1, index1);
        GraphVertex* vertex2 = graphVertexFromPointer(graph2, index2);

        // Se i vertici attuali hanno DataObject o nome diverso, ritorno false
        if(adtCompare(vertex1->dataObject, vertex2->dataObject) != 0 || vertex1->name != vertex2->name) {
            return false;
        }
        else {
            // Dichiaro l'iteratore per gli adiacenti di questo vertice
            ITRObject* adjIterator1 = graphVertexEdges(graph1, vertex1->name);
            ITRObject* adjIterator2 = graphVertexEdges(graph2, vertex2->name);

            while(!itrTerminated(adjIterator1) && !itrTerminated(adjIterator2)) {
                void *innerId1 = itrElement(adjIterator1), *innerId2 = itrElement(adjIterator2);

                GraphVertex* innerVertex1 = graphVertexFromPointer(graph1, innerId1);
                GraphVertex* innerVertex2 = graphVertexFromPointer(graph2, innerId2);

                // Se sono uguali distruggo tutto e ritorno false, altrimenti continuo ad iterare
                if(adtCompare(innerVertex1->dataObject, innerVertex2->dataObject) != 0 || innerVertex1->name != innerVertex2->name) {
                    destructIteratorAndType(adjIterator1);
                    destructIteratorAndType(adjIterator2);
                    return false;
                }
                itrSuccessor(adjIterator1);
                itrSuccessor(adjIterator2);
            }

            // Se solo uno dei due e' finito distruggo tutto e ritorno false
            if(!itrTerminated(adjIterator1) != !itrTerminated(adjIterator2)) {
                destructIteratorAndType(adjIterator1);
                destructIteratorAndType(adjIterator2);
                return false;
            }

            destructIteratorAndType(adjIterator1);
            destructIteratorAndType(adjIterator2);

            // Se arrivo qui i grafi sono uguali finora, avanzo e richiamo ricorsivamente
            itrSuccessor(iterator1);
            itrSuccessor(iterator2);
            return equalRecSupport(iterator1, iterator2, graph1, graph2);
        }
    }
}

/** Ritorna la distanza minima tra un vertice ed un altro
  * @param graph il grafo che contiene i due vertici
  * @param start il vertice di partenza
  * @param arrival il vertice di destinazione
  * @return la distanza fra i due vertici, -1 se non esiste un percorso tra di loro
  *
  * @note Questo algoritmo può essere scritto solo utilizzando la BFS e quindi non
  * può essere scritto in modo ricorsivo, di conseguenza la sua implementazione
  * ricorsiva e' identica a quella iterativa
  */
int graphShortestPathRec(void* graph, int start, int arrival) {
    GraphObject* graphObject = (GraphObject*) graph;
    int shortestPath = -1;

    // Mi scorro i vari vertici per verificare che esistano entrambi
    ITRObject* vertices = graphVertices(graph);
    uint startId = -1, arrivalId = -1;
    bool foundStart = false, foundArrival = false;

    while(!itrTerminated(vertices) && (!foundStart || !foundArrival)) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graph, index);

        // Se start o arrival esistono, allora mi prendo il loro index
        if(start == vertex->name) {
            startId = *index;
            foundStart = true;
        }

        if(arrival == vertex->name) {
            arrivalId = *index;
            foundArrival = true;
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    /* Se entrambi sono falsi, oppure solo uno dei due e' falso, ritorno -1 */
    if((!foundStart && !foundArrival) || foundStart != foundArrival) {
        shortestPath = -1;
    }
    // Altrimenti se sono lo stesso vertice il percorso e' 0
    else if(startId == arrivalId) {
        shortestPath = 0;
    }
    // Altrimenti potrebbe esistere un percorso
    else {
        // Dichiaro l'array dei colori, array delle distanze e la coda
        char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);
        int* distance = (int*) malloc(sizeof(int) * graphObject->numberOfVertex);
        DataType* intType = ConstructIntDataType();
        DataObject* newElem = adtConstruct(intType);
        QueueType* queueType = ConstructQueueVecType();
        QueueObject* queue = queConstruct(queueType);

        // Inizializzo i colori a bianco e le distanze a -1
        for (int i = 0; i < graphObject->numberOfVertex; ++i) {
            color[i] = 'b';
            distance[i] = -1;
        }

        /* Imposto che il colore grigio e la distanza 0 del nodo iniziale
         * e lo accodo */
        color[startId] = 'g';
        distance[startId] = 0;
        adtSetValue(newElem, &startId);
        queEnqueue(queue, newElem);

        // Effettuo la BFS, la interrompo quando trovo l'elemento
        bool found = false;
        while(!queEmpty(queue) && !found) {
            DataObject *name = queHeadNDequeue(queue);
            uint *currIndex = adtGetValue(name);
            GraphVertex *vertex = graphVertexFromPointer(graph, currIndex);

            color[*currIndex] = 'n';

            // Scorro tutti gli adiacenti di questo vertice e li accodo se bianchi
            ITRObject *adjIter = graphVertexEdges(graph, vertex->name);
            while (!itrTerminated(adjIter) && !found) {
                uint *innerId = itrElement(adjIter);

                if (color[*innerId] == 'b') {
                    adtSetValue(newElem, innerId);
                    queEnqueue(queue, newElem);
                    color[*innerId] = 'g';
                    distance[*innerId] = distance[*currIndex] + 1;

                    if(*innerId == arrivalId) {
                        shortestPath = distance[arrivalId];
                        found = true;
                    }
                }
                itrSuccessor(adjIter);
            }
            destructIteratorAndType(adjIter);

            adtDestruct(name);
            free(currIndex);
        }

        queClear(queue);
        queDestruct(queue);
        DestructQueueVecType(queueType);
        adtDestruct(newElem);
        DestructIntDataType(intType);
        free(color);
        free(distance);
    }

    return shortestPath;
}

/** Verifica se il grafo in ingresso e' aciclico
  * @param graph il grafo che si vuole verificare
  * @return true se il grafo non ha cicli, false altrimenti
  */
bool graphIsAcyclicRec(void* graph) {
    GraphObject* graphObject = (GraphObject*) graph;
    bool ret = true;

    // Dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices) && ret) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 'b') {
            ret = isAcyclicRecSupport(graphObject, color, *index);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    free(color);

    return ret;
}

bool isAcyclicRecSupport(GraphObject* graph, char* color, uint index) {
    // Coloro il nodo di grigio
    color[index] = 'g';
    bool isAcyclic = true;

    // Scendo ricorsivamente ai suoi adiacenti
    GraphVertex* vertex = graphVertexFromPointer(graph, &index);
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter) && isAcyclic) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 'b') {
            isAcyclic = isAcyclicRecSupport(graph, color, *innerIndex);
        }
        else if(color[*innerIndex] == 'g') {
            isAcyclic = false;
        }

        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);

    // Coloro il vertice di nero in modo da garantirmi che questo percorso e' terminato
    color[index] = 'n';

    return isAcyclic;
}

/** Ritorna un iteratore per l'ordine topologico
  * @param graph il grafo di cui si vuole ottenere l'ordinamento topologico
  * @assert Non e' possibile fare ordinamento topologico di un grafo ciclico
  * @return ITRObject l'iteratore per l'ordine topologico
  */
void* graphTopologicalOrderRec(void* graph) {
    assert(graphIsAcyclicRec(graph));
    GraphObject* graphObject = (GraphObject*) graph;

    StackType* stackType = ConstructStackVecType();
    StackObject* stack = stkConstruct(stackType);
    DataType* intType = ConstructIntDataType();

    // Dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 'b') {
            stack = topologicalOrderRecSupport(graphObject, stack, color, *index, intType);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    free(color);

    // Costruisco e ritorno l'iteratore per lo stack che contiene in ordine topologico i vertici
    ITRType* itrType = ConstructStackIterator();
    ITRObject* iterator = itrConstruct(itrType, stack);
    return iterator;
}

StackObject* topologicalOrderRecSupport(GraphObject* graph, StackObject* stack, char* color, uint index, DataType* intType) {
    // Coloro il nodo di grigio
    color[index] = 'g';

    // Scendo ricorsivamente ai suoi adiacenti
    GraphVertex* vertex = graphVertexFromPointer(graph, &index);
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 'b') {
            stack  = topologicalOrderRecSupport(graph, stack, color, *innerIndex, intType);
        }
        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);

    // Coloro il vertice di nero in modo da garantirmi che questo percorso e' terminato
    color[index] = 'n';

    // Inserisco questo vertice in modo post-order nello stack
    DataObject* ptr = adtConstruct(intType);
    adtSetValue(ptr, &index);
    stkPush(stack, ptr);
    adtDestruct(ptr);

    return stack;
}

/** Ritorna il massimo sottografo raggiungibile da quel vertice
  * @param graph il grafo di cui si vuole ottenere il sottografo
  * @param vertexName il vertice di partenza
  * @return subGraph il sottografo raggiungibile da vertexName
  */
void* graphMaximalReachableSubgraphRec(void* graph, int vertexName) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Creo il grafo
    GraphObject* subGraph = graphConstruct(graphObject->type);

    // Mi scorro i vari vertici per verificare che esista il vertice
    ITRObject* vertices = graphVertices(graph);
    uint vertexId = -1;
    bool found = false;

    while(!itrTerminated(vertices) && !found) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graph, index);

        // Se vertexName coincide con quello attuale, allora mi prendo il suo index
        if(vertexName == vertex->name) {
            vertexId = *index;
            found = true;
        }

        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    // Se non l'ho trovato ritorno banalmente un grafo vuoto
    if(!found) {
        return subGraph;
    }

    // Se arrivo qui allora l'ho trovato, dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    GraphVertex *vertex = graphVertexFromPointer(graph, &vertexId);

    // Inserisco nel sottografo il primo elemento
    DataObject* clone = adtClone(vertex->dataObject);
    graphInsertVertex(subGraph, clone, vertex->name);
    adtDestruct(clone);

    //Richiamo la DFS ricorsiva per riempire man mano il subGraph
    subGraph = maximalReachableSubGraphRecSupport(graphObject, subGraph, color, vertexId);

    free(color);

    return subGraph;
}

void* maximalReachableSubGraphRecSupport(GraphObject* graph, GraphObject* subGraph, char* color, uint index) {
    // Coloro il nodo di grigio
    color[index] = 'g';

    // Scendo ricorsivamente ai suoi adiacenti
    GraphVertex* vertex = graphVertexFromPointer(graph, &index);
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);
        GraphVertex* innerVertex = graphVertexFromPointer(graph, innerIndex);

        // Se il colore e' bianco allora procedo ad aggiungerlo e richiamarmi ricorsivamente
        if(color[*innerIndex] == 'b') {
            DataObject* clone = adtClone(innerVertex->dataObject);
            graphInsertVertex(subGraph, clone, innerVertex->name);
            graphInsertEdge(subGraph, vertex->name, innerVertex->name);
            adtDestruct(clone);

            subGraph = maximalReachableSubGraphRecSupport(graph, subGraph, color, *innerIndex);
        }

        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);

    // Coloro il vertice di nero in modo da garantirmi che questo percorso e' terminato
    color[index] = 'n';

    return subGraph;
}

/** Ritorna il grafo delle componenti fortemente connesse
  * @param graph il grafo di cui si vuole ottenere il CFCGraph
  * @return SCCGraph il sottografo delle componenti
  */
void* graphSCCGraphRec(void* graph) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Creo il nuovo grafo delle CFC
    GraphObject* SCCGraph = graphConstruct(graphObject->type);

    /* Dichiaro l'array dei colori, questa volta come int* in quanto possono esserci più
     * "colori" in quanto ogni vertice sarà identificato dal suo rappresentante */
    int* color = (int*) calloc(graphObject->numberOfVertex, sizeof(int));

    // Creo lo stack per memorizzare i vari nodi della DFS1
    StackType* stackType = ConstructStackVecType();
    StackObject* stack = stkConstruct(stackType);
    DataType* intType = ConstructIntDataType();

    // Scorro i vari vertici del grafo con la DFS1 e li memorizzo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graphObject, index);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 0) {
            stack = DFS1_SCCGGraph(graphObject, color, *index, stack, intType, vertex->name);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    GraphObject* transpose = graphTranspose(graphObject);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 0;
    }

    // Ciclo sugli elementi dello stack
    while(!stkEmpty(stack)) {
        DataObject* tmp = stkTopNPop(stack);
        uint* index = adtGetValue(tmp);
        GraphVertex* vertex = graphVertexFromPointer(graphObject, index);

        // Se il colore e' bianco allora lo aggiungo a SCCGraph e lo visito
        if(color[*index] == 0) {
            graphInsertVertex(SCCGraph, vertex->dataObject, vertex->name);
            DFS2_SCCGGraph(transpose, color, *index, vertex->name, SCCGraph);
        }
        free(index);
        adtDestruct(tmp);
    }

    return SCCGraph;
}

// Costruisce uno stack dei vari vertici in post-order
StackObject* DFS1_SCCGGraph(GraphObject* graph, int* color, uint index, StackObject* stack, DataType* intType, int reprColor) {
    // Coloro il nodo con il rappresentante
    color[index] = reprColor;

    // Scendo ricorsivamente ai suoi adiacenti
    GraphVertex* vertex = graphVertexFromPointer(graph, &index);
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 0) {
            stack  = DFS1_SCCGGraph(graph, color, *innerIndex, stack, intType, reprColor);
        }
        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);

    // Inserisco questo vertice in modo post-order nello stack
    DataObject* newElem = adtConstruct(intType);
    adtSetValue(newElem, &index);
    stkPush(stack, newElem);
    adtDestruct(newElem);

    return stack;
}

void DFS2_SCCGGraph(GraphObject* graph, int* color, uint index, int reprColor, GraphObject* newGraph) {
    // Coloro il nodo con il rappresentante
    color[index] = reprColor;

    // Scendo ricorsivamente ai suoi adiacenti
    GraphVertex* vertex = graphVertexFromPointer(graph, &index);
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 0) {
            DFS2_SCCGGraph(graph, color, *innerIndex, reprColor, newGraph);
        }
        // Se non è bianco devo aggiungere un arco
        else if (color[*innerIndex] != reprColor){
            graphInsertEdge(newGraph, color[*innerIndex], reprColor);
        }
        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);
}

void graphPreOrderMapRec(void* graph, MapFun mapFunction, void* parameter) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 'b') {
            DFSVisitPreOrderMap(graphObject, color, *index, mapFunction, parameter);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    free(color);
}

void DFSVisitPreOrderMap(GraphObject* graph, char* color, uint index, MapFun mapFunction, void* parameter) {
    color[index] = 'g';

    GraphVertex* vertex = graphVertexFromPointer(graph, &index);

    // Visito l'elemento corrente del grafo
    mapFunction(vertex->dataObject, parameter);

    // Scendo ricorsivamente ai suoi adiacenti
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 'b') {
            DFSVisitPreOrderMap(graph, color, *innerIndex, mapFunction, parameter);
        }
        itrSuccessor(adjIter);
    }

    destructIteratorAndType(adjIter);
}

void graphPostOrderMapRec(void* graph, MapFun mapFunction, void* parameter) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 'b') {
            DFSVisitPostOrderMap(graphObject, color, *index, mapFunction, parameter);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    free(color);
}

void DFSVisitPostOrderMap(GraphObject* graph, char* color, uint index, MapFun mapFunction, void* parameter) {
    color[index] = 'g';
    GraphVertex* vertex = graphVertexFromPointer(graph, &index);

    // Scendo ricorsivamente ai suoi adiacenti
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 'b') {
            DFSVisitPostOrderMap(graph, color, *innerIndex, mapFunction, parameter);
        }
        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);

    // Visito l'elemento corrente del grafo
    mapFunction(vertex->dataObject, parameter);
}

void graphBreadthMapRec(void* graph, MapFun mapFunction, void* parameter) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Dichiaro l'array dei colori e la coda
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);
    DataType* intType = ConstructIntDataType();
    DataObject* newElem = adtConstruct(intType);
    QueueType* queueType = ConstructQueueVecType();
    QueueObject* queue = queConstruct(queueType);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*index] == 'b') {
            adtSetValue(newElem, index);
            queEnqueue(queue, newElem);
            color[*index] = 'g';

            // Effettuo la BFS
            while(!queEmpty(queue)) {
                // Prendo il vertice dalla coda e lo assegno a currentVertex
                DataObject* name = queHeadNDequeue(queue);
                uint* currIndex = adtGetValue(name);
                GraphVertex* vertex = graphVertexFromPointer(graph, currIndex);

                mapFunction(vertex->dataObject, parameter);

                // Imposto che il suo colore e' ora nero, siccome l'ho raggiunto
                color[*currIndex] = 'n';

                // Scorro tutti gli adiacenti di questo vertice e li accodo se bianchi
                ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
                while(!itrTerminated(adjIter)) {
                    uint* innerId = itrElement(adjIter);

                    if(color[*innerId] == 'b') {
                        adtSetValue(newElem, innerId);
                        queEnqueue(queue, newElem);
                        color[*innerId] = 'g';
                    }
                    itrSuccessor(adjIter);
                }
                destructIteratorAndType(adjIter);

                adtDestruct(name);
                free(currIndex);
            }
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    queDestruct(queue);
    DestructQueueVecType(queueType);
    adtDestruct(newElem);
    DestructIntDataType(intType);
    free(color);
}

void graphPreOrderFoldRec(void* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 'b') {
            DFSVisitPreOrderFold(graphObject, color, *index, foldFunction, accumulator, parameter);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    free(color);
}

void DFSVisitPreOrderFold(GraphObject* graph, char* color, uint index, FoldFun foldFunction, void* accumulator, void* parameter) {
    color[index] = 'g';

    GraphVertex* vertex = graphVertexFromPointer(graph, &index);

    // Visito l'elemento corrente del grafo
    foldFunction(vertex->dataObject, accumulator, parameter);

    // Scendo ricorsivamente ai suoi adiacenti
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 'b') {
            DFSVisitPreOrderFold(graph, color, *innerIndex, foldFunction, accumulator, parameter);
        }
        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);
}

void graphPostOrderFoldRec(void* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 'b') {
            DFSVisitPostOrderFold(graphObject, color, *index, foldFunction, accumulator, parameter);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    free(color);
}

void DFSVisitPostOrderFold(GraphObject* graph, char* color, uint index, FoldFun foldFunction, void* accumulator, void* parameter) {
    color[index] = 'g';
    GraphVertex* vertex = graphVertexFromPointer(graph, &index);

    // Scendo ricorsivamente ai suoi adiacenti
    ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
    while(!itrTerminated(adjIter)) {
        uint* innerIndex = itrElement(adjIter);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*innerIndex] == 'b') {
            DFSVisitPostOrderFold(graph, color, *innerIndex, foldFunction, accumulator, parameter);
        }
        itrSuccessor(adjIter);
    }
    destructIteratorAndType(adjIter);

    // Visito l'elemento corrente del grafo
    foldFunction(vertex->dataObject, accumulator, parameter);
}

void graphBreadthFoldRec(void* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Dichiaro l'array dei colori e la coda
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);
    DataType* intType = ConstructIntDataType();
    DataObject* newElem = adtConstruct(intType);
    QueueType* queueType = ConstructQueueVecType();
    QueueObject* queue = queConstruct(queueType);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Prendo il primo vertice
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        // Se il colore e' bianco allora procedo a prenderlo e visitarlo
        if(color[*index] == 'b') {
            adtSetValue(newElem, index);
            queEnqueue(queue, newElem);
            color[*index] = 'g';

            // Effettuo la BFS
            while(!queEmpty(queue)) {
                // Prendo il vertice dalla coda e lo assegno a currentVertex
                DataObject* name = queHeadNDequeue(queue);
                uint* currIndex = adtGetValue(name);
                GraphVertex* vertex = graphVertexFromPointer(graph, currIndex);

                foldFunction(vertex->dataObject, accumulator, parameter);

                // Imposto che il suo colore e' ora nero, siccome l'ho raggiunto
                color[*currIndex] = 'n';

                // Scorro tutti gli adiacenti di questo vertice e li accodo se bianchi
                ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
                while(!itrTerminated(adjIter)) {
                    uint* innerId = itrElement(adjIter);

                    if(color[*innerId] == 'b') {
                        adtSetValue(newElem, innerId);
                        queEnqueue(queue, newElem);
                        color[*innerId] = 'g';
                    }
                    itrSuccessor(adjIter);
                }
                destructIteratorAndType(adjIter);
                adtDestruct(name);
                free(currIndex);
            }
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    queDestruct(queue);
    DestructQueueVecType(queueType);
    adtDestruct(newElem);
    DestructIntDataType(intType);
    free(color);
}