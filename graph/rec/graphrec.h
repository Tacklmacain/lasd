
#ifndef GRAPHREC_H
#define GRAPHREC_H

/* ************************************************************************** */

#include "../../stack/stack.h"
#include "../../stack/vec/stackvec.h"
#include "../../stack/stackitr.h"
#include "../graph.h"

/* ************************************************************************** */

GraphType* ConstructGraphRecursive(GraphRepresentation*);
void DestructGraphRecursive(GraphType*);

bool graphEqualRec(void*, void*);

int graphShortestPathRec(void*, int, int);

bool graphIsAcyclicRec(void*);

void* graphTopologicalOrderRec(void*);

void* graphMaximalReachableSubgraphRec(void*, int indice);

void* graphSCCGraphRec(void*);

void graphPreOrderMapRec(void*, MapFun, void*);
void graphPostOrderMapRec(void*, MapFun, void*);
void graphBreadthMapRec(void*, MapFun, void*);
void graphPreOrderFoldRec(void*, FoldFun, void*, void*);
void graphPostOrderFoldRec(void*, FoldFun, void*, void*);
void graphBreadthFoldRec(void*, FoldFun, void*, void*);

/* ************************************************************************** */

#endif
