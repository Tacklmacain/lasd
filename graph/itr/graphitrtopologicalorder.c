#include "graphitrtopologicalorder.h"

/* ************************************************************************** */

ITRType* ConstructGraphTopologicalItr() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &itrConstructGraphTopologicalItr;
    type->destruct = &itrDestructGraphTopologicalItr;
    type->isTerminated = &itrTerminatedGraphTopologicalItr;
    type->element = &itrElementGraphTopologicalItr;
    type->successor = &itrSuccessorGraphTopologicalItr;

    return type;
}

void DestructGraphTopologicalItr(ITRType* type) {
    free(type);
}

void* itrConstructGraphTopologicalItr(void* graph) {
    GraphTopologicalIterator* iter = (GraphTopologicalIterator*) malloc(sizeof(GraphTopologicalIterator));
    GraphObject* graphObject = (GraphObject*) graph;

    iter->graph = graphObject;

    // Alloco l'array dei gradi entranti e lo inizializzo a 0 con la calloc
    iter->incomingGrade = (int*) calloc(graphObject->numberOfVertex, sizeof(int));

    // Calcolo il grado entrante per ogni vertice
    ITRObject* vertices = graphVertices(graph);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graphObject, index);

        // Aumento il grado di ogni vertice adiacente a questo
        ITRObject* adjIter = graphVertexEdges(graph, vertex->name);
        while(!itrTerminated(adjIter)) {
            uint* innerIndex = itrElement(adjIter);
            iter->incomingGrade[*innerIndex]++;
            itrSuccessor(adjIter);
        }
        destructIteratorAndType(adjIter);
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    // Creo la coda che conterrà i vari elementi con grado 0
    QueueType* type = ConstructQueueVecType();
    iter->queue = queConstruct(type);

    // Creo il tipo intero condiviso tra gli elementi della coda
    iter->intType = ConstructIntDataType();
    DataObject* newElem = adtConstruct(iter->intType);

    // Inserisco in coda i vertici con grado 0
    vertices = graphVertices(graph);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);

        if(iter->incomingGrade[*index] == 0) {
            adtSetValue(newElem, index);
            queEnqueue(iter->queue, newElem);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    // Se la coda non è vuota, procedo a mettere l'elemento in current e decrementare i gradi
    if(!queEmpty(iter->queue)) {
        // Prendo l'elemento dalla coda e lo converto in vertice
        DataObject* tmp = queHeadNDequeue(iter->queue);
        uint* vertexId = adtGetValue(tmp);
        iter->current = graphVertexFromPointer(iter->graph, vertexId);
        free(vertexId);
        adtDestruct(tmp);

        // Decremento tutti gli archi entranti degli adiacenti di questo vertice
        ITRObject* adjIter = graphVertexEdges(graph, iter->current->name);
        while(!itrTerminated(adjIter)) {
            uint* innerVertex = itrElement(adjIter);

            // Se decrementando il vertice corrente diventa 0 lo accodo
            if(--iter->incomingGrade[*innerVertex] == 0) {
                adtSetValue(newElem, innerVertex);
                queEnqueue(iter->queue, newElem);
            }
            itrSuccessor(adjIter);
        }
        destructIteratorAndType(adjIter);
    }
    else {
        // Se la coda è vuota, current è NULL
        iter->current = NULL;
    }

    adtDestruct(newElem);
    return iter;
}

void itrDestructGraphTopologicalItr(void* iterator) {
    GraphTopologicalIterator* iter = (GraphTopologicalIterator*) iterator;
    QueueType* type = iter->queue->type;

    queClear(iter->queue);
    queDestruct(iter->queue);
    DestructQueueVecType(type);
    DestructIntDataType(iter->intType);
    free(iter->incomingGrade);
    free(iter);
}

bool itrTerminatedGraphTopologicalItr(void* iterator) {
    return ((GraphTopologicalIterator*) iterator)->current == NULL;
}

void* itrElementGraphTopologicalItr(void* iterator) {
    return ((GraphTopologicalIterator*) iterator)->current;
}

void itrSuccessorGraphTopologicalItr(void* iterator) {
    GraphTopologicalIterator* iter = (GraphTopologicalIterator*) iterator;

    // Se la coda non è vuota, procedo ad assegnare l'elemento in current e decrementare i gradi
    if(!queEmpty(iter->queue)) {
        DataObject* newElem = adtConstruct(iter->intType);

        // Prendo l'elemento dalla coda e lo converto in vertice
        DataObject* tmp = queHeadNDequeue(iter->queue);
        uint* vertexId = adtGetValue(tmp);
        iter->current = graphVertexFromPointer(iter->graph, vertexId);
        free(vertexId);
        adtDestruct(tmp);

        // Decremento tutti gli archi entranti degli adiacenti di questo vertice
        ITRObject* adjIter = graphVertexEdges(iter->graph, iter->current->name);
        while(!itrTerminated(adjIter)) {
            uint* innerVertex = itrElement(adjIter);

            // Se decrementando il vertice corrente diventa 0 lo accodo
            if(--iter->incomingGrade[*innerVertex] == 0) {
                adtSetValue(newElem, innerVertex);
                queEnqueue(iter->queue, newElem);
            }
            itrSuccessor(adjIter);
        }
        destructIteratorAndType(adjIter);
        adtDestruct(newElem);
    }
    else {
        iter->current = NULL;
    }
}