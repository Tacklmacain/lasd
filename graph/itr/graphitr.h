
#ifndef GRAPHITR_H
#define GRAPHITR_H

/* ************************************************************************** */

#include "../../stack/stack.h"
#include "../../stack/vec/stackvec.h"
#include "../graphitrbreadth.h"
#include "../graphitrpreord.h"
#include "../graphitrpostord.h"
#include "../graph.h"
#include "graphitrtopologicalorder.h"

/* ************************************************************************** */

GraphType* ConstructGraphIterative(GraphRepresentation*);
void DestructGraphIterative(GraphType*);

bool graphEqualItr(void*, void*);

int graphShortestPathItr(void*, int, int);

bool graphIsAcyclicItr(void*);

void* graphTopologicalOrderItr(void*);

void* graphMaximalReachableSubgraphItr(void*, int indice);

void* graphSCCGraphItr(void*);

void graphPreOrderMapItr(void*, MapFun, void*);
void graphPostOrderMapItr(void*, MapFun, void*);
void graphBreadthMapItr(void*, MapFun, void*);
void graphPreOrderFoldItr(void*, FoldFun, void*, void*);
void graphPostOrderFoldItr(void*, FoldFun, void*, void*);
void graphBreadthFoldItr(void*, FoldFun, void*, void*);

/* ************************************************************************** */

#endif
