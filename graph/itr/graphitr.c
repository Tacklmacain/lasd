#include "graphitr.h"

/* ************************************************************************** */

// Prototipi di funzioni di supporto iterative
bool isAcyclicIterSupport(GraphObject*, char*, GraphVertex*, uint);
StackObject* DFS1_SCCGGraphItr(GraphObject*, int*, uint, GraphVertex*, StackObject*, DataType*, int);
void DFS2_SCCGGraphItr(GraphObject*, int*, uint, GraphVertex*, int, GraphObject*);

/* ************************************************************************** */

GraphType* ConstructGraphIterative(GraphRepresentation* representation) {
    GraphType* type = (GraphType*) malloc(sizeof(GraphType));

    type->representation = representation;
    type->equal = graphEqualItr;
    type->shortestPath = graphShortestPathItr;
    type->isAcyclic = graphIsAcyclicItr;
    type->topologicalOrder = graphTopologicalOrderItr;
    type->maximalReachableSubgraph = graphMaximalReachableSubgraphItr;
    type->sccGraph = graphSCCGraphItr;
    type->preOrderMap = graphPreOrderMapItr;
    type->postOrderMap = graphPostOrderMapItr;
    type->breadthMap = graphBreadthMapItr;
    type->preOrderFold = graphPreOrderFoldItr;
    type->postOrderFold = graphPostOrderFoldItr;
    type->breadthFold = graphBreadthFoldItr;

    return type;
}

void DestructGraphIterative(GraphType* type) {
    free(type);
}

/** Verifica se due grafi in ingresso sono uguali in struttura ed in elementi
  * @param graph1 il primo grafo
  * @param graph2 il secondo grafo
  * @return true se sono identici, false altrimenti
  */
bool graphEqualItr(void* graph1, void* graph2) {
    ITRObject *vertices1 = graphVertices(graph1), *vertices2 = graphVertices(graph2);
    bool areEqual = true;

    /* Siccome so che hanno lo stesso numero di vertici, posso scorrere
     * sfruttando anche solo uno dei due iteratori evitando un controllo in più */
    while(!itrTerminated(vertices1) && areEqual) {
        uint *index1 = itrElement(vertices1), *index2 = itrElement(vertices2);

        GraphVertex* vertex1 = graphVertexFromPointer(graph1, index1);
        GraphVertex* vertex2 = graphVertexFromPointer(graph2, index2);

        // Se i vertici attuali hanno DataObject o nome diverso, setto equal a false ed esco
        if(adtCompare(vertex1->dataObject, vertex2->dataObject) != 0 || vertex1->name != vertex2->name) {
            areEqual = false;
        }
        else {
            // Dichiaro l'iteratore per gli adiacenti di questo vertice
            ITRObject* adjIterator1 = graphVertexEdges(graph1, vertex1->name);
            ITRObject* adjIterator2 = graphVertexEdges(graph2, vertex2->name);

            while(!itrTerminated(adjIterator1) && !itrTerminated(adjIterator2) && areEqual) {
                void *innerId1 = itrElement(adjIterator1), *innerId2 = itrElement(adjIterator2);

                GraphVertex* innerVertex1 = graphVertexFromPointer(graph1, innerId1);
                GraphVertex* innerVertex2 = graphVertexFromPointer(graph2, innerId2);

                // Se sono diversi setto equal a false ed esco, altrimenti continuo ad iterare
                if(adtCompare(innerVertex1->dataObject, innerVertex2->dataObject) != 0 || innerVertex1->name != innerVertex2->name) {
                    areEqual = false;
                }
                else {
                    itrSuccessor(adjIterator1);
                    itrSuccessor(adjIterator2);
                }
            }

            // Verifico che siano entrambi finiti, se uno dei due soltanto è finito allora ritorno false
            if(!itrTerminated(adjIterator1) != !itrTerminated(adjIterator2)) {
                areEqual = false;
            }

            // Libero i type degli iteratori utilizzati, li distruggo e distruggo gli iteratori
            destructIteratorAndType(adjIterator1);
            destructIteratorAndType(adjIterator2);

            itrSuccessor(vertices1);
            itrSuccessor(vertices2);
        }
    }
    // Libero i type degli iteratori utilizzati, li distruggo e distruggo gli iteratori
    destructIteratorAndType(vertices1);
    destructIteratorAndType(vertices2);

    return areEqual;
}

/** Ritorna la distanza minima tra un vertice ed un altro
  * @param graph il grafo che contiene i due vertici
  * @param start il vertice di partenza
  * @param arrival il vertice di destinazione
  * @return la distanza fra i due vertici, -1 se non esiste un percorso tra di loro
  */
int graphShortestPathItr(void* graph, int start, int arrival) {
    GraphObject* graphObject = (GraphObject*) graph;
    int shortestPath = -1;

    // Mi scorro i vari vertici per verificare che esistano entrambi
    ITRObject* vertices = graphVertices(graph);
    uint startId = -1, arrivalId = -1;
    bool foundStart = false, foundArrival = false;

    while(!itrTerminated(vertices) && (!foundStart || !foundArrival)) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graph, index);

        // Se start o arrival esistono, allora mi prendo il loro index
        if(start == vertex->name) {
            startId = *index;
            foundStart = true;
        }

        if(arrival == vertex->name) {
            arrivalId = *index;
            foundArrival = true;
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    /* Se entrambi sono falsi, oppure solo uno dei due è falso, ritorno -1 */
    if((!foundStart && !foundArrival) || foundStart != foundArrival) {
        shortestPath = -1;
    }
    // Altrimenti se sono lo stesso vertice il percorso è 0
    else if(startId == arrivalId) {
        shortestPath = 0;
    }
    // Altrimenti potrebbe esistere un percorso
    else {

        // Dichiaro l'array dei colori, array delle distanze e la coda
        char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);
        int* distance = (int*) malloc(sizeof(int) * graphObject->numberOfVertex);
        DataType* intType = ConstructIntDataType();
        DataObject* newElem = adtConstruct(intType);
        QueueType* queueType = ConstructQueueVecType();
        QueueObject* queue = queConstruct(queueType);

        // Inizializzo i colori a bianco e le distanze a 0
        for (int i = 0; i < graphObject->numberOfVertex; ++i) {
            color[i] = 'b';
            distance[i] = -1;
        }

        /* Imposto che il colore grigio e la distanza 0 del nodo iniziale
         * e lo accodo */
        color[startId] = 'g';
        distance[startId] = 0;
        adtSetValue(newElem, &startId);
        queEnqueue(queue, newElem);

        // Effettuo la BFS, la interrompo se trovo l'elemento
        bool found = false;
        while(!queEmpty(queue) && !found) {
            DataObject *name = queHeadNDequeue(queue);
            uint *currIndex = adtGetValue(name);
            GraphVertex *vertex = graphVertexFromPointer(graph, currIndex);

            color[*currIndex] = 'n';

            // Scorro tutti gli adiacenti di questo vertice e li accodo se bianchi
            ITRObject *adjIter = graphVertexEdges(graph, vertex->name);
            while (!itrTerminated(adjIter) && !found) {
                uint *innerId = itrElement(adjIter);

                if (color[*innerId] == 'b') {
                    adtSetValue(newElem, innerId);
                    queEnqueue(queue, newElem);
                    color[*innerId] = 'g';
                    distance[*innerId] = distance[*currIndex] + 1;

                    if(*innerId == arrivalId) {
                        shortestPath = distance[arrivalId];
                        found = true;
                    }
                }
                itrSuccessor(adjIter);
            }
            destructIteratorAndType(adjIter);

            adtDestruct(name);
            free(currIndex);
        }

        queClear(queue);
        queDestruct(queue);
        DestructQueueVecType(queueType);
        adtDestruct(newElem);
        DestructIntDataType(intType);
        free(color);
        free(distance);
    }

    return shortestPath;
}

/** Verifica se il grafo in ingresso è aciclico
  * @param graph il grafo che si vuole verificare
  * @return true se il grafo non ha cicli, false altrimenti
  */
bool graphIsAcyclicItr(void* graph) {
    GraphObject* graphObject = (GraphObject*) graph;
    bool ret = true;

    // Dichiaro l'array dei colori
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    // Scorro i vari vertici del grafo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices) && ret) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graphObject, index);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 'b') {
            ret = isAcyclicIterSupport(graphObject, color, vertex, *index);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);
    free(color);

    return ret;
}

bool isAcyclicIterSupport(GraphObject* graphObject, char* color, GraphVertex* vertex, uint index) {
    bool isAcyclic = true;

    StackType* stackType = ConstructStackVecType();
    StackObject* stackVertices = stkConstruct(stackType);
    StackObject* stackIndexes = stkConstruct(stackType);
    StackObject* stackAdjacent = stkConstruct(stackType);

    DataType* ptrType = ConstructPtrDataType();
    DataObject* newPtr = adtConstruct(ptrType);

    GraphVertex *currentVertex, *nextVertex;
    uint* currentIndex;

    // Inserisco negli stack il primo vertice ed il primo index nello stack
    adtSetValue(newPtr, vertex);
    stkPush(stackVertices, newPtr);

    adtSetValue(newPtr, &index);
    stkPush(stackIndexes, newPtr);

    // Ciclo finché non è finito lo stack oppure trovo un ciclo
    bool newCall = true;
    while(!stkEmpty(stackVertices) && isAcyclic) {
        DataObject* tmpVertex = stkTop(stackVertices);
        DataObject* tmpIndex = stkTop(stackIndexes);
        currentVertex = adtGetValue(tmpVertex);
        currentIndex = adtGetValue(tmpIndex);
        adtDestruct(tmpIndex);
        adtDestruct(tmpVertex);

        // Nuova chiamata ricorsiva
        if(newCall) {
            // Coloro il nodo di grigio
            color[*currentIndex] = 'g';

            // Scorro gli adiacenti di questo vertice
            ITRObject* adjIter = graphVertexEdges(graphObject, currentVertex->name);
            bool found = false;
            while(!found) {
                // Se l'iteratore non è terminato ho vertici da verificare
                if(!itrTerminated(adjIter)) {
                    uint* nextIndex = itrElement(adjIter);
                    nextVertex = graphVertexFromPointer(graphObject, nextIndex);

                    itrSuccessor(adjIter);

                    // Non ho ancora esplorato questo vertice e lo pusho
                    if(color[*nextIndex] == 'b') {
                        // Salvo l'iteratore ed il vertice
                        adtSetValue(newPtr, adjIter);
                        stkPush(stackAdjacent, newPtr);

                        adtSetValue(newPtr, nextVertex);
                        stkPush(stackVertices, newPtr);

                        adtSetValue(newPtr, nextIndex);
                        stkPush(stackIndexes, newPtr);
                        found = true;
                    }
                        // Se il colore è grigio, esiste un ciclo
                    else if(color[*nextIndex] == 'g') {
                        destructIteratorAndType(adjIter);
                        isAcyclic = false;
                        found = true;
                    }
                }
                else {
                    // Se l'iteratore è terminato elimino il vertice attuale e risalgo
                    color[*currentIndex] = 'n';
                    stkPop(stackVertices);
                    stkPop(stackIndexes);
                    destructIteratorAndType(adjIter);
                    newCall= false;
                    found = true;
                }
            }
        }
            // Ripresa del contesto
        else {
            DataObject* tmpAdj = stkTop(stackAdjacent);
            ITRObject* adjIter = adtGetValue(tmpAdj);
            adtDestruct(tmpAdj);

            bool found = false;
            while(!found) {
                // Se l'iteratore non è terminato ho vertici da verificare
                if(!itrTerminated(adjIter)) {
                    uint* nextIndex = itrElement(adjIter);
                    nextVertex = graphVertexFromPointer(graphObject, nextIndex);

                    itrSuccessor(adjIter);

                    // Non ho ancora esplorato questo vertice e lo pusho
                    if(color[*nextIndex] == 'b') {
                        adtSetValue(newPtr, nextVertex);
                        stkPush(stackVertices, newPtr);

                        adtSetValue(newPtr, nextIndex);
                        stkPush(stackIndexes, newPtr);

                        found = true;
                        newCall = true;
                    }
                        // Se il colore è grigio, esiste un ciclo
                    else if(color[*nextIndex] == 'g') {
                        isAcyclic = false;
                        found = true;
                    }
                }
                else {
                    // Coloro il vertice di nero e lo rimuovo dagli stack
                    color[*currentIndex] = 'n';
                    stkPop(stackVertices); stkPop(stackAdjacent); stkPop(stackIndexes);
                    destructIteratorAndType(adjIter);
                    found = true;
                }
            }
        }
    }

    while(!stkEmpty(stackAdjacent)) {
        DataObject* tmp = stkTopNPop(stackAdjacent);
        ITRObject* adjIter = adtGetValue(tmp);
        destructIteratorAndType(adjIter);
    }

    adtDestruct(newPtr);
    stkClear(stackAdjacent);
    stkClear(stackVertices);
    stkClear(stackIndexes);
    stkDestruct(stackAdjacent);
    stkDestruct(stackVertices);
    stkDestruct(stackIndexes);
    free(stackType);
    free(ptrType);

    return isAcyclic;
}

/** Ritorna un iteratore per l'ordine topologico
  * @param graph il grafo di cui si vuole ottenere l'ordinamento topologico
  * @assert Non è possibile fare ordinamento topologico di un grafo ciclico
  * @return ITRObject l'iteratore per l'ordine topologico
  */
void* graphTopologicalOrderItr(void* graph) {
    assert(graphIsAcyclicItr(graph));

    ITRType* itrType = ConstructGraphTopologicalItr();
    ITRObject* iterator = itrConstruct(itrType, graph);
    return iterator;
}

/** Ritorna il massimo sottografo raggiungibile da quel vertice
  * @param graph il grafo di cui si vuole ottenere il sottografo
  * @param vertexName il vertice di partenza
  * @return subGraph il sottografo raggiungibile da vertexName
  */
void* graphMaximalReachableSubgraphItr(void* graph, int vertexName) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Creo il grafo
    GraphObject* subGraph = graphConstruct(graphObject->type);

    // Mi scorro i vari vertici per verificare che esista quello in ingresso
    ITRObject* vertices = graphVertices(graph);
    uint vertexId = -1;
    bool found = false;

    while(!itrTerminated(vertices) && !found) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graph, index);

        // Se coincide il nome allora l'ho trovato
        if(vertexName == vertex->name) {
            vertexId = *index;
            found = true;
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    // Se non l'ho trovato ritorno banalmente un grafo vuoto
    if(!found) {
        return subGraph;
    }

    // Se l'ho trovato dichiaro l'array dei colori e la coda
    char* color = (char*) malloc(sizeof(char) * graphObject->numberOfVertex);
    DataType* intType = ConstructIntDataType();
    DataObject* queueElem = adtConstruct(intType);
    QueueType* queueType = ConstructQueueVecType();
    QueueObject* queue = queConstruct(queueType);

    // Inizializzo i colori a bianco e le distanze a 0
    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 'b';
    }

    GraphVertex *vertex = graphVertexFromPointer(graph, &vertexId);

    /* Imposto che il colore grigio, lo accodo e aggiungo al sottografo */
    color[vertexId] = 'g';
    adtSetValue(queueElem, &vertexId);
    queEnqueue(queue, queueElem);

    // Inserisco nel sottografo il primo elemento
    DataObject* clone = adtClone(vertex->dataObject);
    graphInsertVertex(subGraph, clone, vertex->name);
    adtDestruct(clone);

    // Effettuo la BFS per aggiungere man mano i vari elementi
    while(!queEmpty(queue)) {
        DataObject *name = queHeadNDequeue(queue);
        uint *currIndex = adtGetValue(name);
        vertex = graphVertexFromPointer(graph, currIndex);

        color[*currIndex] = 'n';

        // Scorro tutti gli adiacenti di questo vertice e li accodo se bianchi
        ITRObject *adjIter = graphVertexEdges(graph, vertex->name);
        while (!itrTerminated(adjIter)) {
            uint *innerId = itrElement(adjIter);
            GraphVertex* innerVertex = graphVertexFromPointer(graph, innerId);

            // Se il colore è bianco posso aggiungerlo al sottografo
            if (color[*innerId] == 'b') {
                clone = adtClone(innerVertex->dataObject);
                graphInsertVertex(subGraph, clone, innerVertex->name);
                graphInsertEdge(subGraph, vertex->name, innerVertex->name);
                adtDestruct(clone);

                adtSetValue(queueElem, innerId);
                queEnqueue(queue, queueElem);
                color[*innerId] = 'g';
            }
            itrSuccessor(adjIter);
        }
        destructIteratorAndType(adjIter);

        adtDestruct(name);
        free(currIndex);
    }

    queDestruct(queue);
    DestructQueueVecType(queueType);
    adtDestruct(queueElem);
    DestructIntDataType(intType);
    free(color);
    return subGraph;
}

/** Ritorna il grafo delle componenti fortemente connesse
  * @param graph il grafo di cui si vuole ottenere il CFCGraph
  * @return SCCGraph il sottografo delle componenti
  */
void* graphSCCGraphItr(void* graph) {
    GraphObject* graphObject = (GraphObject*) graph;

    // Creo il nuovo grafo delle CFC
    GraphObject* SCCGraph = graphConstruct(graphObject->type);

    /* Dichiaro l'array dei colori, questa volta come int* in quanto possono esserci più
     * "colori" in quanto ogni vertice sarà identificato dal suo rappresentante */
    int* color = (int*) calloc(graphObject->numberOfVertex, sizeof(int));

    // Creo lo stack per memorizzare i vari nodi della DFS1
    StackType* stackType = ConstructStackVecType();
    StackObject* stack = stkConstruct(stackType);
    DataType* intType = ConstructIntDataType();

    // Scorro i vari vertici del grafo con la DFS1 e li memorizzo
    ITRObject* vertices = graphVertices(graphObject);
    while(!itrTerminated(vertices)) {
        uint* index = itrElement(vertices);
        GraphVertex* vertex = graphVertexFromPointer(graphObject, index);

        // Se il colore e' bianco allora procedo a visitarlo
        if(color[*index] == 0) {
            stack = DFS1_SCCGGraphItr(graphObject, color, *index, vertex, stack, intType, vertex->name);
        }
        itrSuccessor(vertices);
    }
    destructIteratorAndType(vertices);

    GraphObject* transpose = graphTranspose(graphObject);

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        color[i] = 0;
    }

    // Ciclo sugli elementi dello stack
    while(!stkEmpty(stack)) {
        DataObject* tmp = stkTopNPop(stack);
        uint* index = adtGetValue(tmp);
        GraphVertex* vertex = graphVertexFromPointer(graphObject, index);

        // Se il colore e' bianco allora lo aggiungo a SCCGraph e lo visito
        if(color[*index] == 0) {
            graphInsertVertex(SCCGraph, vertex->dataObject, vertex->name);
            DFS2_SCCGGraphItr(transpose, color, *index, vertex, vertex->name, SCCGraph);
        }
        free(index);
        adtDestruct(tmp);
    }

    return SCCGraph;
}

StackObject* DFS1_SCCGGraphItr(GraphObject* graphObject, int* color, uint index, GraphVertex* vertex, StackObject* stack, DataType* intType, int reprColor) {
    StackType* stackType = ConstructStackVecType();
    StackObject* stackVertices = stkConstruct(stackType);
    StackObject* stackIndexes = stkConstruct(stackType);
    StackObject* stackAdjacent = stkConstruct(stackType);

    DataType* ptrType = ConstructPtrDataType();
    DataObject* newPtr = adtConstruct(ptrType);

    GraphVertex *currentVertex, *nextVertex;
    uint* currentIndex;

    // Inserisco negli stack il primo vertice ed il primo index nello stack
    adtSetValue(newPtr, vertex);
    stkPush(stackVertices, newPtr);

    adtSetValue(newPtr, &index);
    stkPush(stackIndexes, newPtr);

    // Ciclo finché non è finito lo stack oppure trovo un ciclo
    bool newCall = true;
    while(!stkEmpty(stackVertices)) {
        DataObject* tmpVertex = stkTop(stackVertices);
        DataObject* tmpIndex = stkTop(stackIndexes);
        currentVertex = adtGetValue(tmpVertex);
        currentIndex = adtGetValue(tmpIndex);
        adtDestruct(tmpIndex);
        adtDestruct(tmpVertex);

        // Nuova chiamata ricorsiva
        if(newCall) {
            // Coloro il nodo di grigio
            color[*currentIndex] = reprColor;

            // Scorro gli adiacenti di questo vertice
            ITRObject* adjIter = graphVertexEdges(graphObject, currentVertex->name);
            bool found = false;
            while(!found) {
                // Se l'iteratore non è terminato ho vertici da verificare
                if(!itrTerminated(adjIter)) {
                    uint* nextIndex = itrElement(adjIter);
                    nextVertex = graphVertexFromPointer(graphObject, nextIndex);

                    itrSuccessor(adjIter);

                    // Non ho ancora esplorato questo vertice e lo pusho
                    if(color[*nextIndex] == 0) {
                        // Salvo l'iteratore ed il vertice
                        adtSetValue(newPtr, adjIter);
                        stkPush(stackAdjacent, newPtr);

                        adtSetValue(newPtr, nextVertex);
                        stkPush(stackVertices, newPtr);

                        adtSetValue(newPtr, nextIndex);
                        stkPush(stackIndexes, newPtr);
                        found = true;
                    }
                }
                else {
                    // Se l'iteratore è terminato elimino il vertice attuale e risalgo
                    color[*currentIndex] = reprColor;
                    stkPop(stackVertices);
                    stkPop(stackIndexes);

                    DataObject* newElem = adtConstruct(intType);
                    adtSetValue(newElem, currentIndex);
                    stkPush(stack, newElem);
                    adtDestruct(newElem);

                    destructIteratorAndType(adjIter);
                    newCall= false;
                    found = true;
                }
            }
        }
            // Ripresa del contesto
        else {
            DataObject* tmpAdj = stkTop(stackAdjacent);
            ITRObject* adjIter = adtGetValue(tmpAdj);
            adtDestruct(tmpAdj);

            bool found = false;
            while(!found) {
                // Se l'iteratore non è terminato ho vertici da verificare
                if(!itrTerminated(adjIter)) {
                    uint* nextIndex = itrElement(adjIter);
                    nextVertex = graphVertexFromPointer(graphObject, nextIndex);

                    itrSuccessor(adjIter);

                    // Non ho ancora esplorato questo vertice e lo pusho
                    if(color[*nextIndex] == 0) {
                        adtSetValue(newPtr, nextVertex);
                        stkPush(stackVertices, newPtr);

                        adtSetValue(newPtr, nextIndex);
                        stkPush(stackIndexes, newPtr);

                        found = true;
                        newCall = true;
                    }
                }
                else {
                    // Coloro il vertice di nero e lo rimuovo dagli stack
                    color[*currentIndex] = reprColor;
                    stkPop(stackVertices);
                    stkPop(stackAdjacent);
                    stkPop(stackIndexes);

                    DataObject* newElem = adtConstruct(intType);
                    adtSetValue(newElem, currentIndex);
                    stkPush(stack, newElem);
                    adtDestruct(newElem);

                    destructIteratorAndType(adjIter);
                    found = true;
                }
            }
        }
    }

    while(!stkEmpty(stackAdjacent)) {
        DataObject* tmp = stkTopNPop(stackAdjacent);
        ITRObject* adjIter = adtGetValue(tmp);
        destructIteratorAndType(adjIter);
    }

    adtDestruct(newPtr);
    stkClear(stackAdjacent);
    stkClear(stackVertices);
    stkClear(stackIndexes);
    stkDestruct(stackAdjacent);
    stkDestruct(stackVertices);
    stkDestruct(stackIndexes);
    free(stackType);
    free(ptrType);

    return stack;
}

void DFS2_SCCGGraphItr(GraphObject* graphObject, int* color, uint index, GraphVertex* vertex, int reprColor, GraphObject* newGraph) {
    StackType* stackType = ConstructStackVecType();
    StackObject* stackVertices = stkConstruct(stackType);
    StackObject* stackIndexes = stkConstruct(stackType);
    StackObject* stackAdjacent = stkConstruct(stackType);

    DataType* ptrType = ConstructPtrDataType();
    DataObject* newPtr = adtConstruct(ptrType);

    GraphVertex *currentVertex, *nextVertex;
    uint* currentIndex;

    // Inserisco negli stack il primo vertice ed il primo index nello stack
    adtSetValue(newPtr, vertex);
    stkPush(stackVertices, newPtr);

    adtSetValue(newPtr, &index);
    stkPush(stackIndexes, newPtr);

    // Ciclo finché non è finito lo stack oppure trovo un ciclo
    bool newCall = true;
    while(!stkEmpty(stackVertices)) {
        DataObject* tmpVertex = stkTop(stackVertices);
        DataObject* tmpIndex = stkTop(stackIndexes);
        currentVertex = adtGetValue(tmpVertex);
        currentIndex = adtGetValue(tmpIndex);
        adtDestruct(tmpIndex);
        adtDestruct(tmpVertex);

        // Nuova chiamata ricorsiva
        if(newCall) {
            // Coloro il nodo di grigio
            color[*currentIndex] = reprColor;

            // Scorro gli adiacenti di questo vertice
            ITRObject* adjIter = graphVertexEdges(graphObject, currentVertex->name);
            bool found = false;
            while(!found) {
                // Se l'iteratore non è terminato ho vertici da verificare
                if(!itrTerminated(adjIter)) {
                    uint* nextIndex = itrElement(adjIter);
                    nextVertex = graphVertexFromPointer(graphObject, nextIndex);

                    itrSuccessor(adjIter);

                    // Non ho ancora esplorato questo vertice e lo pusho
                    if(color[*nextIndex] == 0) {
                        // Salvo l'iteratore ed il vertice
                        adtSetValue(newPtr, adjIter);
                        stkPush(stackAdjacent, newPtr);

                        adtSetValue(newPtr, nextVertex);
                        stkPush(stackVertices, newPtr);

                        adtSetValue(newPtr, nextIndex);
                        stkPush(stackIndexes, newPtr);
                        found = true;
                    }
                    else if (color[*nextIndex] != reprColor){
                        graphInsertEdge(newGraph, color[*nextIndex], reprColor);
                    }
                }
                else {
                    // Se l'iteratore è terminato elimino il vertice attuale e risalgo
                    color[*currentIndex] = reprColor;
                    stkPop(stackVertices);
                    stkPop(stackIndexes);
                    destructIteratorAndType(adjIter);
                    newCall= false;
                    found = true;
                }
            }
        }
            // Ripresa del contesto
        else {
            DataObject* tmpAdj = stkTop(stackAdjacent);
            ITRObject* adjIter = adtGetValue(tmpAdj);
            adtDestruct(tmpAdj);

            bool found = false;
            while(!found) {
                // Se l'iteratore non è terminato ho vertici da verificare
                if(!itrTerminated(adjIter)) {
                    uint* nextIndex = itrElement(adjIter);
                    nextVertex = graphVertexFromPointer(graphObject, nextIndex);

                    itrSuccessor(adjIter);

                    // Non ho ancora esplorato questo vertice e lo pusho
                    if(color[*nextIndex] == 0) {
                        adtSetValue(newPtr, nextVertex);
                        stkPush(stackVertices, newPtr);

                        adtSetValue(newPtr, nextIndex);
                        stkPush(stackIndexes, newPtr);

                        found = true;
                        newCall = true;
                    }
                    else if (color[*nextIndex] != reprColor){
                        graphInsertEdge(newGraph, color[*nextIndex], reprColor);
                    }
                }
                else {
                    // Coloro il vertice di nero e lo rimuovo dagli stack
                    color[*currentIndex] = reprColor;
                    stkPop(stackVertices);
                    stkPop(stackAdjacent);
                    stkPop(stackIndexes);

                    destructIteratorAndType(adjIter);
                    found = true;
                }
            }
        }
    }

    while(!stkEmpty(stackAdjacent)) {
        DataObject* tmp = stkTopNPop(stackAdjacent);
        ITRObject* adjIter = adtGetValue(tmp);
        destructIteratorAndType(adjIter);
    }

    adtDestruct(newPtr);
    stkClear(stackAdjacent);
    stkClear(stackVertices);
    stkClear(stackIndexes);
    stkDestruct(stackAdjacent);
    stkDestruct(stackVertices);
    stkDestruct(stackIndexes);
    free(stackType);
    free(ptrType);
}

void graphPreOrderMapItr(void* graph, MapFun mapFunction, void* parameter) {
    ITRType* preOrderType = ConstructGraphPreOrderIterator();
    ITRObject* iter = itrConstruct(preOrderType, graph);

    while(!itrTerminated(iter)) {
        DataObject* dataObject = itrElement(iter);
        mapFunction(dataObject, parameter);
        itrSuccessor(iter);
    }
    itrDestruct(iter);
    DestructGraphBreadthIterator(preOrderType);
}

void graphPostOrderMapItr(void* graph, MapFun mapFunction, void* parameter) {
    ITRType* preOrderType = ConstructGraphPostOrderIterator();
    ITRObject* iter = itrConstruct(preOrderType, graph);

    while(!itrTerminated(iter)) {
        DataObject* dataObject = itrElement(iter);
        mapFunction(dataObject, parameter);
        itrSuccessor(iter);
    }
    itrDestruct(iter);
    DestructGraphBreadthIterator(preOrderType);
}

void graphBreadthMapItr(void* graph, MapFun mapFunction, void* parameter) {
    ITRType* breadthType = ConstructGraphBreadthIterator();
    ITRObject* iter = itrConstruct(breadthType, graph);

    while(!itrTerminated(iter)) {
        DataObject* dataObject = itrElement(iter);
        mapFunction(dataObject, parameter);
        itrSuccessor(iter);
    }
    itrDestruct(iter);
    DestructGraphBreadthIterator(breadthType);
}

void graphPreOrderFoldItr(void* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    ITRType* preOrderType = ConstructGraphPreOrderIterator();
    ITRObject* iter = itrConstruct(preOrderType, graph);

    while(!itrTerminated(iter)) {
        DataObject* dataObject = itrElement(iter);
        foldFunction(dataObject, accumulator, parameter);
        itrSuccessor(iter);
    }
    itrDestruct(iter);
    DestructGraphBreadthIterator(preOrderType);
}

void graphPostOrderFoldItr(void* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    ITRType* preOrderType = ConstructGraphPostOrderIterator();
    ITRObject* iter = itrConstruct(preOrderType, graph);

    while(!itrTerminated(iter)) {
        DataObject* dataObject = itrElement(iter);
        foldFunction(dataObject, accumulator, parameter);
        itrSuccessor(iter);
    }
    itrDestruct(iter);
    DestructGraphBreadthIterator(preOrderType);
}

void graphBreadthFoldItr(void* graph, FoldFun foldFunction, void* accumulator, void* parameter) {
    ITRType* breadthType = ConstructGraphBreadthIterator();
    ITRObject* iter = itrConstruct(breadthType, graph);

    while(!itrTerminated(iter)) {
        DataObject* dataObject = itrElement(iter);
        foldFunction(dataObject, accumulator, parameter);
        itrSuccessor(iter);
    }
    itrDestruct(iter);
    DestructGraphBreadthIterator(breadthType);
}
