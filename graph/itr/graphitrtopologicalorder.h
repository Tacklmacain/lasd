#ifndef EXERCISE3_GRAPHITRTOPOLOGICALORDER_H
#define EXERCISE3_GRAPHITRTOPOLOGICALORDER_H

/* ************************************************************************** */

#include "../../itr/itr.h"
#include "../../queue/queue.h"
#include "../../queue/vec/queuevec.h"
#include "../../adt/adt.h"
#include "../../adt/ptr/adtptr.h"
#include "../graph.h"

/* ************************************************************************** */

typedef struct GraphTopologicalIterator {
    GraphObject* graph;
    QueueObject* queue;
    GraphVertex* current;
    int* incomingGrade;

    DataType* intType;
} GraphTopologicalIterator;

/* ************************************************************************** */

ITRType* ConstructGraphTopologicalItr();
void DestructGraphTopologicalItr(ITRType*);

void* itrConstructGraphTopologicalItr(void*);
void itrDestructGraphTopologicalItr(void*);
bool itrTerminatedGraphTopologicalItr(void*);
void* itrElementGraphTopologicalItr(void*);
void itrSuccessorGraphTopologicalItr(void*);

/* ************************************************************************** */

#endif