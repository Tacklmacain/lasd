#include "graphitrpreord.h"
#include "../stack/vec/stackvec.h"

/* ************************************************************************** */

ITRType* ConstructGraphPreOrderIterator() {
    ITRType* type = (ITRType*) malloc(sizeof(ITRType));

    type->construct = &GraphPreOrderIteratorConstruct;
    type->destruct = &GraphPreOrderIteratorDestruct;
    type->element = &GraphPreOrderIteratorElement;
    type->successor = &GraphPreOrderIteratorSuccessor;
    type->isTerminated = &GraphPreOrderIteratorTerminated;

    return type;
}

void DestructGraphPreOrderIterator(ITRType* type) {
    free(type);
}

void* GraphPreOrderIteratorConstruct(void* graph) {
    GraphPreOrderIterator* iter = (GraphPreOrderIterator*) malloc(sizeof(GraphPreOrderIterator));
    GraphObject* graphObject = (GraphObject*) graph;

    // Inizio a creare i 2 stack che mi serviranno
    StackType* stackType = ConstructStackVecType();
    StackObject* stackVertices = stkConstruct(stackType);
    StackObject* stackAdjacents = stkConstruct(stackType);
    iter->ptrType = ConstructPtrDataType();

    // Assegno il grafo, alloco i colori e li inizializzo
    iter->graph = graphObject;
    iter->color = (char*) malloc(graphObject->numberOfVertex * sizeof(char));

    for (int i = 0; i < graphObject->numberOfVertex; ++i) {
        iter->color[i] = 'b';
    }

    // Assegno un valore di default
    iter->currentVertex = NULL;

    // Creo l'iteratore dei vertici e prendo il primo
    iter->verticesIterator = graphVertices(graphObject);
    if(!itrTerminated(iter->verticesIterator)) {
        uint* index = itrElement(iter->verticesIterator);
        GraphVertex* vertex = graphVertexFromPointer(iter->graph, index);

        // Lo assegno come corrente e coloro di grigio
        iter->currentVertex = vertex;
        iter->color[*index] = 'g';

        // Costruisco l'iteratore dei suoi adiacenti
        ITRObject* adjIter = graphVertexEdges(graphObject, vertex->name);
        iter->currentAdjIterator = adjIter;
    }

    // Salvo i due stack
    iter->stackAdjacent = stackAdjacents;
    iter->stackVertices = stackVertices;

    return iter;
}

void GraphPreOrderIteratorDestruct(void* iterator) {
    GraphPreOrderIterator* iter = (GraphPreOrderIterator*) iterator;
    StackType* stackType = iter->stackVertices->type;

    // Distruggo l'iteratore ed il suo tipo
    destructIteratorAndType(iter->verticesIterator);
    destructIteratorAndType(iter->currentAdjIterator);

    // Se ho elementi nello stack degli adiacenti devo distruggerli man mano
    if(!stkEmpty(iter->stackAdjacent)) {
        DataObject* elem = stkTopNPop(iter->stackAdjacent);
        destructIteratorAndType(elem->value);
        adtDestruct(elem);
    }

    stkClear(iter->stackVertices);
    stkDestruct(iter->stackVertices);
    stkDestruct(iter->stackAdjacent);
    DestructStackVecType(stackType);
    DestructPtrDataType(iter->ptrType);
    free(iter->color);
    free(iter);
}

bool GraphPreOrderIteratorTerminated(void* iterator) {
    // Ritorno true solo se non ho un corrente
    return !((GraphPreOrderIterator*) iterator)->currentVertex;
}

void* GraphPreOrderIteratorElement(void* iterator) {
    return ((GraphPreOrderIterator*) iterator)->currentVertex->dataObject;
}

void GraphPreOrderIteratorSuccessor(void* iterator) {
    GraphPreOrderIterator* iter = (GraphPreOrderIterator*) iterator;
    DataObject* newPtr = adtConstruct(iter->ptrType);
    bool foundNext = false, foundAdj = true;

    // Itero finché non trovo un elemento
    while(!foundNext) {
        // Cerco un elemento tra gli adiacenti correnti, mi fermo quando lo trovo
        while (!itrTerminated(iter->currentAdjIterator) && !foundNext) {
            uint *innerIndex = itrElement(iter->currentAdjIterator);
            GraphVertex *innerVertex = graphVertexFromPointer(iter->graph, innerIndex);

            itrSuccessor(iter->currentAdjIterator);

            // Se il vertice è bianco lo salvo, salvo la lista adiacenti ed esco dal ciclo
            if (iter->color[*innerIndex] == 'b') {
                adtSetValue(newPtr, &iter->currentVertex->name);
                stkPush(iter->stackVertices, newPtr);

                adtSetValue(newPtr, iter->currentAdjIterator);
                stkPush(iter->stackAdjacent, newPtr);

                iter->currentVertex = innerVertex;
                iter->color[*innerIndex] = 'g';

                ITRObject *adjIter = graphVertexEdges(iter->graph, innerVertex->name);
                iter->currentAdjIterator = adjIter;

                // TOGLI POST ORDER
                foundNext = true;
            }
        }

        // Se arrivo qui l'iteratore è terminato, se foundNext è falso verifico se posso prendere dallo stack
        if ((!stkEmpty(iter->stackVertices) && !foundNext)) {
            DataObject *tmp1 = stkTopNPop(iter->stackVertices);
            DataObject *tmp2 = stkTopNPop(iter->stackAdjacent);

            iter->currentVertex = adtGetValue(tmp1);
            iter->currentAdjIterator = adtGetValue(tmp2);

            adtDestruct(tmp1);
            adtDestruct(tmp2);
        }
        else if (stkEmpty(iter->stackVertices) || !foundNext){
            foundAdj = false;
            foundNext = true;
            iter->currentVertex = NULL;
        }
    }

    // Non ho trovato il nuovo vertice e lo cerco nell'iteratore dei vertici
    if(!foundAdj) {
        foundNext = false;

        while(!itrTerminated(iter->verticesIterator) && !foundNext) {
            uint *index = itrElement(iter->verticesIterator);
            GraphVertex *vertex = graphVertexFromPointer(iter->graph, index);

            itrSuccessor(iter->verticesIterator);

            // Se il vertice è bianco lo salvo, salvo la lista adiacenti ed esco dal ciclo
            if (iter->color[*index] == 'b') {
                iter->currentVertex = vertex;
                iter->color[*index] = 'g';

                ITRObject *adjIter = graphVertexEdges(iter->graph, vertex->name);
                iter->currentAdjIterator = adjIter;

                foundNext = true;
            }
        }
    }

    if(!foundNext) {
        iter->currentVertex = NULL;
    }

    adtDestruct(newPtr);
}