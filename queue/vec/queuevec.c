#include "queuevec.h"

/* ************************************************************************** */

// Costruisco il QueueType ed assegno tutti i puntatori a funzioni
QueueType* ConstructQueueVecType() {
    QueueType* type = (QueueType*) malloc(sizeof(QueueType));

    type->construct = &queVecConstruct;
    type->destruct = &queVecDestruct;
    type->isEmpty = &queVecEmpty;
    type->head = &queVecHead;
    type->dequeue = &queVecDequeue;
    type->headAndDequeue = &queVecHeadNDequeue;
    type->enqueue = &queVecEnqueue;
    type->clear = &queVecClear;
    type->clone = &queVecClone;
    type->isEqual = &queVecEqual;
    type->map = &queVecMap;
    type->fold = &queVecFold;
    return type;
}

void DestructQueueVecType(QueueType* type) {
    free(type);
}

// Operazioni di costruzione e distruzione della struttura
void* queVecConstruct() {
    QueueVec* queue = (QueueVec*) malloc(sizeof(QueueVec));

    assert(queue);

    queue->numberOfItems = 0;
    queue->dimension = 4;
    queue->front = 0;
    queue->rear = 0;
    queue->elements = (DataObject**) malloc(sizeof(DataObject*) * queue->dimension);

    return queue;
}

void queVecDestruct(void* que) {
    QueueVec* queue = (QueueVec*) que;
    free(queue->elements);
    free(queue);
}

// Operazioni elementari
bool queVecEmpty(void* que) {
    QueueVec* queue = (QueueVec*) que;
    // Se l'indice è diverso da 0 significa che ho un elemento e ritorno che non è vuoto
    return queue->numberOfItems == 0;
}

DataObject* queVecHead(void* que) {
    QueueVec* queue = (QueueVec*) que;

    // Se la coda non è vuota ritorno una copia del primo elemento, altrimenti NULL
    return (!(queVecEmpty(queue))) ? adtClone(queue->elements[queue->front]) : NULL;
}

void queVecDequeue(void* que) {
    QueueVec* queue = (QueueVec*) que;

    // Libero l'elemento alla testa, la incremento e decremento il numero di elementi
    adtDestruct(queue->elements[queue->front++]);
    queue->numberOfItems--;

    // Se sono arrivato alla fine dell'array, torna a 0
    if(queue->front == queue->dimension) {
        queue->front = 0;
    }

    // Se sto usando solo 1/4 dello spazio, lo dimezzo
    if((queue->numberOfItems * 4) == queue->dimension) {
        reallocVecQueue(queue, queue->dimension/2);
    }
}

DataObject* queVecHeadNDequeue(void* que) {
    QueueVec* queue = (QueueVec*) que;

    // Prendo l'elemento di ritorno, incremento front e decremento il numero di elementi
    DataObject* value = queue->elements[queue->front++];
    queue->numberOfItems--;

    // Se sono arrivato alla fine dell'array, torna a 0
    if(queue->front == queue->dimension) {
        queue->front = 0;
    }

    // Se sto usando solo 1/4 dello spazio, lo dimezzo
    if((queue->numberOfItems * 4) == queue->dimension) {
        reallocVecQueue(queue, queue->dimension/2);
    }

    return value;
}

void queVecEnqueue(void* que, DataObject* obj) {
    QueueVec* queue = (QueueVec*) que;

    // Alloco uno spazio per il nuovo DataObject e copio quello in ingresso
    DataObject* newObj = adtClone(obj);

    // Se ho finito lo spazio, ne devo allocare il doppio
    if(queue->numberOfItems == queue->dimension) {
        reallocVecQueue(queue, queue->dimension*2);
    }

    // Se ho finito lo spazio, torno all'inizio
    if(queue->rear == queue->dimension) {
        queue->rear = 0;
    }

    // Inserisco il nuovo elemento nella coda
    queue->elements[queue->rear++] = newObj;
    queue->numberOfItems++;
}

void queVecClear(void* que) {
    QueueVec* queue = (QueueVec*) que;

    uint i = queue->front;
    bool hasFinished = false;

    // Libero i vari elementi della coda
    while(!hasFinished) {
        // Elimino man mano gli elementi e decremento dal numero di elementi
        if(queue->numberOfItems) {
            adtDestruct(queue->elements[i]);
            queue->numberOfItems--;
        }

        i++;

        // Se sono arrivato alla fine, ho liberato tutto
        if(i == queue->rear) {
            hasFinished = true;
        }

        // Se sono alla fine dell'array, ritorna all'inizio
        if(i == queue->dimension) {
            i = 0;
        }
    }

    // Impoasto la grandezza di default 4 e realloco
    queue->dimension = 4;
    queue->front = 0;
    queue->rear = 0;
    queue->elements = (DataObject**) realloc(queue->elements, queue->dimension * sizeof(DataObject*));
}

// Operazioni non-elementari
void* queVecClone(void* que) {
    QueueVec* queue = (QueueVec*) que;

    // Alloco la nuova coda ed il suo numero di elementi come quella in ingresso
    QueueVec* newQueue = (QueueVec*) malloc(sizeof(QueueVec));
    newQueue->dimension = queue->dimension;
    newQueue->numberOfItems = 0;
    newQueue->front = 0;
    newQueue->rear = 0;
    newQueue->elements = (DataObject**) malloc(sizeof(DataObject*) * newQueue->dimension);

    // Inserisco gli elementi uno ad uno nella nuova coda
    uint ind = queue->front;

    // Ciclo finché non arrivo alla fine della coda
    while(ind != queue->rear) {
        // Accodo il valore in newQueue della coda che voglio copiare
        queVecEnqueue(newQueue, queue->elements[ind]);

        ind++;

        // Se sono alla fine dell'array, torno all'inizio
        if(ind == queue->dimension) {
            ind = 0;
        }
    }

    return newQueue;
}

bool queVecEqual(void* que1, void* que2) {
    QueueVec* queue1 = (QueueVec*) que1;
    QueueVec* queue2 = (QueueVec*) que2;

    // Se il numero di elementi o lo spazio allocato non coincide, sono diversi
    if((queue1->numberOfItems != queue2->numberOfItems) || (queue1->dimension != queue2->dimension)) {
        return false;
    }

    // Se la coda non è vuota, verifico elemento per elemento anche se gli indici non coincidono
    if(!queVecEmpty(queue1) && !queVecEmpty(queue2)) {
        // Se la coda non coincide, evito di controllare tutta la coda
        if(adtCompare(queue1->elements[queue1->rear-1], queue2->elements[queue2->rear-1]) != 0) {
            return false;
        }

        // Procedo a controllare elemento per elemento
        uint index1 = queue1->front;
        uint index2 = queue2->front;
        bool hasFinished = false;

        while(!hasFinished) {
            //Verifico che i DataObject siano gli stessi
            if(adtCompare(queue1->elements[index1], queue2->elements[index2]) != 0) {
                return false;
            }

            index1++;
            index2++;

            // Se arrivo alla fine di una delle queue torno all'inizio
            if(index1 == queue1->dimension) {
                index1 = 0;
            }

            if(index2 == queue2->dimension) {
                index2 = 0;
            }

            /* Se sono arrivato alla fine di uno dei due, siccome hanno lo stesso numero
             * di elementi, ho finito */
            if(index1 == queue1->rear) {
                hasFinished = true;
            }
        }
    }
    else {
        // Se arrivo qui devo verificare se l'if è fallito perché sono entrambe vuote o solo una
        if(queVecEmpty(queue1) != queVecEmpty(queue2)) {
            return false;
        }
    }

    return true;
}

void queVecMap(void* que, MapFun function, void* parameter) {
    QueueVec* queue = (QueueVec*) que;

    uint ind = queue->front;
    bool hasFinished = false;

    // Ciclo la funzione su tutti gli elementi della coda
    while(!hasFinished) {
        function(queue->elements[ind], parameter);

        ind++;

        if(ind == queue->dimension) {
            ind = 0;
        }

        if(ind == queue->rear) {
            hasFinished = true;
        }
    }
}

void queVecFold(void* que, FoldFun function, void* value, void* parameter) {
    QueueVec* queue = (QueueVec*) que;

    uint ind = queue->front;
    bool hasFinished = false;

    // Ciclo la funzione su tutti gli elementi della coda
    while(!hasFinished) {
        function(queue->elements[ind], value, parameter);

        ind++;

        if(ind == queue->dimension) {
            ind = 0;
        }

        if(ind == queue->rear) {
            hasFinished = true;
        }
    }
}

// Una funzione per riallocare le code tenendo conto dell'ordine
void reallocVecQueue(QueueVec* queue, uint newDimension) {
    // Alloco un array che è la metà della grandezza attuale
    DataObject** newArray = (DataObject**) malloc(newDimension * sizeof(DataObject*));
    uint oldIndex = queue->front;
    uint newIndex = 0;
    bool hasFinished = false;

    // Inizio a prendere i puntatori della coda nel nuovo array finché non finisco la coda
    while(!hasFinished) {
        // Prendo il puntatore all'elemento
        newArray[newIndex] = queue->elements[oldIndex];

        newIndex++;
        oldIndex++;

        // Se sono arrivato alla fine, ho finito
        if(oldIndex == queue->rear) {
            hasFinished = true;
        }

        // Se sono alla fine dell'array, torno all'inizio
        if(oldIndex == queue->dimension) {
            oldIndex = 0;
        }
    }

    // Libero l'array precedente ed imposto il nuovo nella coda
    free(queue->elements);
    queue->dimension = newDimension;
    queue->elements = newArray;
    queue->front = 0;
    queue->rear = newIndex;
}
