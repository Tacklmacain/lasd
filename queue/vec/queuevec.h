
#ifndef QUEUEVEC_H
#define QUEUEVEC_H

/* ************************************************************************** */

#include "../queue.h"

/* ************************************************************************** */

typedef struct QueueVec {
    uint front, rear;
    uint numberOfItems;
    uint dimension;
    DataObject** elements;
} QueueVec;

/* ************************************************************************** */

// Costruzione del tipo di implementazione con array
QueueType* ConstructQueueVecType();
void DestructQueueVecType(QueueType*);

//Operazioni di costruzione e distruzione della struttura
void* queVecConstruct();
void queVecDestruct(void*);

// Operazioni elementari
bool queVecEmpty(void*);
DataObject* queVecHead(void*);
void queVecDequeue(void*);
DataObject* queVecHeadNDequeue(void*);
void queVecEnqueue(void*, DataObject*);
void queVecClear(void*);

// Operazioni non-elementari
void* queVecClone(void*);
bool queVecEqual(void*, void*);
void queVecMap(void*, MapFun, void*);
void queVecFold(void*, FoldFun, void*, void*);

/* ************************************************************************** */

void reallocVecQueue(QueueVec* queue, uint newSize);

#endif
