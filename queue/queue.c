#include "queue.h"

/* ************************************************************************** */

QueueObject* queConstruct(QueueType* type) {
    assert(type);

    QueueObject* queue = (QueueObject*) malloc(sizeof(QueueObject));
    queue->type = type;
    queue->size = 0;
    queue->que = type->construct();

    return queue;
}

void queDestruct(QueueObject* queObj) {
    assert(queObj);

    if(queObj->que) {
        queObj->type->destruct(queObj->que);
    }

    free(queObj);
}

bool queEmpty(QueueObject* queObj) {
    assert(queObj && queObj->que);
    return queObj->type->isEmpty(queObj->que);
}

DataObject* queHead(QueueObject* queObj) {
    assert(queObj && queObj->que);
    return queObj->type->head(queObj->que);
}

void queDequeue(QueueObject* queObj) {
    assert(queObj && queObj->que);

    if(!queEmpty(queObj)) {
        queObj->size--;
        queObj->type->dequeue(queObj->que);
    }
}

DataObject* queHeadNDequeue(QueueObject* queObj) {
    assert(queObj && queObj->que);

    // Se la coda è vuota ritorno NULL
    if(queEmpty(queObj)) {
        return NULL;
    }
    queObj->size--;
    return queObj->type->headAndDequeue(queObj->que);
}

void queEnqueue(QueueObject* queObj, DataObject* data) {
    assert(queObj && queObj->que);

    if(!data) {
        printf("Impossibile inserire un valore nullo nella coda, operazione annullata.\n\n");
    }
    else {
        queObj->size++;
        queObj->type->enqueue(queObj->que, data);
    }
}

void queClear(QueueObject* queObj) {
    assert(queObj && queObj->que);

    // Elimino gli elementi solo se la coda non è vuota
    if(!queEmpty(queObj)) {
        queObj->size = 0;
        queObj->type->clear(queObj->que);
    }
}

int queSize(QueueObject* queObj) {
    assert(queObj);
    return queObj->size;
}

QueueObject* queClone(QueueObject* queObj) {
    assert(queObj && queObj->que);

    // Creo il QueObject clone
    QueueObject* clone = (QueueObject*) malloc(sizeof(QueueObject));
    clone->type = queObj->type;
    clone->size = queObj->size;
    clone->que = queObj->type->clone(queObj->que);

    return clone;
}

bool queEqual(QueueObject* queObj1, QueueObject* queObj2) {
    assert(queObj1 && queObj1->que && queObj2 && queObj2->que && queObj1->type == queObj2->type);
    return queObj1->type->isEqual(queObj1->que, queObj2->que);
}

void queMap(QueueObject* queObj, MapFun mapFunction, void* parameter) {
    assert(queObj && queObj->que);

    if(!queEmpty(queObj)) {
        queObj->type->map(queObj->que, mapFunction, parameter);
    }
}

void queFold(QueueObject* queObj, FoldFun foldFunction, void* value, void* parameter) {
    assert(queObj && queObj->que);
    queObj->type->fold(queObj->que, foldFunction, value, parameter);
}

bool queExists(QueueObject* queObj, DataObject* data) {
    assert(queObj && queObj->que);
    bool exists = false;
    // Passo il QueueObject in quanto richiamo il metodo astratto
    queFold(queObj, &foldValueExists, &exists, data);
    return exists;
}
