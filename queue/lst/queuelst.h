#ifndef QUEUELST_H
#define QUEUELST_H

/* ************************************************************************** */

#include "../queue.h"

/* ************************************************************************** */

typedef struct QueueLst QueueLst;
typedef struct Node Node;

struct QueueLst {
    Node* front;
    Node* rear;
};

struct Node {
    DataObject* value;
    Node* next;
};

/* ************************************************************************** */

QueueType* ConstructQueueLstType();
void DestructQueueLstType(QueueType*);

//Operazioni di costruzione e distruzione della struttura
void* queLstConstruct();
void queLstDestruct(void*);

// Operazioni elementari
bool queLstEmpty(void*);
DataObject* queLstHead(void*);
void queLstDequeue(void*);
DataObject* queLstHeadNDequeue(void*);
void queLstEnqueue(void*, DataObject*);
void queLstClear(void*);

// Operazioni non-elementari
void* queLstClone(void*);
bool queLstEqual(void*, void*);
void queLstMap(void*, MapFun, void*);
void queLstFold(void*, FoldFun, void*, void*);

/* ************************************************************************** */

#endif
