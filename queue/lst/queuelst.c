#include "queuelst.h"

/* ************************************************************************** */

QueueType* ConstructQueueLstType() {
    QueueType* type = (QueueType*) malloc(sizeof(QueueType));

    type->construct = &queLstConstruct;
    type->destruct = &queLstDestruct;
    type->isEmpty = &queLstEmpty;
    type->head = &queLstHead;
    type->dequeue = &queLstDequeue;
    type->headAndDequeue = &queLstHeadNDequeue;
    type->enqueue = &queLstEnqueue;
    type->clear = &queLstClear;
    type->clone = &queLstClone;
    type->isEqual = &queLstEqual;
    type->map = &queLstMap;
    type->fold = &queLstFold;
    return type;
}

void DestructQueueLstType(QueueType* type) {
    free(type);
}

//Operazioni di costruzione e distruzione della struttura
void* queLstConstruct() {
    QueueLst* queue = (QueueLst*) malloc(sizeof(QueueLst));

    assert(queue);

    // Imposto testa e coda a NULL inizialmente
    queue->front = NULL;
    queue->rear = NULL;

    return queue;
}

void queLstDestruct(void* queue) {
    free(queue);
}

// Operazioni elementari
bool queLstEmpty(void* queue) {
    QueueLst* que = (QueueLst*) queue;
    // Se next è diverso da null significa che ho un elemento e ritorno che non è vuoto
    return !que->front;
}

DataObject* queLstHead(void* queue) {
    QueueLst* que = (QueueLst*) queue;

    // Se la coda non è vuota ritorno la copia del primo elemento, altrimenti NULL
    return (!(queLstEmpty(que))) ? adtClone(que->front->value) : NULL;
}

void queLstDequeue(void* queue) {
    QueueLst* que = (QueueLst*) queue;
    Node* node = que->front;

    // Indico che ora la testa è l'elemento successivo
    que->front = node->next;

    // Libero in quella posizione con la funzione di tipo e libero il nodo
    adtDestruct(node->value);
    free(node);
}

DataObject* queLstHeadNDequeue(void* queue) {
    QueueLst* que = (QueueLst*) queue;
    Node* node = que->front;

    // Indico che ora la sentinella deve puntare all'elemento successivo
    que->front = node->next;

    // Prendo il valore da tornare e libero il nodo
    DataObject* returnObj = node->value;
    free(node);

    return returnObj;
}

void queLstEnqueue(void* queue, DataObject* data) {
    Node* newNode = (Node*) malloc(sizeof(Node));

    // Utilizzo la funzione di clonazione per duplicare il DataObject in ingresso
    newNode->value = adtClone(data);
    newNode->next = NULL;

    // Verifico se la coda è vuota, se è il caso allora anche front deve puntare all'elemento
    QueueLst* que = (QueueLst*) queue;
    if(que->front == NULL) {
        que->front = newNode;
    }
    else {
        que->rear->next = newNode;
    }
    que->rear = newNode;

}

void queLstClear(void* queue) {
    while (!queLstEmpty(queue)) {
        queLstDequeue(queue);
    }
}

// Operazioni non-elementari
void* queLstClone(void* queue) {
    QueueLst* clone = queLstConstruct();

    // Viaggio nella coda in ingresso e metto in coda i vari elementi
    Node* travel = ((QueueLst*) queue)->front;
    while(travel) {
        queLstEnqueue(clone, travel->value);
        travel = travel->next;
    }

    return clone;
}

bool queLstEqual(void* que1, void* que2) {
    Node* travel1 = ((QueueLst*) que1)->front;
    Node* travel2 = ((QueueLst*) que2)->front;

    /* Viaggio tra i due finché entrambi hanno elementi e
     * verifico elemento per elemento */
    while(travel1 && travel2) {
        if(adtCompare(travel1->value, travel2->value) != 0) {
            return false;
        }
        travel1 = travel1->next;
        travel2 = travel2->next;
    }

    /* Ritorno il confronto che entrambi puntino a NULL
     * in quanto è l'unico caso in cui sono uguali */
    return (!travel1 && !travel2);
}

void queLstMap(void* queue, MapFun mapFunction, void* parameter) {
    Node* travel = ((QueueLst*) queue)->front;

    while(travel) {
        mapFunction(travel->value, parameter);
        travel = travel->next;
    }
}

void queLstFold(void* queue, FoldFun foldFunction, void* value, void* parameter) {
    Node* travel = ((QueueLst*) queue)->front;

    while(travel) {
        foldFunction(travel->value, value, parameter);
        travel = travel->next;
    }
}
