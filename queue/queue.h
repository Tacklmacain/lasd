#ifndef QUEUE_H
#define QUEUE_H

/* ************************************************************************** */

#include "../utility.h"
#include "../adt/adt.h"

/* ************************************************************************** */

// Sono quelle utilizzate dal type
typedef void* (*QueConstruct)();
typedef void (*QueDestruct)(void*);

typedef bool (*QueEmpty)(void*);
typedef DataObject* (*QueHead)(void*);
typedef void (*QueDequeue)(void*);
typedef DataObject* (*QueHeadNDequeue)(void*);
typedef void (*QueEnqueue)(void*, DataObject*);
typedef void (*QueClear)(void*);

typedef void* (*QueClone)(void*);
typedef bool (*QueEqual)(void*, void*);
typedef void (*QueMap)(void*, MapFun, void*);
typedef void (*QueFold)(void*, FoldFun, void*, void*);

/* ************************************************************************** */

typedef struct QueueType {
    QueConstruct construct;
    QueDestruct destruct;
    QueEmpty isEmpty;
    QueHead head;
    QueDequeue dequeue;
    QueHeadNDequeue headAndDequeue;
    QueEnqueue enqueue;
    QueClear clear;
    QueClone clone;
    QueEqual isEqual;
    QueMap map;
    QueFold fold;
} QueueType;

typedef struct QueueObject {
    QueueType* type;
    uint size;
    void* que;
} QueueObject;

/* ************************************************************************** */

// Sono quelle richiamate nel main
QueueObject* queConstruct(QueueType*);
void queDestruct(QueueObject*);

bool queEmpty(QueueObject*);
DataObject* queHead(QueueObject*);
void queDequeue(QueueObject*);
DataObject* queHeadNDequeue(QueueObject*);
void queEnqueue(QueueObject*, DataObject*);
void queClear(QueueObject*);

QueueObject* queClone(QueueObject*);
bool queEqual(QueueObject*, QueueObject*);
void queMap(QueueObject*, MapFun, void*);
void queFold(QueueObject*, FoldFun, void*, void*);

int queSize(QueueObject*);

// Dovrò fare la fold e passare l'utility di exits
bool queExists(QueueObject*, DataObject*);

/* ************************************************************************** */

#endif
