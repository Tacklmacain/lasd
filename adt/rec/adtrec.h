#ifndef ADTREC_H
#define ADTREC_H

/* ************************************************************************** */

#include "../adt.h"

#define MaxProductPrice 2500
#define MaxProductWeigth 500

/* ************************************************************************** */

typedef struct Product {
    int weigth;
    float price;
} Product;

/* ************************************************************************** */

DataType* ConstructRecordDataType();
void DestructRecordDataType(DataType*);

void* adtConstructRecord();
void adtDestructRecord(void*);
void* adtGetValueRecord(void*);
void adtSetValueRecord(void**, void*);
void adtRandomValueRecord(void*);
void adtReadFromKeyboardRecord(void*);
void adtWriteToMonitorRecord(void*);
void* adtCloneRecord(void*);
int adtCompareRecord(void*, void*);

/* ************************************************************************** */

#endif