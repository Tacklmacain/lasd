#include "adtrec.h"

/* ************************************************************************** */

DataType* ConstructRecordDataType() {
    DataType* type = (DataType*) malloc(sizeof(DataType));

    type->construction = &adtConstructRecord;
    type->destruction = &adtDestructRecord;
    type->getValue = &adtGetValueRecord;
    type->setValue = &adtSetValueRecord;
    type->randomValue = &adtRandomValueRecord;
    type->readFromKeyboard = &adtReadFromKeyboardRecord;
    type->writeToMonitor = &adtWriteToMonitorRecord;
    type->clone = &adtCloneRecord;
    type->compare = &adtCompareRecord;
    return type;
}

void DestructRecordDataType(DataType* type) {
    free(type);
}

void* adtConstructRecord() {
    Product* newPrd = (Product*) malloc(sizeof(Product));
    newPrd->price = 0;
    newPrd->weigth = 0;
    return newPrd;
}

void adtDestructRecord(void* value) {
    free(value);
}

void* adtGetValueRecord(void* value) {
    Product* copy = (Product*) malloc(sizeof(Product));
    Product* original = (Product*) value;
    copy->price = original->price;
    copy->weigth = original->weigth;

    return copy;
}

void adtSetValueRecord(void** oldValue, void* newValue) {
    Product* newProduct = (Product*) newValue;
    Product* oldProduct = (Product*) *oldValue;

    oldProduct->price = newProduct->price;
    oldProduct->weigth = newProduct->weigth;
}

void adtRandomValueRecord(void* value) {
    Product* randomProduct = (Product*) value;
    randomProduct->weigth = rndNum(1, MaxProductWeigth);
    randomProduct->price = (float)rand() / ((float) RAND_MAX/MaxProductPrice);
}

void adtReadFromKeyboardRecord(void* value) {
    Product* product = (Product*) value;

    printf("Inserire il peso del prodotto (int): \n");
    scanf("%d", &product->weigth);

    printf("Inserire il prezzo del prodotto (float): \n");
    scanf("%f", &product->price);
}

void adtWriteToMonitorRecord(void* value) {
    printf("Peso: %d\nPrezzo: %f\n", ((Product*) value)->weigth, ((Product*) value)->price);
}

void* adtCloneRecord(void* value) {
    Product* clone = (Product*) malloc(sizeof(Product));

    Product* originalProduct = (Product*) value;
    clone->price = originalProduct->price;
    clone->weigth = originalProduct->weigth;

    return clone;
}

int adtCompareRecord(void* val1, void* val2) {
    Product* product1 = (Product*) val1;
    Product* product2 = (Product*) val2;

    /* Se il peso e' uguale ed il prezzo non e' strettamente maggiore ritorno che sono uguali
     * faccio questo confronto per price e' un float */
    if(product1->weigth == product2->weigth) {
        if(!(product1->price > product2->price || product2->price > product1->price)) {
            return 0;
        }
    }
    /* Se arrivo qua uno dei due prezzi o pesi e' diverso ed uso
     * il peso per determinare il più grande, torno di conseguenza 1 o -1 */
    return (product1->weigth > product2->weigth) ? 1 : -1;
}
