
#include "adtstr.h"

/* ************************************************************************** */

DataType* ConstructStringDataType() {
    DataType* type = (DataType*) malloc(sizeof(DataType));

    type->construction = &adtConstructString;
    type->destruction = &adtDestructString;
    type->getValue = &adtGetValueString;
    type->setValue = &adtSetValueString;
    type->randomValue = &adtRandomValueString;
    type->readFromKeyboard = &adtReadFromKeyboardString;
    type->writeToMonitor = &adtWriteToMonitorString;
    type->clone = &adtCloneString;
    type->compare = &adtCompareString;
    return type;
}

void DestructStringDataType(DataType* type) {
    free(type);
}

void* adtConstructString() {
    // Alloco per una stringa pari alla costante globale MaxStrLen + 1 per l'escape
    char* newStr = (char*) malloc(sizeof(char) * (MaxStrLen + 1));
    return newStr;
}

void adtDestructString(void* value) {
    free(value);
}

void* adtGetValueString(void* value) {
    char* copy = (char*) malloc(sizeof(char) * (strlen((char*) value) + 1));
    strcpy(copy, (char*) value);
    return copy;
}

void adtSetValueString(void** oldValue, void* newValue) {
    // Controllo se la stringa in ingresso è sufficiente o devo reallocare
    if(strlen((char*) newValue) <= MaxStrLen) {
        //Siccome già ho allocato spazio nella costruzione posso fare direttamente lo strcpy
        strcpy((char*) (*oldValue), (char*) newValue);
    } else {
        //L'utente ha inserito una stringa più grande e devo reallocare
        *oldValue = (char*) realloc(*oldValue, sizeof(char) * (strlen((char*) newValue) + 1));
        strcpy((char*) (*oldValue), (char*) newValue);
    }
}

void adtRandomValueString(void* value) {
    char* randomStr = rndStr((uint) rndNum(MinStrLen, MaxStrLen));
    strcpy((char*) value, randomStr);
    free(randomStr);
}

void adtReadFromKeyboardString(void* value) {
    // Dichiaro una lunghezza a scelta dell'utente a patto che sia compresa tra MinStrLen e MaxStrLen
    int length = 0;
    do {
        printf("Inserire la lunghezza della stringa, dev'essere un valore compreso tra %d e %d e successivamente la stringa: ", MinStrLen, MaxStrLen);
        scanf("%d", &length);
    } while(length < MinStrLen || length > MaxStrLen);

    // Mi prendo \n
    getchar();

    // Leggo la stringa dall'utente
    printf("\nInserisci la stringa di %d caratteri: ", length);
    getStr(((char*) value), (uint) length);
}

void adtWriteToMonitorString(void* value) {
    printf("%s", (char*) value);
}

void* adtCloneString(void* value) {
    char* clone = (char*) malloc(sizeof(char) * (strlen((char*) value) + 1));
    strcpy(clone, (char*) value);
    return clone;
}

int adtCompareString(void* val1, void* val2) {
    return strcmp((char*) val1, (char*) val2);
}
