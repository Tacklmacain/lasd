#ifndef ADTSTR_H
#define ADTSTR_H

/* ************************************************************************** */

#include "../adt.h"

/* ************************************************************************** */

#define MaxStrLen 50
#define MinStrLen 2

/* ************************************************************************** */

DataType* ConstructStringDataType();
void DestructStringDataType(DataType*);

void* adtConstructString();
void adtDestructString(void*);
void* adtGetValueString(void*);
void adtSetValueString(void**, void*);
void adtRandomValueString(void*);
void adtReadFromKeyboardString(void*);
void adtWriteToMonitorString(void*);
void* adtCloneString(void*);
int adtCompareString(void*, void*);

/* ************************************************************************** */

#endif

