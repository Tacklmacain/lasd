#include "adtint.h"

/* ************************************************************************** */

// Costruisco il DataType ed assegno tutti i puntatori a funzioni
DataType* ConstructIntDataType() {
    DataType* type = (DataType*) malloc(sizeof(DataType));

    type->construction = &adtConstructInt;
    type->destruction = &adtDestructInt;
    type->getValue = &adtGetValueInt;
    type->setValue = &adtSetValueInt;
    type->randomValue = &adtRandomValueInt;
    type->readFromKeyboard = &adtReadFromKeyboardInt;
    type->writeToMonitor = &adtWriteToMonitorInt;
    type->clone = &adtCloneInt;
    type->compare = &adtCompareInt;
    return type;
}

void DestructIntDataType(DataType* type) {
    free(type);
}

void* adtConstructInt() {
    int* newInt = (int*) malloc(sizeof(int));
    *newInt = 1; // Inizializzato ad 1 oppure la fold restituisce sempre 0
    return newInt;
}

void adtDestructInt(void* value) {
    free(value);
}

void* adtGetValueInt(void* value) {
    int* copy = (int*) malloc(sizeof(int));
    *copy = *((int*) value);
    return copy;
}

void adtSetValueInt(void** oldValue, void* newValue) {
    *((int*) (*oldValue)) = *((int*) newValue);
}

void adtRandomValueInt(void* value) {
    *((int*) value) = rndNum(-MaxIntAbsRndVal, MaxIntAbsRndVal);
}

void adtReadFromKeyboardInt(void* value) {
    printf("Inserire un intero: ");
    scanf("%d", (int*) value);
}

void adtWriteToMonitorInt(void* value) {
    printf("%d", *((int*) value));
}

void* adtCloneInt(void* value) {
    int* clone = (int*) malloc(sizeof(int));
    *clone = *((int*) value);
    return clone;
}

//Ritorna 0 se pari, 1 se val1 > val2 mentre -1 se val2 > val1
int adtCompareInt(void* val1, void* val2) {
    int a = *((int*) val1);
    int b = *((int*) val2);
    return (a > b) ? 1 : ((b > a) ? -1 : 0);
}
