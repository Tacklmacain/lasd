#include "adt.h"

/* ************************************************************************** */

// Costruisco il DataObject ed il value tramite il costruttore del dato specifico
DataObject* adtConstruct(DataType* type) {
    assert(type);

    DataObject* obj = (DataObject*) malloc(sizeof(DataObject));
    obj->type = type;
    obj->value = type->construction();

    return obj;
}

// Distrugge il valore ed il DataObject ma non il riferimento al DataType (cancellerebbe il DataType stesso!)
void adtDestruct(DataObject* obj) {
    // Distruggo il tipo solo se è diverso da NULL
    if(obj) {
        if(obj->value) {
            obj->type->destruction(obj->value);
        }
        free(obj);
    }
}

// Il get è indipendente dal tipo di dato
void* adtGetValue(DataObject* obj) {
    assert(obj);
    return obj->type->getValue(obj->value);
}

void adtSetValue(DataObject* obj, void* newValue) {
    assert(obj);

    obj->type->setValue(&obj->value, newValue);
}

// Imposto un valore casuale
void adtRandomValue(DataObject* obj) {
    assert(obj && obj->value);
    obj->type->randomValue(obj->value);
}

// Leggo dall'utente indipendnetemente dal tipo di dato
void adtReadFromKeyboard(DataObject* obj) {
    assert(obj && obj->value);
    obj->type->readFromKeyboard(obj->value);
}

// Stampo esclusivamente il tipo di dato
void adtWriteToMonitor(DataObject* obj) {
    if(!obj || !obj->value) {
        printf("NULL");
    }
    else {
        obj->type->writeToMonitor(obj->value);
    }
}

DataObject* adtClone(DataObject* obj) {
    assert(obj);

    DataObject* clone = (DataObject*) malloc(sizeof(DataObject));
    clone->type = obj->type;
    clone->value = obj->type->clone(obj->value);

    return clone;
}

// Posso confrontare solo DataObject dello stesso tipo
int adtCompare(DataObject* obj1, DataObject* obj2) {
    assert(obj1 && obj2 && obj1->value && obj2->value && obj1->type == obj2->type);
    return obj1->type->compare(obj1->value, obj2->value);
}
