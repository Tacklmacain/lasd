
#ifndef ADT_H
#define ADT_H

/* ************************************************************************** */

#include "../utility.h"

/* ************************************************************************** */

// Dichiarazione dei vari puntatori a funzioni per le funzioni specifiche
typedef void* (*ADTConstruct)();
typedef void (*ADTDestruct)(void*);

typedef void* (*ADTGetValue)(void*);
typedef void (*ADTSetValue)(void**, void*);

typedef void (*ADTRandomValue)(void*);
typedef void (*ADTReadFromKeyboard)(void*);
typedef void (*ADTWriteToMonitor)(void*);

typedef void* (*ADTClone)(void*);
typedef int (*ADTCompare)(void*, void*);

/* ************************************************************************** */

typedef struct DataType {
    ADTConstruct construction;
    ADTDestruct destruction;
    ADTGetValue getValue;
    ADTSetValue setValue;
    ADTRandomValue randomValue;
    ADTReadFromKeyboard readFromKeyboard;
    ADTWriteToMonitor writeToMonitor;
    ADTClone clone;
    ADTCompare compare;
} DataType;

typedef struct DataObject {
    DataType* type;
    void* value;
} DataObject;

/* ************************************************************************** */

// Sono i metodi del main
DataObject* adtConstruct(DataType*);
void adtDestruct(DataObject*);

void* adtGetValue(DataObject*);
void adtSetValue(DataObject*, void*);

void adtRandomValue(DataObject*);
void adtReadFromKeyboard(DataObject*);
void adtWriteToMonitor(DataObject*);

DataObject* adtClone(DataObject*);
int adtCompare(DataObject*, DataObject*);

/* ************************************************************************** */

typedef void (*MapFun)(DataObject *, void *);
typedef void (*FoldFun)(DataObject *, void *, void *);

#endif
