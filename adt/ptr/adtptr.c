#include "adtptr.h"

/* ************************************************************************** */

DataType* ConstructPtrDataType() {
    DataType* type = (DataType*) malloc(sizeof(DataType));

    // Le funzioni non supportate sono NULL
    type->construction = &adtConstructPtr;
    type->destruction = &adtDestructPtr;
    type->getValue = &adtGetValuePtr;
    type->setValue = &adtSetValuePtr;
    type->randomValue = NULL;
    type->readFromKeyboard = NULL;
    type->writeToMonitor = NULL;
    type->clone = &adtClonePtr;
    type->compare = NULL;
    return type;
}

void DestructPtrDataType(DataType* type) {
    free(type);
}

void* adtConstructPtr() {
    void* ptr = NULL;
    return ptr;
}

void adtDestructPtr(void* value) {
    //Non devo liberare nulla, non è allocato!
}

void* adtGetValuePtr(void* value) {
    return value;
}

void adtSetValuePtr(void** oldValue, void* newValue) {
    *oldValue = newValue;
}

// La enqueue richiede la clone, ma che non fa altro che restituire lo stesso puntatore
void* adtClonePtr(void* value) {
    return value;
}
