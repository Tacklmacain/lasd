#ifndef ADTPTR_H
#define ADTPTR_H

/* ************************************************************************** */

#include "../adt.h"

/* ************************************************************************** */

DataType* ConstructPtrDataType();
void DestructPtrDataType(DataType*);

void* adtConstructPtr();
void adtDestructPtr(void*);
void* adtGetValuePtr(void*);
void adtSetValuePtr(void**, void*);
void* adtClonePtr(void*);

/* ************************************************************************** */

#endif
