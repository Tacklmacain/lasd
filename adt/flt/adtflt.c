#include "adtflt.h"

/* ************************************************************************** */

DataType* ConstructFloatDataType() {
    DataType* type = (DataType*) malloc(sizeof(DataType));

    type->construction = &adtConstructFloat;
    type->destruction = &adtDestructFloat;
    type->getValue = &adtGetValueFloat;
    type->setValue = &adtSetValueFloat;
    type->randomValue = &adtRandomValueFloat;
    type->readFromKeyboard = &adtReadFromKeyboardFloat;
    type->writeToMonitor = &adtWriteToMonitorFloat;
    type->clone = &adtCloneFloat;
    type->compare = &adtCompareFloat;
    return type;
}

void DestructFloatDataType(DataType* type) {
    free(type);
}

void* adtConstructFloat() {
    float* newFloat = (float*) malloc(sizeof(float));
    *newFloat = 0;
    return newFloat;
}

void adtDestructFloat(void* value) {
    free(value);
}

void* adtGetValueFloat(void* value) {
    float* copy = (float*) malloc(sizeof(float));
    *(copy) = *((float*) value);
    return copy;
}

void adtSetValueFloat(void** oldValue, void* newValue) {
    *((float*) (*oldValue)) = *((float*) newValue);
}

void adtRandomValueFloat(void* value) {
    *((float*) value) = (float)rand() / ((float) RAND_MAX/MaxFltAbsRndVal);
}

void adtReadFromKeyboardFloat(void* value) {
    printf("Inserire un float: ");
    scanf("%f", (float*) value);
}

void adtWriteToMonitorFloat(void* value) {
    printf("%f", *((float*) value));
}

void* adtCloneFloat(void* value) {
    float* clone = (float*) malloc(sizeof(float));
    *clone = *((float*) value);
    return clone;
}

//Ritorna 0 se pari, 1 se val1 > val2 mentre -1 se val2 > val1
int adtCompareFloat(void* val1, void* val2) {
    float a = *((float*) val1);
    float b = *((float*) val2);
    return (a > b) ? 1 : ((b > a) ? -1 : 0);
}
