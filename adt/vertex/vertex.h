#ifndef ADTVERTEX_H
#define ADTVERTEX_H

/* ************************************************************************** */

#include "../adt.h"

/* ************************************************************************** */

/** ADT di appoggio per fare mapping nome-indice dei vertici nel grafo
  * @attribute identifier
  * L'indice effettivo nell'array dei vertici
  *
  * @attribute vertexName
  * Il nome scelto dall'utente per quel vertice
  */
typedef struct ADTVertexMap {
    uint identifier;
    int vertexName;
} ADTVertexMap;

/* ************************************************************************** */

DataType* ConstructGraphMapDataType();
void DestructGraphMapDataType(DataType*);

void* adtConstructGraphMap();
void adtDestructGraphMap(void*);
void* adtGetValueGraphMap(void*);
void adtSetValueGraphMap(void**, void*);
void adtWriteToMonitorGraphMap(void*);
void* adtCloneGraphMap(void*);
int adtCompareGraphMap(void*, void*);

/* ************************************************************************** */

#endif