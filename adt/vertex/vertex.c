#include "vertex.h"

/* ************************************************************************** */

DataType* ConstructGraphMapDataType() {
    DataType* type = (DataType*) malloc(sizeof(DataType));

    // Le funzioni non supportate puntano a NULL impedendomi la loro invocazione
    type->construction = &adtConstructGraphMap;
    type->destruction = &adtDestructGraphMap;
    type->getValue = &adtGetValueGraphMap;
    type->setValue = &adtSetValueGraphMap;
    type->randomValue = NULL;
    type->readFromKeyboard = NULL;
    type->writeToMonitor = &adtWriteToMonitorGraphMap;
    type->clone = &adtCloneGraphMap;
    type->compare = &adtCompareGraphMap;
    return type;
}

void DestructGraphMapDataType(DataType* type) {
    free(type);
}

void* adtConstructGraphMap() {
    ADTVertexMap* newMap = (ADTVertexMap*) malloc(sizeof(ADTVertexMap));
    newMap->identifier = 0;
    newMap->vertexName = 0;
    return newMap;
}

void adtDestructGraphMap(void* value) {
    free(value);
}

void* adtGetValueGraphMap(void* value) {
    ADTVertexMap* original = (ADTVertexMap*) value;
    int* id = (int*) malloc(sizeof(int));
    *id = original->identifier;

    return id;
}

void adtSetValueGraphMap(void** oldValue, void* newValue) {
    ADTVertexMap* new = (ADTVertexMap*) newValue;
    ADTVertexMap* old = (ADTVertexMap*) *oldValue;

    old->vertexName = new->vertexName;
    old->identifier = new->identifier;
}

void adtWriteToMonitorGraphMap(void* value) {
    printf("Identificatore: %u\nNome del vertice: %u\n", ((ADTVertexMap*) value)->identifier, ((ADTVertexMap*) value)->vertexName);
}

void* adtCloneGraphMap(void* value) {
    ADTVertexMap* original = (ADTVertexMap*) value;
    ADTVertexMap* clone = (ADTVertexMap*) malloc(sizeof(ADTVertexMap));
    clone->vertexName = original->vertexName;
    clone->identifier = original->identifier;

    return clone;
}

int adtCompareGraphMap(void* val1, void* val2) {
    int a = ((ADTVertexMap*) val1)->vertexName;
    int b = ((ADTVertexMap*) val2)->vertexName;

    // Ritorno in base al nome del vertice
    return (a > b) ? 1 : ((b > a) ? -1 : 0);
}
